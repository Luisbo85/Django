from django.db import models

from vie.models import Auto, Chofer, Usuario
from viePay.models import MetodoPago

# Create your models here.

class TarjetaVie(models.Model):
	"""
		Modelo en el que se va tener registro de las tarjetas para el sistma de vieTransporte.
	"""
	idTarjeta          = models.AutoField (primary_key = True)
	#Codigo Físico de la tarjeta
	codigoTarj 	       = models.CharField (max_length =  12, unique = True, db_index = True)
	#Codigo a nivel registro-usuario		
	codigoUsuario      = models.CharField (max_length = 12, db_index = True)
	estatus            = models.SmallIntegerField (default = 0)
	#Saldo que tiene la tarjeta al entregarse con saldo inicial
	saldo              = models.DecimalField(max_digits = 6, decimal_places = 2, default = 0)
	#Fecha en la que se registro la tarjeta
	fechaReg           = models.DateTimeField (auto_now_add = True)
	fechaActualizacion = models.DateTimeField(auto_now_add = False, null = True)
	registrador        = models.ForeignKey(Usuario, on_delete = models.CASCADE, null = True)


class TarjetaUsuario(models.Model):
	"""
		Modelo para manejar un historial de las tarjetas
	"""
	idTarjetaU = models.AutoField(primary_key = True)
	cartera    = models.ForeignKey        (MetodoPago, on_delete = models.CASCADE)
	tarjeta    = models.ForeignKey        (TarjetaVie, on_delete = models.CASCADE)
	fecha      = models.DateTimeField     (auto_now_add = True)
	estatus    = models.SmallIntegerField (default = 0)

class TarjetaLigada(models.Model):
	"""docstring for TarjetaLigada"""
	idTarjeta          = models.BigIntegerField (primary_key=True)
	codigoTarj         = models.CharField (max_length = 12)
	codigoUsuario      = models.CharField (max_length = 12)
	saldo              = models.DecimalField(max_digits = 6, decimal_places = 2, default = 0)
	fecha              = models.DateTimeField()
	fechaActualizacion = models.DateTimeField()
	cartera            = models.ForeignKey(MetodoPago, on_delete=models.DO_NOTHING)

	class Meta:
		managed  = False
		db_table = 'vTarjetaLigada'
		

class SaldoTarjeta(models.Model):
	"""
		Registro donde se sabe cuanto saldo se esta agregando a una tarjeta
	"""
	idSaldoT      = models.AutoField    (primary_key = True)
	tarjeta       = models.ForeignKey   (TarjetaVie, on_delete = models.CASCADE)
	saldoAnterior = models.DecimalField (max_digits = 6, decimal_places = 2)
	saldoInicio   = models.DecimalField (max_digits = 6, decimal_places = 2)
	fecha         = models.DateTimeField(auto_now_add = True)
	recargador    = models.ForeignKey   (Usuario, on_delete = models.CASCADE)

class Camioneta(models.Model):
	"""
		Registro de la informacion de la camioneta de vieTransporte
	"""
	idCamioneta  = models.AutoField     (primary_key = True)
	alias        = models.CharField     (max_length = 30, db_index = True)
	disponible   = models.BooleanField  (default = True)
	idAuto       = models.OneToOneField (Auto, on_delete = models.CASCADE)
	fecha     	 = models.DateTimeField (auto_now_add = True)

class Ruta(models.Model):
	"""
		Registro de los datos 
	"""
	idRuta  = models.AutoField         (primary_key = True)
	estatus = models.SmallIntegerField (default = 0)
	nombre  = models.CharField         (max_length = 70)
	cuartoS = models.CharField         (max_length = 30)
	fecha   = models.DateTimeField     (auto_now_add = True)

class Turno(models.Model):
	"""
		Permite tener regitro del turno del chofer
	"""
	idTurno     = models.AutoField         (primary_key = True)
	estatus     = models.SmallIntegerField (default = 0)
	idRuta      = models.ForeignKey        (Ruta, on_delete = models.CASCADE)
	idCamioneta = models.ForeignKey        (Camioneta, on_delete = models.CASCADE)
	idChofer    = models.ForeignKey        (Chofer, on_delete = models.CASCADE)
	fechaInicio = models.DateTimeField     (auto_now_add = True)
	fechaFin    = models.DateTimeField     (auto_now_add = False, null = True)
	vueltas     = models.SmallIntegerField (default = 0)

class Lector(models.Model):
	"""
		Registro de los lectores que estan operando en el sistema
	"""
	idLector    = models.AutoField         (primary_key = True)
	estatus     = models.SmallIntegerField (default = 0)
	turno       = models.ForeignKey        (Turno, on_delete = models.CASCADE, null = True, default = None)
	#Identificacion externa. Vie
	codigo      = models.IntegerField      (unique = True, db_index = True)
	#Identificacion fisica
	macAddress  = models.CharField         (max_length = 17, unique = True, db_index = True)
	fecha       = models.DateTimeField     (auto_now_add = True)
	descripcion = models.CharField         (max_length = 500, default = '')
	ultimaSincro = models.DateTimeField (auto_now_add = False, null = True)
		
class LectorCamioneta(models.Model):
	"""
		Modelo para relacionar los lectores con las camionetas
	"""
	idLector    = models.ForeignKey        (Lector, on_delete = models.CASCADE)
	idCamioneta = models.ForeignKey        (Camioneta, on_delete = models.CASCADE)
	estatus     = models.SmallIntegerField (default = 0)
	fecha       = models.DateTimeField     (auto_now_add = True)

class Pasaje(models.Model):
	"""
		Registro de los viajes hechos por los usuarios
	"""
	idPasaje    = models.AutoField         (primary_key = True)
	idTurno     = models.ForeignKey        (Turno, on_delete = models.CASCADE)
	idTarjeta   = models.ForeignKey        (TarjetaVie, on_delete = models.CASCADE)
	#Identificacion enviada por el lector para poder seguir su cobro
	transaccion = models.IntegerField      (default = 0)
	estatus     = models.SmallIntegerField (default = 0)
	fecha       = models.DateTimeField     (auto_now_add = True)
	pasajeros   = models.SmallIntegerField (default = 0)
	costo       = models.DecimalField      (max_digits = 6, decimal_places = 2)