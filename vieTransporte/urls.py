# -*- coding: utf-8 -*-
from django.conf.urls                   import url
from django.conf                        import settings
from django.conf.urls.static            import static

from vie.tools.telefono                 import Movil
from vieTransporte.views                import views
from vieTransporte.views.viewUsuario    import Usuario
from vieTransporte.views.viewAdmin      import Admin
from vieTransporte.views.viewChofer     import Chofer
from vieTransporte.views.viewCamioneta  import Camioneta
from vieTransporte.views.viewLector     import Lector
from vieTransporte.views.viewTarjeta    import Tarjeta
from vieTransporte.views.viewChofer     import Chofer
from vieTransporte.views.viewPersona    import Persona

urlpatterns = [
	url(r'^$',                              views.index, name='index'),
    url(r'^comprobar/$',                    Usuario.esValido),
    url(r'^admin/login/$',                  Admin.vieAdminLogin),
    url(r'^admin/historialTarjeta/$',       Admin.historialViajes),
    url(r'^admin/chofer/lista/$',           Admin.choferLista),
    url(r'^admin/chofer/suspender/$',       Admin.choferSuspender),
    url(r'^admin/chofer/$',                 Admin.dirigir),
    url(r'^admin/reporte$',            Admin.reporteDiario, name='Reporte'),
    url(r'^admin/tarjeta/$',                Admin.tarjeta),
    url(r'^admin/tarjeta/vinculo/$',        Admin.tarjetaVinculo),
    url(r'^logout/$',                       Usuario.vieLogout),
    url(r'^lector/pasaje/$',                Lector.pasaje),
    url(r'^lector/saldo/$',                 Lector.saldo),
    url(r'^lector/sincronizar/$',           Lector.sincronizar),
    url(r'^killsessions/$',                 Usuario.cerrarSesiones),
    url(r'^recuperar/$',                    Usuario.recuperar),
    url(r'^restablecer/$',                  Usuario.restablecer),
    url(r'^validarCod/$',                   Usuario.vieValidarCod),
    url(r'^envcod/$',                       Movil.reenvioCod),
    url(r'^valcod/$',                       Movil.revalidarCod),
	url(r'^persona/login/$',                Usuario.viePersonaLogin),
    url(r'^persona/registro/$',             Persona.obtenerPeticion),
    url(r'^persona/tarjeta/$',              Tarjeta.ligarTarjeta),
    url(r'^persona/tarjeta/listado/$',      Tarjeta.mostrar),
	url(r'^chofer/login/$',                 Usuario.vieChoferLogin),
    url(r'^chofer/logout/$',                Chofer.logout),
    url(r'^chofer/turno/$',                 Chofer.turnoActivo),
    url(r'^chofer/listaTurno$',             Chofer.listaTurnoActivos),
    url(r'^chofer/$',                       Chofer.obtenerPeticion),
    url(r'^camioneta/listaDisp/$',          Camioneta.listaDisp)
]