from django.apps import AppConfig


class VieTransporteConfig(AppConfig):
    name = 'vieTransporte'

    path = 'vieTransporte/'
