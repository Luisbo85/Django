# -*- coding: utf-8 -*-
from django.views.decorators.csrf	import csrf_exempt

from Django.settings import APPS_VIE, EMPRESAS

from vie.tools.errores			import Status as stat
from vie.views.views import verfAcceso,recibirJson, enviarJson, leerDatos
from vieTransporte.models import Camioneta as CamionetaMdl

class Camioneta:

	def listaDisp(request):
		lista = CamionetaMdl.objects.filter(disponible = True).only('idCamioneta');
		resp = []
		if len(lista) > 0:
			#debug(lista);
			for dato in lista:
				campos = {}
				campos['alias'] = dato.alias
				campos['id'] = dato.idCamioneta
				resp.append(campos)
			return enviarJson({'camionetas': resp}, stat.OK)
		else:
			return enviarJson({'mensaje': 'No se encontraron camionetas'}, stat.NOT_FOUND)