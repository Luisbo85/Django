from django.views.decorators.csrf	import csrf_exempt

import vie.tools.validar   as val
from vie.views.views	   import recibirJson, enviarJson, debug
from vie.tools.errores	   import Status as stat
from vieTransporte.models  import Pasaje as PasajeMdl

class Pasaje:

	PENDIENTE = 0
	PAGADO = 1

	#Varibles para cobros
	PASAJE = 15
	ENCONTRA = 25

	def save(turno, tarjeta, trans, estatus, costo):
		try:
			pasaje = PasajeMdl(
				idTurno = turno,
				idTarjeta = tarjeta,
				transaccion = trans,
				estatus = estatus,
				costo = costo
			)
			pasaje.save()
			return True
		except Exception as e:
			debug(e)
			return False

	def costo():
		return Pasaje.PASAJE

	def max_descuento():
		return Pasaje.PASAJE - Pasaje.ENCONTRA

	

		