import logging

from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone

import vie.tools.validar			as val
from vie.tools.estatus              import EMPRESAS, APPS_VIE
from vie.views.views	            import recibirJson, enviarJson, leerDatos, verfAcceso, debug
from vie.tools.errores		        import Status as stat
from vie.views.viewUsuario          import Usuario as vUsuario
from vie.views.viewChofer           import Chofer
from .viewTarjeta                   import Tarjeta
from .viewReporte					import Reporte

logger = logging.getLogger(__name__)

class Admin:
	"""
		Vista que controla las funciones que tiene un administrador con 
	"""
	@csrf_exempt
	def vieAdminLogin(request):
		"""
			Dar acceso al administrador para poder usar las opciones que tiene actualmente
		"""
		error, datos = leerDatos(request)
		if error:
			return enviarJson(datos, error)
		else:
			return vUsuario.vieLogin(datos, APPS_VIE['TRANS_ADMIN'])
			

	@csrf_exempt
	def dirigir(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				if 'token' in datos:
					if type(datos['token']) == str:
						usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
						if usuario:
							if request.method == 'POST':
								return Chofer.vieRegistroChofer(datos, APPS_VIE['TRANS_CHOFER'])
							elif request.method == 'PATCH':
								return Chofer.vieSuspender(datos)
							elif request.method == 'PUT':
								return Chofer.vieReactivar(datos)
							elif request.method == 'GET':
								return Chofer.mostrar(datos)
							else:
								return enviarJson({'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}, stat.NOT_ALLOW)
						else:
							if permitido:
								return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
							else:
								return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'token', 'tipo': 'string'}, stat.BAD_REQUEST)	
				else:
					return enviarJson({'mensaje': 'Falta campos en la peticion', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def choferLista(request):
		try:
			error, datos = leerDatos(request) 
			if error:
				return enviarJson(datos, error)
			else:
				if 'token' in datos:
					if type(datos['token']) == str:
						usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
						if usuario:
							return Chofer.listar()
						else:
							if permitido:
								return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
							else:
								return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'token', 'tipo': 'string'}, stat.BAD_REQUEST)	
				else:
					return enviarJson({'mensaje': 'Falta campos en la peticion', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def choferSuspender(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				if 'token' in datos:
					if type(datos['token']) == str:
						usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
						if usuario:
							if request.method == 'POST':
								return Chofer.vieSuspender(datos)
							else:
								return enviarJson({'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}, stat.NOT_ALLOW)
						else:
							if permitido:
								return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
							else:
								return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'token', 'tipo': 'string'}, stat.BAD_REQUEST)	
				else:
					return enviarJson({'mensaje': 'Falta campos en la peticion', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def historialViajes(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string', 'tarjeta': 'string'})
				if valido:
					usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
					if usuario:
						return Tarjeta.historial(datos['tarjeta'])
					else:
						if permitido:
							return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
						else:
							return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	def recargarTarjeta(datos, usuario):
		valido, campo, tipo = val.validarDatos(datos, {'codigo':'string', 'cantidad': 'int'})
		if valido:
			estatus, resp = Tarjeta.recargar(datos['codigo'], datos['cantidad'], usuario)
			if estatus == stat.OK:
				resp = {'mensaje': 'Tarjeta recargada', 'saldo': resp}
		else:
			resp = {'mensaje': 'Faltan campos', 'campo': campo}
			estatus = stat.BAD_REQUEST
			if tipo:
				resp['tipo'] = tipo
		return estatus, resp

	def registrarTarjeta(datos, usuario):
		estatus, resp = Tarjeta.registrarTarjeta(datos, usuario)
		if estatus == stat.OK:
			resp = {'mensaje': 'Tarjeta registrada con exito'}
		return estatus, resp

	@csrf_exempt
	def reporteDiario(request):
		if request.method == 'POST':
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string', 'fecha':'string'})
				if valido:
					return Reporte.reporteDiario(datos)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}, stat.NOT_ALLOW)

	def saldoTarjeta(datos):
		if 'codigo' in datos:
			if type(datos['codigo']) == str:
				estatus, resp = Tarjeta.saldo(datos['codigo'], False)
				if estatus == stat.OK:
					resp = {'mensaje': 'El saldo de su tarjeta es el siguiente', 'saldo': resp}
				return estatus, resp
			else:
				return stat.BAD_REQUEST, {'mensaje': 'Faltan campos', 'campo': 'codigo', 'tipo': 'string'}
		else:
			return stat.BAD_REQUEST, {'mensaje': 'Faltan campos', 'campo': 'codigo'}

	@csrf_exempt
	def tarjeta(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string'})
				if valido:
					usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
					if usuario:
						if request.method == 'POST':
							estatus, resp = Admin.registrarTarjeta(datos, usuario)
						elif request.method == 'GET':
							estatus, resp = Admin.saldoTarjeta(datos)
						elif request.method == 'PUT':
							estatus, resp = Admin.recargarTarjeta(datos, usuario)
						else:
							estatus, resp = stat.NOT_ALLOW, {'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}
						return enviarJson(resp, estatus)
					else:
						if permitido:
							return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
						else:
							return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def tarjetaVinculo(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string', 'codigo': 'string'})
				if valido:
					usuario, permitido = verfAcceso(datos['token'], APPS_VIE['TRANS_ADMIN'])
					if usuario:
						estatus, resp = Tarjeta.propietario(datos['codigo'])
						if estatus == stat.OK:
							resp = {'mensaje': 'Se encontro un dueño para la tarjeta', 'email': resp['email'], 'nombre': resp['nombre']}
						return enviarJson(resp, estatus)
					else:
						if permitido:
							return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
						else:
							return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)