import datetime
import logging

from django.utils					  import timezone
from django.views.decorators.csrf	  import csrf_exempt

import vie.tools.validar              as val
from vie.tools.errores	              import Status as stat
from vie.tools.estatus                import LECTOR_ESTATUS, TARJETA_ESTATUS
from vie.views.views	              import recibirJson, enviarJson
from vieTransporte.models             import Lector as LectorMdl, TarjetaVie as TarjetaMdl, TarjetaUsuario as TarjetaUMdl, TarjetaLigada, Pasaje
from vieTransporte.views.viewPasaje   import Pasaje
from vieTransporte.views.viewTarjeta  import Tarjeta

logger = logging.getLogger(__name__)

class Lector:

	@csrf_exempt
	def pasaje(request):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)
			error, campo, tipo = val.validarDatos(datos, {'mac':'string', 'trans': 'int', 'codigo': 'string'})
			if error:
				estatus, dato = Lector.validarLector(datos['mac'], True)
				if estatus == stat.OK:
					try:
						MAX_DESC = Pasaje.max_descuento()
						tarjeta = TarjetaMdl.objects.get(codigoTarj = datos['codigo'])
						if tarjeta.estatus == TARJETA_ESTATUS['ACTIVO']:
							if tarjeta.saldo >= MAX_DESC:
								costo = Pasaje.costo()
								tarjeta.saldo = tarjeta.saldo - costo
								tarjeta.fechaActualizacion = timezone.now()
								tarjeta.save(update_fields = ['saldo', 'fechaActualizacion'])
								Pasaje.save(dato.turno, tarjeta, datos['trans'], Pasaje.PAGADO, costo)
								return enviarJson({'mensaje': 'Pasaje registrado', 'saldo': tarjeta.saldo}, stat.OK)
							else:
								return enviarJson({'mensaje':'Saldo insuficiente', 'saldo': tarjeta.saldo}, stat.FORBIDDEN)
						elif tarjeta.estatus == TARJETA_ESTATUS['LIGADO']:
							liga = TarjetaUMdl.objects.get(tarjeta = tarjeta)
							if liga.cartera.saldo >= MAX_DESC:
								costo = Pasaje.costo()
								liga.cartera.saldo = liga.cartera.saldo - costo
								liga.cartera.fechaActualizacion = timezone.now()
								liga.cartera.save(update_fields = ['saldo', 'fechaActualizacion'])
								Pasaje.save(dato.turno, tarjeta, datos['trans'], Pasaje.PAGADO, costo)
								return enviarJson({'mensaje': 'Pasaje registrado', 'saldo': liga.cartera.saldo}, stat.OK)
							else:
								return enviarJson({'mensaje':'Saldo insuficiente', 'saldo': liga.cartera.saldo}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'El estado actual de la tarjeta no permite cobros', 'campo':'codigo'}, stat.NOT_ALLOW)
					except TarjetaMdl.DoesNotExist:
						return enviarJson({'mensaje': 'La tarjeta no se encuentra registrada', 'campo': 'codigo'}, stat.NOT_FOUND)
					except TarjetaUMdl.DoesNotExist:
						return enviarJson({'mensaje': 'La tarjeta no se encuentra ligada', 'campo': 'liga'}, stat.NOT_FOUND)
				else:
					return enviarJson(dato, estatus)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def saldo(request):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)
			error, campo, tipo = val.validarDatos(datos, {'mac':'string', 'tarjeta': 'string'})
			if error:
				estatus, resp = Lector.validarLector(datos['mac'])
				if estatus == stat.OK:
					estatus, resp = Tarjeta.saldo(datos['tarjeta'])
					if estatus == stat.OK:
						return enviarJson({'mensaje': 'El saldo de su tarjeta', 'saldo': str(resp)}, stat.OK)
					else:
						return enviarJson(resp, estatus)	
				else:
					return enviarJson(resp, estatus)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def sincronizar(request):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)
			logger.debug(datos)
			valido, campo, tipo = val.validarDatos(datos, {'mac':'string', 'fechaCreacion': ('string','none'), 'fechaActualizacion':('string', 'none')})
			if valido:
				estatus, dato = Lector.validarLector(datos['mac'])
				if estatus == stat.OK:
					#No tiene ninguna tarjeta en la base de datos
					if datos['fechaCreacion'] == None:
						tarjetas = TarjetaMdl.objects.filter(estatus = TARJETA_ESTATUS['ACTIVO'])
						lista = []
						for tarjeta in tarjetas:
							pos = {}
							pos['saldo'] = tarjeta.saldo
							pos['codigoTarj'] = tarjeta.codigoTarj
							lista.append(pos)
						#Tarjetas ligadas
						tarjetas = TarjetaLigada.objects.all()
						for tarjeta in tarjetas:
							pos = {}
							pos['saldo'] = tarjeta.saldo
							pos['codigoTarj'] = tarjeta.codigoTarj
							lista.append(pos)
						dato.ultimaSincro = timezone.now()
						dato.save(update_fields = ['ultimaSincro'])
						return enviarJson({'mensaje':'Lista de tarjetas y saldo', 'tarjetas': lista}, stat.OK)
					else:
						try:
							fechaCreacion = datetime.datetime.strptime(datos['fechaCreacion'], '%d-%m-%Y %H:%M:%S')
							fechaCreacion = fechaCreacion.replace(tzinfo=timezone.utc)
							#En caso de no tener ninguna tarjeta actualizada
							if datos['fechaActualizacion'] == None:
								fechaActualizacion = datetime.datetime.strptime('01-01-2000 00:00:00', '%d-%m-%Y %H:%M:%S')
								fechaActualizacion = fechaActualizacion.replace(tzinfo = timezone.utc)
							else:
								fechaActualizacion = datetime.datetime.strptime(datos['fechaActualizacion'], '%d-%m-%Y %H:%M:%S')
								fechaActualizacion = fechaActualizacion.replace(tzinfo = timezone.utc)
						except ValueError:
							return enviarJson({'mensaje': 'Formato de la fecha es invalido. Formato: dd-mm-yyyy HH:MM:SS'}, stat.FORBIDDEN)
						#Nuevas tarjetas a partir de la fecha de creacion
						
						tarjetas = TarjetaMdl.objects.filter(estatus = TARJETA_ESTATUS['ACTIVO'], fechaActualizacion__isnull = True, fechaReg__gte = fechaCreacion)
						lista = []
						for tarjeta in tarjetas:
							pos = {}
							pos['saldo'] = tarjeta.saldo
							pos['codigoTarj'] = tarjeta.codigoTarj
							lista.append(pos)
						#Tarjetas actualizadas a partir de la fecha de actualizacion
						tarjetas = TarjetaMdl.objects.filter(estatus = TARJETA_ESTATUS['ACTIVO'], fechaActualizacion__gte = fechaActualizacion)
						for tarjeta in tarjetas:
							pos = {}
							pos['saldo'] = tarjeta.saldo
							pos['codigoTarj'] = tarjeta.codigoTarj
							lista.append(pos)
						#Tarjetas ligadas
						tarjetas = TarjetaLigada.objects.filter(fechaActualizacion__gte = fechaActualizacion)
						for tarjeta in tarjetas:
							pos = {}
							pos['saldo'] = tarjeta.saldo
							pos['codigoTarj'] = tarjeta.codigoTarj
							lista.append(pos)
						dato.ultimaSincro = timezone.now()
						dato.save(update_fields = ['ultimaSincro'])
						return enviarJson({'mensaje':'Lista de tarjetas y saldo', 'tarjetas': lista}, stat.OK)
				else:
					return enviarJson(dato, estatus)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'mac'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def validarLector(mac, cobrando = False):		
		try:
			lector = LectorMdl.objects.get(macAddress = mac)
			if lector.estatus == LECTOR_ESTATUS['LIGADO']:
				if cobrando:
					if lector.turno:
						return stat.OK, lector
					else:
						return stat.NOT_FOUND, {'mensaje': 'Lector no tiene registrado un turno', 'campo':'turno'}
				else:
					return stat.OK, lector
			else:
				return stat.FORBIDDEN, {'mensaje': 'EL lector no esta ligado a una camioneta', 'campo':'estatus'}
		except LectorMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'Lector no se encuentra registrado'}
