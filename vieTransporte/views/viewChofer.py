import logging

from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone

import vie.tools.validar		    as val
from vie.models                     import Usuario as UsuarioMdl
from vieTransporte.models           import Turno as TurnoMdl, Camioneta as CamionetaMdl
from vie.tools.errores		        import Status as stat
from vie.tools.estatus              import APPS_VIE, EMPRESAS, TURNO_ESTATUS, USUARIO_ROLES
from vie.views.views	            import recibirJson, enviarJson, leerDatos, verfUsuario, debug, verfChofer
from vie.views.viewUsuario 			import Usuario
from vie.views.viewChofer			import Chofer as vChofer
from vieTaxi.views.viewAuto			import Automovil

logger = logging.getLogger(__name__)


class Chofer:
	"""
		Vista que controla opciones del Chofer.
	"""
	@csrf_exempt
	def listaTurnoActivos(request):
		try:
			if request.method  == 'GET':
				#Valor que se equivalente al primer momento de hoy
				hoy = timezone.localtime().replace(minute=0, hour=0, second=0, microsecond=0)
				turnos = TurnoMdl.objects.filter(estatus = TURNO_ESTATUS['SERVICIO'], fechaInicio__gte = hoy)
				lista = []
				resp = {
					'mensaje': 'Listado de choferes con turno activo'
				}
				for turno in turnos:
					chofer = turno.idChofer
					pos = {
						'camioneta': turno.idCamioneta_id,
						'nombre': chofer.nombre
					}
					try:
						usuario = UsuarioMdl.objects.get(idFila = turno.idChofer_id, permiso = USUARIO_ROLES['CHOFER'])
						if usuario.foto and len(usuario.foto) > 0:
							pos['foto'] = usuario.foto
					except UsuarioMdl.DoesNotExist:
						pass
					lista.append(pos)
				resp['choferes'] = lista
				return enviarJson(resp, stat.OK)
			else:
				return enviarJson({'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}, stat.NOT_ALLOW)
		except Exception as e: 
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def logout(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string'})
				if valido:
					usuario = verfUsuario(datos['token'], APPS_VIE['TRANS_CHOFER'])
					if usuario:
						logger.debug('Usuario '+str(usuario.idUsuario.email)+' Chofer '+str(usuario.idUsuario.idFila))
						#Valor que se equivalente al primer momento de hoy
						hoy = timezone.localtime().replace(minute=0, hour=0, second=0, microsecond=0)
						turno = TurnoMdl.objects.filter(estatus = TURNO_ESTATUS['SERVICIO'], idChofer = usuario.idUsuario.idFila, fechaInicio__gte = hoy).order_by('-fechaInicio').first()
						if turno:
							try:
								CamionetaMdl.objects.filter(idCamioneta = turno.idCamioneta_id).update(disponible = True)
							except CamionetaMdl.DoesNotExist:
								pass
							turno.estatus = TURNO_ESTATUS['TERMINADO']
							turno.fechaFin = timezone.now()
							turno.save(update_fields = ['estatus', 'fechaFin'])
						return Usuario.vieLogout(request, datos)
					else:
						return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			msj = str(e)
			logger.error(msj)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': msj}, stat.UNPROCESSABLE)

	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'GET':
			token = request.GET.get("token", None)
			if token:
			    return vChofer.vieTenerDatos(str(token), APPS_VIE['TRANS_CHOFER'])
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.UNPROCESSABLE)
		elif request.method == 'PUT':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return vChofer.modificar(datos, APPS_VIE['TRANS_CHOFER'])
		elif request.method == 'DELETE':
			return enviarJson({'mensaje': 'Enviaste un delete'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'El metodo HTTP no soportado', 'campo': 'http'}, stat.NOT_ALLOW)

	@csrf_exempt
	def turnoActivo(request):
		try:
			error, datos = leerDatos(request, True) 
			if error:
				return enviarJson(datos, error)
			else:
				valido, campo, tipo = val.validarDatos(datos, {'token':'string'})
				if valido:
					usuario = verfUsuario(datos['token'], APPS_VIE['TRANS_CHOFER'])
					if usuario:
						logger.debug('Usuario '+str(usuario.idUsuario.email)+' Chofer '+str(usuario.idUsuario.idFila))
						#Valor que se equivalente al primer momento de hoy
						hoy = timezone.localtime().replace(minute=0, hour=0, second=0, microsecond=0)
						turno = TurnoMdl.objects.filter(estatus = TURNO_ESTATUS['SERVICIO'], idChofer = usuario.idUsuario.idFila, fechaInicio__gte = hoy).order_by('-fechaInicio').first()
						if turno:
							return enviarJson({'mensaje': 'El chofer tiene un turno activo', 'trayecto': turno.idRuta.cuartoS}, stat.OK)
						else:
							return enviarJson({'mensaje': 'El chofer no tiene un turno activo', 'campo': 'turno'}, stat.FORBIDDEN)
					else:
						return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
				else:
					respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
					if tipo:
						respuesta['tipo'] = tipo
					return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			msj = str(e)
			logger.error(msj)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': msj}, stat.UNPROCESSABLE)


