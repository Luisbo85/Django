import xlsxwriter
import os.path as path
from xlsxwriter.utility 		import xl_range_abs
from django.views.generic 		import View
from django.http 				import HttpResponse
from django.db.models 			import Count
from vieTransporte.models		import Camioneta as CamionetaMdl, Turno as TurnoMdl, Pasaje as PasajeMdl
from viePay.views.viewRecarga   import Recarga
from datetime					import datetime,timedelta

class Reporte:

	def reporteDiario(datos):
		fecha=datos['fecha']

		archivo = 'static/reportes/Reporte'+fecha+'.xlsx'

		if path.exists('static/reportes/Reporte'+fecha+'.xlsx'):
			response = HttpResponse(open(archivo,'rb'),content_type='aplication/ms-excel')
			response['Content-Disposition']='attachment; filename=Reporte'+fecha+'.xlsx'
			return response

		else:
			wb=xlsxwriter.Workbook(archivo)
			sheet=wb.add_worksheet('Pasajes')
			sheet2=wb.add_worksheet('Recargas')
			sheet.insert_image(0,0,'media/reportes/vietransporte.jpeg')
			sheet2.insert_image(0,0,'media/reportes/vietransporte.jpeg')

			section_header_format=wb.add_format({
				'bold':True,
				'align':'left',
				'font_size':16,
				})

			section_total=wb.add_format({
				'bold':True,
				'align':'right',
				'font_size':16,
				})

			num_format=wb.add_format({
				'num_format': '0',
				'align':'left',
				'font_size':12,
				})

			general_format=wb.add_format({
				'align':'left',
				'font_size':12,
				})

			sheet.set_column(0,0,45, general_format)
			sheet.set_column(1,1,20, num_format)
			sheet.set_column(2,2,20, num_format)
			sheet.set_column(3,3,20, num_format)
			sheet.merge_range(6,0,6,1, 'Reporte VieTransporte', section_header_format)

			row = 8
			sheet.write('B1','Fecha:')
			sheet.write('C1',fecha)
			sheet.write('B9','Camioneta')
			sheet.write('C9','No. Pasajes')
			sheet.write('D9','Cobros')
			sheet.write('A22','Total',section_total)

			fecha=datetime.strptime(fecha, '%Y-%m-%d')
			dia=timedelta(days=1)
			fechaP=fecha+dia

			pasaje=PasajeMdl.objects.values('idTurno__idCamioneta_id').filter(fecha__gte=fecha, fecha__lte=fechaP).annotate(num_turnos=Count('idTurno')).order_by('idTurno__idCamioneta_id')

			num=0
			for campo in pasaje:
				row +=1
				sheet.write(row,1,pasaje[num]['idTurno__idCamioneta_id'])
				sheet.write(row,2,pasaje[num]['num_turnos'])
				num +=1

			fecha=datos['fecha']

			sheet.write_formula('D10','{C10*15}')
			sheet.write_formula('D11','{C11*15}')
			sheet.write_formula('D12','{C12*15}')
			sheet.write_formula('D13','{C13*15}')
			sheet.write_formula('D14','{C14*15}')
			sheet.write_formula('D15','{C15*15}')
			sheet.write_formula('D16','{C16*15}')
			sheet.write_formula('D17','{C17*15}')
			sheet.write_formula('D18','{C18*15}')
			sheet.write_formula('D19','{C19*15}')
			sheet.write_formula('D20','{C21*15}')
			sheet.write_formula('D21','{C21*15}')

			sheet.write_formula('C22','{=SUM(C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21)}')
			sheet.write_formula('D22','{=SUM(D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21)}')

			
			sheet2.set_column(0,0,45, general_format)
			sheet2.set_column(1,1,20, general_format)
			sheet2.set_column(2,2,20, general_format)
			sheet2.set_column(3,3,20, general_format)
			sheet2.merge_range(6,0,6,1, 'Reporte VieTransporte', section_header_format)

			row = 8
			sheet.write('B1','Fecha:')
			sheet.write('C1',fecha)
			sheet2.write('B9','Fecha')
			sheet2.write('C9','Monto')
			sheet2.write('D9','Referencia')

			
			fecha=datetime.strptime(fecha, '%Y-%m-%d')
			dia=timedelta(days=1)
			fechaP=fecha+dia
			recarga=Recarga.recarga(fecha)

			fecha=datos['fecha']

			num=0
			for campo in recarga:
				row +=1
				sheet2.write(row,1,fecha)
				sheet2.write(row,2,recarga[num]['monto'])
				sheet2.write(row,3,recarga[num]['referencia'])
				num +=1


			wb.close()

			response = HttpResponse(open(archivo,'rb'),content_type='aplication/ms-excel')
			response['Content-Disposition']='attachment; filename=Reporte'+fecha+'.xlsx'
			return response