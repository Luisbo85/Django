from django.views.decorators.csrf	import csrf_exempt
import json

# Create your views here.
from django.http 					import HttpResponse, HttpResponseNotFound

from vie.tools.errores				import Status
from Django.settings 				import DEBUG, EMPRESAS

@csrf_exempt
def index(request):
	"""
		Vista que muestra la pagina de inicia al sitio.
	"""
	html = """
			<html>
			<head>
			       <title>VieTransporte</title>
			</head>
			<body>
			       <h3>VieTransporte</h3>
			</body>
			</html>
			"""
	return HttpResponse(html)