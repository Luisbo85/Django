import logging

from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone

import vie.tools.validar		    as val
from vie.tools.estatus              import APPS_VIE, TARJETA_ESTATUS, T_LIGADA_ESTATUS
from vie.views.views			    import recibirJson, enviarJson, verfUsuario
from vie.tools.errores			    import Status as stat
from vie.models                     import Persona as PersonaMdl
from vieTransporte.models		    import TarjetaVie as TarjetaMdl, SaldoTarjeta, TarjetaUsuario as TarjetaUMdl, Pasaje
from viePay.models	                import MetodoPago as MetodoPagoMdl
from viePay.views.viewMetodoPago 	import MetodoPago

logger = logging.getLogger(__name__)

class Tarjeta:
	"""
		Vista que controla las opciones de tarjetas de vieTransporte
	"""
	@csrf_exempt
	def desligar(request, app = APPS_VIE['TRANS_CLIENTE']):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			if val.validarExistencia(datos, ['token', 'codigo']):
				user = verfUsuario(datos['token'], app)
				if user:
					try:
						tarj = TarjetaMdl.objects.get(codigoUsuario = datos['codigo'])
						try:
							existe = TarjetaUMdl.objects.get(tarjeta = tarj)
							if existe.estatus == T_LIGADA_ESTATUS['ACTIVO']:
								existe.estatus = T_LIGADA_ESTATUS['CANCELADO']
								existe.save(update_fields = ['estatus'])
								return enviarJson({'mensaje': 'La tarjeta ha sido dada de baja'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Tarjeta ya fue dada de baja'}, stat.FORBIDDEN)
						except TarjetaUMdl.DoesNotExist:
							return enviarJson({'mensaje': 'El usuario no esta ligado con la tarjeta indicada', 'campo':'cartera'}, stat.NOT_FOUND)
					except TarjetaMdl.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontro la tarjeta', 'campo': 'codigo'}, stat.NOT_FOUND)				
				else:
					return enviarJson({'mensaje': 'Token no valido!', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campos': 'token, codigo'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def historial(tarjeta):
		"""
			Historial de los pasajes realizados por un llavero
		"""
		msj = ''
		try:
			if len(tarjeta) == 6:
				tarj = TarjetaMdl.objects.get(codigoUsuario = tarjeta)
				pasajes = Pasaje.objects.filter(idTarjeta = tarj).order_by('-fecha')
				lista = []
				for pasaje in pasajes:
					pos = {}
					pos['fecha'] = pasaje.fecha
					lista.append(pos)
				return enviarJson({'mensaje': 'Listado de pasajes', 'historial': lista}, stat.OK)
			else:
				return enviarJson({'mensaje': 'El llavero no tiene 6 caracteres', 'campo': 'tarjeta'}, stat.FORBIDDEN)
		except TarjetaMdl.DoesNotExist:
			msj = "Tarjeta '"+str(tarjeta)+"' no encontrada"
			logger.debug(msj)
			return enviarJson({'mensaje': msj, 'campo': 'tarjeta'}, stat.NOT_FOUND)
		except Exception as e:
			msj = str(e)
			logger.error(msj)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': msj}, stat.UNPROCESSABLE)

	@csrf_exempt
	def ligarTarjeta(request, app = APPS_VIE['TRANS_CLIENTE']):
		try:
			datos = recibirJson(request)

			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo':'json'}, stat.BAD_REQUEST)

			valido, campo, tipo = val.validarDatos(datos, {'token':'string', 'codigo': 'string'})
			if valido:
				user = verfUsuario(datos['token'], app)
				if user:
					try:
						tarj = TarjetaMdl.objects.get(codigoUsuario = datos['codigo'])
						try:
							existe = TarjetaUMdl.objects.get(tarjeta = tarj)
							return enviarJson({'mensaje':'La tarjeta ya está registrada', 'campo': 'tarjeta'}, stat.FORBIDDEN)

						except TarjetaUMdl.DoesNotExist:

							try:
								cartera = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario.idUsuario, tipo = MetodoPago.MetodoPagos['P_CARTERA'])
								cartera.saldo += tarj.saldo
								tarj.saldo = 0
								tarj.estatus = TARJETA_ESTATUS['LIGADO']

								cartera.save(update_fields = ['saldo'])
								tarj.save(update_fields = ['saldo', 'estatus'])

								tarjus = TarjetaUMdl(
									cartera = cartera,
									tarjeta =  tarj,
									fecha = timezone.now(),
									estatus = TARJETA_ESTATUS['ACTIVO']
								)
								tarjus.save()
								return enviarJson({'mensaje': 'La tarjeta ha sido registrada'}, stat.OK)
							except MetodoPagoMdl.DoesNotExist:
								return enviarJson({'mensaje': 'No se encontró carterta', 'campo':'cartera'}, stat.NOT_FOUND)
					except TarjetaMdl.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontro la tarjeta', 'campo': 'codigo'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'Token no valido!', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def mostrar(request, app = APPS_VIE['TRANS_CLIENTE']):
		try:
			datos = recibirJson(request)

			if 'token' in datos:
				user = verfUsuario(datos['token'], app)

				if user:
					try:
						meth = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario, tipo = MetodoPago.MetodoPagos['P_CARTERA'])
						tarjs_user = TarjetaUMdl.objects.filter(cartera = meth)
						
						lista = []
						for item in tarjs_user:
							campos = {}
							campos['tarjeta'] = item.tarjeta.codigoUsuario
							campos['estatus'] = item.estatus
							campos['fecha'] = item.fecha
							lista.append(campos)

						return enviarJson({'mensaje':'Lista de tarjetas del usuario','tarjetas': lista}, stat.OK)
					except MetodoPagoMdl.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontró Método de Pago', 'campo': 'cartera'}, stat.NOT_FOUND)
					except TarjetaUMdl.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontraron tarjetas', 'campo': 'tarjeta'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'El token no es válido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Falta el token', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def propietario(codigo):
		try:
			if val.validarCodigo(codigo):
				tarjeta = TarjetaMdl.objects.get(codigoUsuario = codigo)
				if tarjeta.estatus == TARJETA_ESTATUS['ACTIVO']:
					return stat.NOT_FOUND, {'mensaje': 'No tiene un propietario', 'campo':'propietario'}
				elif tarjeta.estatus == TARJETA_ESTATUS['LIGADO']:
					liga = TarjetaUMdl.objects.get(tarjeta = tarjeta)
					usuario = liga.cartera.idUsuario
					persona = PersonaMdl.objects.get(idPersona = usuario.idFila)
					resp = {
						'email': usuario.email,
						'nombre': persona.nombre + ' ' + persona.apellidos
					}
					return stat.OK, resp
				else:
					respuesta, estatus = {'mensaje': 'El estado actual de la tarjeta no permite recargas', 'campo':'codigo'}, stat.NOT_ALLOW
			else:
				return stat.FORBIDDEN, {'mensaje': 'Formato del codigo no es correcto', 'campo':'codigo'}
		except TarjetaMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no fue encontrada', 'campo': 'codigo'}
		except TarjetaUMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no se encuentra ligada a un usuario', 'campo': 'liga'}
		except PersonaMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'No se encontro la persona dueña de la tarjeta', 'campo':'persona'}
		except Exception as e:
			logger.error(e)
			return stat.UNPROCESSABLE, {'mensaje':'Problemas con el servidor', 'campo':'servidor', 'error':str(e)}

	def recargar(codigo, cantidad, usuario):
		try:
			if cantidad > 0 and cantidad < 2000:
				if not val.validarCodigo(codigo):
					return stat.FORBIDDEN, {'mensaje': 'Formato del codigo es invalido', 'campo': 'codigo'}
				tarjeta = TarjetaMdl.objects.get(codigoUsuario = codigo)
				recargar = False
				if tarjeta.estatus == TARJETA_ESTATUS['ACTIVO']:
					recargar = tarjeta
				elif tarjeta.estatus == TARJETA_ESTATUS['LIGADO']:
					liga = TarjetaUMdl.objects.get(tarjeta = tarjeta)
					recargar = liga.cartera
				else:
					respuesta, estatus = {'mensaje': 'El estado actual de la tarjeta no permite recargas', 'campo':'codigo'}, stat.NOT_ALLOW
				if recargar:
					hoy = timezone.now()
					saldoAnterior = recargar.saldo 
					recargar.saldo = recargar.saldo + cantidad
					recargar.fechaActualizacion = hoy
					recargar.save(update_fields = ['saldo', 'fechaActualizacion'])
					saldoTar = SaldoTarjeta(
						tarjeta = tarjeta,
						saldoAnterior = saldoAnterior,
						saldoInicio = recargar.saldo,
						fecha = hoy,
						recargador = usuario
					)
					saldoTar.save()
					return stat.OK, recargar.saldo
				else:
					return estatus, respuesta
			else:
				return stat.FORBIDDEN, {'mensaje': 'La cantidad ingresada no esta en un rango valido', 'campo': 'cantidad'}
		except TarjetaMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no fue encontrada', 'campo': 'codigo'}
		except TarjetaUMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no se encuentra ligada a un usuario', 'campo': 'liga'}
		except Exception as e:
			return stat.UNPROCESSABLE, {'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}

	def registrarTarjeta(datos, usuario):
		try:
			valido, campo, tipo = val.validarDatos(datos, {'chip':'string', 'codigo': 'string', 'saldo': 'int'})
			if valido:
				logger.debug(datos)
				if datos['saldo'] in (50, 100, 200, 500):
					try:
						chip = datos['chip']
						if not val.validarChip(chip):
							return stat.FORBIDDEN, {'mensaje': 'Longitud incorrecta. Max 12', 'campo':'chip'}
						codigo = datos['codigo'].upper()
						if not val.validarCodigo(codigo):
							return stat.FORBIDDEN, {'mensaje': 'Formato no valido para el codigo', 'campo':'codigo'}
						tar = TarjetaMdl.objects.get(codigoTarj = chip)
						tar2 = TarjetaMdl.objects.get(codigoUsuario = codigo)
						if tar:
							return stat.FORBIDDEN, {'mensaje': 'El chip ya esta registrado', 'campo': 'chip'}
						if tar2:
							return stat.FORBIDDEN, {'mensaje': 'El codigo ya está registrado', 'campo': 'codigo'}
					except TarjetaMdl.DoesNotExist:
						hoy = timezone.now()
						tarjeta = TarjetaMdl(
							codigoTarj = chip,
							codigoUsuario = codigo,
							estatus = TARJETA_ESTATUS['ACTIVO'],
							saldo = datos['saldo'],
							fechaReg = hoy,
							fechaActualizacion = hoy,
							registrador = usuario
						)
						tarjeta.save()
						saldoTar = SaldoTarjeta(
							tarjeta = tarjeta,
							saldoAnterior = 0,
							saldoInicio = tarjeta.saldo,
							fecha = hoy,
							recargador = usuario
						)
						saldoTar.save()
						return stat.OK, None
				else:
					return stat.FORBIDDEN, {'mensaje': 'El saldo no es valido', 'campo': 'saldo'}
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return stat.BAD_REQUEST, respuesta
		except Exception as e:
			logger.error(e)
			return stat.UNPROCESSABLE, {'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}

	def saldo(codigo, esChip = True):
		try:
			if esChip:
				if val.validarChip(codigo):
					tarjeta = TarjetaMdl.objects.get(codigoTarj = codigo)
				else:
					return stat.FORBIDDEN, {'mensaje': 'Formato invalido del codigo', 'campo': 'chip'}
			else:
				if val.validarCodigo(codigo):
					tarjeta = TarjetaMdl.objects.get(codigoUsuario = codigo)
				else:
					return stat.FORBIDDEN, {'mensaje': 'Formato invalido del codigo', 'campo': 'codigo'}	
			if tarjeta.estatus == TARJETA_ESTATUS['ACTIVO']:
				respuesta, estatus = tarjeta.saldo, stat.OK
			elif tarjeta.estatus == TARJETA_ESTATUS['LIGADO']:
				liga = TarjetaUMdl.objects.get(tarjeta = tarjeta)
				respuesta, estatus = liga.cartera.saldo, stat.OK
			else:
				respuesta, estatus = {'mensaje': 'El estado actual de la tarjeta no permite cobros', 'campo':'codigo'}, stat.NOT_ALLOW
			return estatus, respuesta
		except TarjetaMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no se encuentra registrada', 'campo': 'codigo'}
		except TarjetaUMdl.DoesNotExist:
			return stat.NOT_FOUND, {'mensaje': 'La tarjeta no se encuentra ligada a un usuario', 'campo': 'liga'}
		except Exception as e:
			logger.error(e)
			return stat.UNPROCESSABLE, {'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}

	
