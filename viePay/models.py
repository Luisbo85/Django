from django.db import models
from vie.models	import Usuario, Chofer

# Create your models here.

class MetodoPago (models.Model):
	"""
		Modelo del metdo de pago, para determinar y almacenar el metodo de pago usado.
	"""
	idMetodoPago	    = models.AutoField			(primary_key= True)
	idUsuario		    = models.ForeignKey			(Usuario, on_delete = models.CASCADE)
	metodoConekta 	    = models.CharField			(max_length = 40, null = True)
	#Numero que hace referencia al metodo de pago
	tipo			    = models.SmallIntegerField	(default	= 0)
	#Cadena que relaciona el metodo de pago con Conekta
	token			    = models.CharField			(max_length = 40, null = True)
	tarjeta			    = models.CharField			(max_length = 20, null = True)
	#Ultimos digitos de la tarjeta
	codigo 			    = models.CharField			(max_length = 4,  null = True)
	promocion 		    = models.CharField			(max_length = 19, null = True) 
	#Nombre que el cliente le da a la forma de pago 
	alias			    = models.CharField			(max_length = 40, null = True, blank = True, default = 'EFECTIVO')
	saldo			    = models.DecimalField		(max_digits = 10, decimal_places = 2, default = 0)
	eliminado		    = models.BooleanField		(default	= False)
	fechaActualizacion  = models.DateTimeField    (auto_now_add = True, null = True)
	metodoDefault	    = models.BooleanField		(default 	= False)

	def __str__ (self):
		if self.tipo == 0:
			return 'Efectivo ' + str(self.idMetodoPago)
		elif self.tipo == 1:
			return 'Debito ' + str(self.idMetodoPago)
		elif self.tipo == 2:
			return 'Credito ' + str(self.idMetodoPago)
		elif self.tipo == 3:
			return 'Cartera ' + str(self.idMetodoPago)
		elif self.tipo == 4:
			return 'Paypal ' + str(self.idMetodoPago)
		elif self.tipo == 5:
			return 'Promocion ' + str(self.idMetodoPago)
		elif self.tipo == 6:
			return 'TARJETA VIETRANS ' + str(self.idMetodoPago)
		elif self.tipo == 7:
			return 'OXXO ' + str(self.idMetodoPago)
		else:
			return 'hmmm...'

class Pago(models.Model):
	"""
		Modelo para manejar el historial de pagos recibidos
	"""
	idPago	    = models.AutoField         (primary_key = True)
	idMetodoPago = models.ForeignKey        (MetodoPago, on_delete = models.CASCADE, null = True)
	idOrden 	= models.CharField 			(max_length = 40, null = True)
	IDvie		= models.CharField 			(max_length = 50, null = True)
	empresa      = models.SmallIntegerField (default = 0) #Empresa a la que esta destinado el pago
	tipo        = models.SmallIntegerField (default = 0) # En caso de subscripcion
	descripcion = models.CharField         (max_length = 400)
	fecha       = models.DateTimeField     (auto_now_add = True)
	estatus     = models.SmallIntegerField (default = 0)
	cantidad    = models.DecimalField	   (max_digits	= 10, decimal_places = 3, default = 0)
	referencia 	= models.CharField         (max_length = 50, null = True)
	numEvento	= models.CharField         (max_length = 25, db_index = True, null = True)
	comision	= models.DecimalField      (max_digits = 6, decimal_places = 2, default = 0)

class CodigoPromocion(models.Model):
	"""
		Modelo para manejar los codigo de promocion que se usen en el sistema. Permite registrarlos y sabes cuales estan activos o no
	"""
	idCodigo    = models.AutoField         (primary_key = True)
	codigo      = models.CharField         (max_length = 20)
	tipo        = models.SmallIntegerField (default = 0) #Indica si es de uso multiple o unico
	estatus     = models.SmallIntegerField (default = 0)
	fechaInicio = models.DateTimeField     (auto_now_add = False)
	fechaFin    = models.DateTimeField     (auto_now_add = False)
	cantidad    = models.DecimalField      (max_digits = 10, decimal_places = 3, default = 0)

class PromocionHistorial(models.Model):
	"""
		Modelo para manejar los usos del código.
	"""
	idPromUso	= models.AutoField		(primary_key = True)
	promocion 	= models.CharField		(max_length = 19, null = False)
	idUsuario	= models.ForeignKey		(Usuario, on_delete = models.CASCADE)
	fecha 		= models.DateTimeField	(auto_now_add = True)

class ChoferDeposito(models.Model):
	"""
		Modelo para registrar los pagos de los choferes
	"""
	idDeposito = models.AutoField     (primary_key = True)
	chofer     = models.ForeignKey    (Chofer, on_delete = models.CASCADE)
	fecha      = models.DateTimeField (auto_now_add = True)
	cantidad   = models.DecimalField  (max_digits = 12, decimal_places = 2, default = 0)

class Recarga(models.Model):
	"""
		Modelo para representar la cartera.
	"""
	idRecarga	= models.AutoField 		(primary_key = True)
	idUsuario	= models.ForeignKey		(Usuario, on_delete = models.CASCADE)
	estatus 	= models.SmallIntegerField 	(default = 00)
	monto 		= models.DecimalField	(max_digits = 10, decimal_places = 3, default = 0)
	fecha 	 	= models.DateTimeField 	(auto_now_add = False)
	referencia 	= models.CharField      (max_length = 50, null = True)
	empresa 	= models.SmallIntegerField (default = -1)
	origen 		= models.SmallIntegerField (default = -1)