from django.http 				    import HttpResponse
from Django.settings                import APPS_VIE
from viePay.models					import Recarga as RecargaMdl
from datetime						import datetime,timedelta

class Recarga:

	def recarga(fecha):
		fecha=fecha.replace(minute=0, hour=0, second=0, microsecond=0)
		dia=timedelta(days=1)
		fechaP=fecha+dia
		return RecargaMdl.objects.values('monto','referencia').filter(fecha__gte=fecha, fecha__lte=fechaP)