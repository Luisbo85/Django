from decimal import Decimal

from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from Django.settings 				import EMPRESAS

from vie.tools.errores 				import Status as stat
import vie.tools.validar			as val
from viePay.models 					import MetodoPago as MetodoPagoMdl, Pago as PagoMdl, Recarga
from vie.views.views				import recibirJson, enviarJson, verfUsuario, debug
from vie.tools.SMS 					import SMS
from vie.models						import Usuario as UsuarioMdl, Persona as PersonaMdl

class Pago:
	"""
		Clase para administrar registrar los pagos realiazados al sistema
	"""
	#Estatus de los Pagos
	ESTATUS = {
		'ESPERANDO': 0,
		'PAGADO' : 1,
		'RECHAZADO' : 2,
		'SUB_PAGADA' : 3,
		'SUB_FALLADA' : 4
	}

	#Status Conekta
	STAT_CONEKCTA = {
		'payment_pending': 0,
		'paid': 1,
		'declined': 2,
		'pre_authorized': 3,
		'partially_refunded': 4,
		'refunded': 5,
		'charged_back': 6,
		'voided': 7,
		'expired': 8

	}
	#
	COMISION = 0
	COMISION_OXXO = 10

	"""
		empresa. Hace referencia hacia qué empresa se dirige el pago (VieTaxi, ViePay, VieTransporte).
		IDvie. ID del viaje.
		orden. Orden creada por conekta.
		comision. comisión que se cobrará del viaje.
		estado. estado del pago.
		tipo. tipo de pago que se realiza.
		cantidad. Monto del cobro.
		idMetodoPago. ID del método de pago vie.
		referencia. Referencia de Oxxo o método de pago conekta.
		descripción. Datos adicionales que quieran añadirse.
	"""

	@csrf_exempt
	def registrarPago(empresa, IDvie, orden, comision, estado, tipo, cantidad, idMetodoPago, referencia = '', descripcion = ''):
		try:
			if empresa == EMPRESAS['VIETAXI'] or empresa == EMPRESAS['VIEPAY']:
				if estado in Pago.ESTATUS.values():
					from viePay.views.viewMetodoPago	import MetodoPago

					pago = PagoMdl(
						idMetodoPago = idMetodoPago,
						empresa = empresa,
						IDvie = IDvie,
						comision = Decimal.from_float(comision),
						idOrden = orden,
						tipo = tipo,
						descripcion = descripcion,
						fecha = timezone.now(),
						estatus = estado,
						cantidad = cantidad,
						referencia = referencia
					)
					pago.save()
					
					return pago

				else:
					return enviarJson({'mensaje': 'Estatus no reconocido', 'campo': 'estatus'}, stat.UNPROCESSABLE)
			
			elif empresa == EMPRESAS['VIETRANSPORTE']:
				pass
			else:
				return enviarJson({'mensaje': 'Empresa no valida', 'campo': 'empresa'}, stat.UNPROCESSABLE)
			return False
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Empresa no valida', 'campo': 'pago'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def regRecarga(usuario, estado, cantidad, ref, empresa, origen):
		"""
			usuario. Id del usuario a guardar.
			cantidad. Cantida de la recarga.
			ref. Referencia del tipo de pago con el que se recarga (ref. oxxo/idMetodo de Conekta).
			empresa. Empresa a la que va dirigida.
			origen. Empresa de donde se realizó la recarga.
			ref.charges[0].payment_method.reference
		"""
		if empresa == EMPRESAS['VIEPAY']:
			from viePay.views.viewMetodoPago	import MetodoPago
			rec = Recarga(
				idUsuario = usuario,
				estatus = estado,
				monto = cantidad,
				fecha = timezone.now(),
				referencia = ref,
				empresa = empresa,
				origen = origen
			)

			return rec
			#return Pago.registrarPago(empresa, None, oxxo.id, Pago.COMISION,Pago.ESTATUS['ESPERANDO'], MetodoPago.MetodoPagos['P_OXXO'],cantidad, None, oxxo.charges[0].payment_method.reference, 'VieTaxi')
		else:
			return enviarJson({'mensaje': 'Empresa no valida', 'campo': 'empresa'}, stat.FORBIDDEN)

	@csrf_exempt
	def mostrarPagos(request):
		
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if 'token' in datos:
			tok = verfUsuario(datos['token'])
			if tok:
				listPago = PagoMdl.objects.filter(idUsuario = tok.idUsuario)
				lista = []
				for uno in listPago:
					campos = {}
					campos['id'] = uno.idMetodoPago
					campos['tipo'] = uno.tipo
					campos['notarj'] = uno.codigo
					campos['alias'] = uno.alias
					campos['saldo']  = uno.saldo
					campos['tipo_tarj'] = uno.tarjeta
					if not uno.eliminado:
						lista.append(campos)
				debug (lista)
				rest = str(tok.idUsuario.idUsuario)
				return enviarJson({'pagos': lista}, stat.OK)
			else:
				return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'}, stat.BAD_REQUEST)

	def recargaCartera(usuario, cantidad):
		if not usuario == None and not cantidad == None:
			from viePay.views.viewMetodoPago	import MetodoPago
			try:
				cartera = MetodoPagoMdl.objects.get(idUsuario = usuario.idUsuario, tipo = MetodoPago.MetodoPagos['P_CARTERA'])
				cartera.saldo =  cartera.saldo + Decimal.from_float(cantidad) + Pago.COMISION_OXXO # Pesos por la comision de OXXO
				cartera.save(update_fields = ['saldo'])
				debug("Recargado en cartera!")
			except MetodoPagoMdl.DoesNotExist:
				return enviarJson({'mensaje': 'No se encotró el método de pago', 'campo':'token'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Datos erroneos'}, stat.BAD_REQUEST)

	def webRecarga(empresa, IDvie, orden, comision, estado, tipo, cantidad, idMetodoPago = None, referencia = '', concepto = ''):
		try:
			cantidad = cantidad/100
			comision = comision/100
			pago = Pago.registrarPago(empresa, IDvie, orden, comision,estado, tipo, cantidad, idMetodoPago,referencia, concepto)
								
			rec = Recarga.objects.get(idRecarga = IDvie)
			rec.estatus = estado
			rec.save(update_fields = ['estatus'])

			if estado == Pago.ESTATUS['PAGADO']:
				Pago.recargaCartera(rec.idUsuario, cantidad)
				if Pago.confirmacionsms(rec.idUsuario.idFila, cantidad):
					debug("SmS Envidado de confirmacion!")
				else:
					debug("No pudo enviarse el SMS de confirmacion")
		except Recarga.DoesNotExist:
			debug('No existe el numero de recarga indicada '+str(IDvie))

	def confirmacionsms(usuario, cantidad):
		debug(usuario)
		try:
			pers = PersonaMdl.objects.get(idPersona = usuario)

			sms = SMS()
			if sms.confirmacionRecarga(cantidad, pers.telefono):
				return True

		except PersonaMdl.DoesNotExist as e:
			debug(e)
			return False
