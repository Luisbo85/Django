# Create your views here.
from django.http 					import HttpResponse


def index(request):
	"""
		Vista que muestra la pagina de inicia al sitio.
	"""
	html = """
			<html>
			<head>
			       <title>ViePay</title>
			</head>
			<body>
			       <h3>ViePay</h3>
			</body>
			</html>"""
	return HttpResponse(html)