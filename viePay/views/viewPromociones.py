from django.views.decorators.csrf	import csrf_exempt
from vie.tools.errores 					import Status as stat
from viePay.models 					import CodigoPromocion, PromocionHistorial, MetodoPago
from django.utils					import timezone
from django.utils.crypto			import get_random_string
from vie.views.views				import recibirJson, enviarJson, verfUsuario, debug
from datetime						import datetime
import vie.tools.validar as val
import pytz

class Promociones:

	@csrf_exempt
	def generarPromocion(request):

		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		debug (datos)

		debug (timezone.now())

		if val.validarExistencia(datos,['cantidad', 'tipo', 'fechaFin']):
			if datos['cantidad'] > 0:
				try:
					fin = datetime.strptime(datos['fechaFin'],  "%Y-%m-%d %H:%M")
				except:
					return enviarJson({'mensaje': 'Formato fecha incorrecto', 'campo': 'fechaFin'}, stat.BAD_REQUEST)
				n_fin = fin.replace(tzinfo = pytz.UTC)
				debug ("fecha nueva: " + str(n_fin))
				if 'fechaInicio' in datos:
					try:
						inicio = datetime.strptime(datos['fechaInicio'],  "%Y-%m-%d %H:%M")
					except:
						return enviarJson({'mensaje': 'Formato fecha incorrecto', 'campo': 'fechaInicio'}, stat.BAD_REQUEST)
					n_inicio = inicio.replace(tzinfo = pytz.UTC)
					debug("fecha inicio: " + str(n_inicio))
					if n_inicio < timezone.now():
						return enviarJson({'mensaje': 'Fecha invalida', 'campo': 'fechaInicio'}, stat.UNPROCESSABLE)
				else:
					n_inicio = timezone.now()

				if n_inicio < n_fin:
					promocion = CodigoPromocion(
						codigo = get_random_string(19),
						tipo = datos['tipo'],
						estatus = 0,
						fechaInicio = n_inicio,
						fechaFin = n_fin,
						cantidad = datos['cantidad']
						)
					promocion.save()
					return enviarJson({'mensaje': 'Promocion guardada'}, stat.OK)
				else:
					return enviarJson({'mensaje': 'Fecha Invalida', 'campo': 'fechaFin'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'La cantidad introducida es incorretca.'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan datos'}, stat.BAD_REQUEST)

	@csrf_exempt
	def hola (request):

		debug ("Esto es una prueba... tu fecha " + str(timezone.now()))
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
		try:
			date = datetime.strptime(datos['fechaFin'], "%Y-%m-%d %H:%M")
		except:
			return enviarJson({'mensaje': 'Formato fecha incorrecto', 'campo': 'fecha'}, 418)
		debug("fecha :v -> " + str(date))
		debug("tipo ._. ->" + str(type(date)))
		debug("Ahora .-. >" + str(timezone.now()))
		debug("Otro Ahora o.o ->" + str(datetime.today()))
		debug(datos)

		return enviarJson(datos, stat.OK)

	def pagoPromocion (request):

		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if "idMetodo" in datos:
			try:
				campos = {}
				pago = MetodoPago.object.get(idMetodoPago = datos['idMetodo'])
				campos['ID'] = pago.idMetodoPago
				campos['usuario'] = pago.idUsuario
				campos['tipo'] = pago.tipo
				campos['promo'] = pago.promocion

				promo = PromocionHistorial(
					promocion = pago.promocion,
					idUsuario = pago.usuario,
					fechaFin = timezone.now()
					)
				return enviarJson(campos, stat.OK)

			except MetodoPago.DoesNotExist:
				return enviarJson({'mensaje': 'No se encuentra el Metodo.'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Falta id'}, stat.BAD_REQUEST)