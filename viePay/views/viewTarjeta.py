from datetime import datetime, timedelta

from django.views.decorators.csrf	import csrf_exempt
from django.views.decorators.csrf	import csrf_exempt
from django.utils				    import timezone
from django.http 				    import HttpResponse
from Django.settings                import APPS_VIE

import vie.tools.validar		    as val
from vie.views.views			    import recibirJson, enviarJson, generarToken, debug, verfUsuario
from vie.tools.errores			    import Status as stat

from vieTransporte.views.viewTarjeta import Tarjeta as vTarjeta

class Tarjeta:

	@csrf_exempt
	def desligar(request):
		return vTarjeta.desligar(request, APPS_VIE['PAY'])

	@csrf_exempt
	def ligarTarjeta(request):
		return vTarjeta.ligarTarjeta(request, APPS_VIE['PAY'])

	@csrf_exempt
	def mostrar(request):
		return vTarjeta.mostrar(request, APPS_VIE['PAY'])

