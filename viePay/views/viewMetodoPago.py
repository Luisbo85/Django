from django.views.decorators.csrf 		import csrf_exempt
from django.utils					import timezone

from vie.tools.errores 					import Status as stat
from vie.tools.SMS 					import SMS
from viePay.models 						import MetodoPago as MetodoPagoMdl
from vie.models							import Persona
from viePay.models						import Recarga as RecargaMdl
from vie.views.views					import recibirJson, enviarJson, verfUsuario, debug
import vie.tools.validar as val
from viePay.views.viewConekta			import PagoConekta
from viePay.views.viewPagos				import Pago
from vie.views.viewUsuario 				import Usuario
from Django.settings					import EMPRESAS, APPS_VIE

class MetodoPago:
	"""
		Clase para administrar los metodos de MetodoPago
	"""
	#Formas de pago
	MetodoPagos = {
		'P_EFECTIVO': 0,
		'P_DEBITO': 1,
		'P_CREDITO': 2,
		'P_CARTERA': 3,
		'P_PAYPAL': 4,
		'P_PROMO': 5,
		'P_VIETRANS': 6,
		'P_OXXO' : 7,
		'P_TRANS': 8,
		'P_SUBSCR': 9
	}

	PagoInterp = {
		0 : 'P_EFECTIVO',
		1 : 'P_DEBITO',
		2 : 'P_CREDITO',
		3 : 'P_CARTERA',
		4 : 'P_PAYPAL',
		5 : 'P_PROMO',
		6 : 'P_VIETRANS',
		7 : 'P_OXXO',
		8 : 'P_TRANS',
		9 : 'P_SUBSCR'	
	}

	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return MetodoPago.agregar(datos, APPS_VIE['PAY'])
		elif request.method == 'GET':
			q = request.GET.get("token", None)
			metodo = request.GET.get("metodo", None)
			if q and metodo:
			    return MetodoPago.mostrar(q, metodo, APPS_VIE['PAY'])
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token,metodo'}, stat.BAD_REQUEST)
		elif request.method == 'PATCH':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			debug(datos)
			return Pago.modificar(datos, APPS_VIE['PAY'])
		else:
			return enviarJson({'mensaje': 'El metodo HTTP no se reconoce', 'campo': 'metodo HTTP'}, stat.NOT_ALLOW)

	def pag_predeterminado(usuario, alias = 'Efectivo'):
		"""
			Registrar ciertos metodos de pago que van estar por default en el usuario 
		"""
		if not usuario == None:
			debug ("Persona: " + str(usuario))
			meth_pago = MetodoPagoMdl (
				tipo = MetodoPago.MetodoPagos['P_EFECTIVO'],
				idUsuario = usuario,
				alias = alias,
				metodoDefault = True
			)
			meth_pago.save()

			meth_2 = MetodoPagoMdl(
				tipo = MetodoPago.MetodoPagos['P_CARTERA'],
				idUsuario = usuario,
				alias = 'Cartera',
				metodoDefault = False				
			)
			meth_2.save()
			return enviarJson({'mensaje': 'Todo chido! :D'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'No se inserto persona', 'campo': 'persona'}, stat.BAD_REQUEST)

	@csrf_exempt
	def agregar(datos, app):
		try:
			error, campo, tipo = val.validarDatos(datos, {'token':'string', 'alias': 'string', 'tipo': 'string'})
			if error:
				tok = verfUsuario(datos['token'], app)
				if tok:
					if not tok.idUsuario.permiso == Usuario.CLIENTE:
						return enviarJson({'mensaje': 'No ha terminado su registro', 'campo': 'token'}, stat.UNAUTHORIZED)
					nomb = datos['alias']
					if datos['tipo'] in MetodoPago.MetodoPagos:
						meth_pago = MetodoPagoMdl (
							idUsuario = tok.idUsuario,
							tipo = MetodoPago.MetodoPagos[datos['tipo']],
							alias = datos['alias']
							)
						debug("Metodo de pago ->" + str(meth_pago.tipo))
						debug(MetodoPago.MetodoPagos[datos['tipo']])
						debug("ID clienteConekta: " + tok.idUsuario.clienteID)
						if meth_pago.tipo == MetodoPago.MetodoPagos['P_CREDITO'] or meth_pago.tipo == MetodoPago.MetodoPagos['P_DEBITO']:
							error, campo, tipo = val.validarDatos(datos, {'tarjeta':'string', 'codigo': 'string', 'tipo_tarj': 'string'})
							if error:
								meth_pago.token = datos['tarjeta']							
								meth_pago.codigo = datos['codigo']
								meth_pago.tarjeta = datos['tipo_tarj']
								meth_pago.metodoConekta = PagoConekta.metodo(datos['tarjeta'], tok.idUsuario.clienteID)
							else:
								respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
								if tipo:
									respuesta['tipo'] = tipo
								return enviarJson(respuesta, stat.BAD_REQUEST)
						else:
							return enviarJson({'mensaje': 'Tipo de pago no es soportado para registro', 'campo':'tipo'}, stat.FORBIDDEN)
						meth_pago.save()
						return enviarJson({'mensaje': 'Pago agregado'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'El tipo de pago no es valido', 'campo': 'tipo'}, stat.FORBIDDEN)
				else:
					return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)
	
	@csrf_exempt
	def pagar(request, datos = {}, empresa = EMPRESAS['VIEPAY']):

		try:
			debug("entre a pagar...")

			if len(datos) == 0:
				debug("voy request")
				datos = recibirJson(request)

			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST), False

			# Agregar más verificaciones de usuario
			# Token del usuario, cantidad a pagar, ID del método, concepto de pago, ID del viaje o recarga
			error, campo, tipo = val.validarDatos(datos, {'token':'string', 'cantidad':'float','idmetodo':'int'})
			if error:
				debug(datos)
				#Verifcar esta parte!!
				user = verfUsuario(datos['token'], APPS_VIE['PAY'])

				if user:
					if not user.idUsuario.permiso == Usuario.CLIENTE:
						return enviarJson({'mensaje': 'El usuario no puede realizar esta accion', 'campo': 'token'}, stat.UNAUTHORIZED)
					try:
						idMetodoPago = int(datos['idmetodo'])
						mPago = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario.idUsuario, idMetodoPago = idMetodoPago)
						if empresa == EMPRESAS['VIETAXI']:
							debug("entre en Taxi")
							if 'viaje' in datos:
								if r.tipo == MetodoPago.MetodoPagos['P_CREDITO'] or mPago.tipo == MetodoPago.MetodoPagos['P_DEBITO']:
									debug("ID cliente -> " + user.idUsuario.clienteID)
									debug("source_method -> " + mPago.metodoConekta)
									orden = PagoConekta.orden(user.idUsuario.clienteID, datos['cantidad']*100, mPago.metodoConekta, datos['concepto'])
									Pago.registrarPago(empresa, datos['viaje'], orden.id, Pago.COMISION, Pago.STAT_CONEKCTA[orden.payment_status], MetodoPago.MetodoPagos['P_CREDITO'], datos['cantidad'], mPago, mPago.metodoConekta, datos['concepto'])
								
								elif mPago.tipo == MetodoPago.MetodoPagos['P_EFECTIVO']:
									Pago.registrarPago(empresa, datos['viaje'], None, Pago.COMISION, Pago.ESTATUS['PAGADO'], MetodoPago.MetodoPagos['P_EFECTIVO'], datos['cantidad'], mPago, None, datos['concepto'])
								
								elif mPago.tipo == Metodo.MetodoPagos['P_CARTERA']:
									MetodoPago.pagoCartera(empresa, datos)
							else:
								return enviarJson({'mensaje': 'No se detectó ID viaje', 'campo': 'viaje'}, stat.BAD_REQUEST)
						elif empresa == EMPRESAS['VIETRANSPORTE']:
							debug("entre en Trasporte")
							if mPago.tipo == Metodo.MetodoPagos['P_CARTERA']:
								MetodoPago.pagoCartera(empresa, )
								Pago.registrarPago(empresa,)
							else:
								return enviarJson({'mensaje': 'Forma de pago no valida', 'campo': 'metodopago'}, stat.UNAUTHORIZED)
						# Verificar con Jarillo si la empresa es correcta
						elif empresa == EMPRESAS['VIEPAY']:
							debug("entre en Pay")
							if mPago.tipo == MetodoPago.MetodoPagos['P_CREDITO'] or mPago.tipo == MetodoPago.MetodoPagos['P_DEBITO']:
								debug("ID cliente -> " + user.idUsuario.clienteID)
								debug("source_method -> " + mPago.metodoConekta)
								orden = PagoConekta.orden(user.idUsuario.clienteID, datos['cantidad']*100, mPago.metodoConekta, datos['concepto'])
								pag = Pago.registrarPago(empresa, datos['idvie'], orden.id, Pago.COMISION, Pago.STAT_CONEKCTA[orden.payment_status], MetodoPago.MetodoPagos['P_CREDITO'], datos['cantidad'], mPago, mPago.metodoConekta, datos['concepto'])

								debug("regresare un objeto")
								return pag
						else:
							return enviarJson({'mensaje': 'Empresa no valida', 'campo':'empresa'}, stat.UNPROCESSABLE)
					except MetodoPagoMdl.DoesNotExist as e:
						debug(e)
						return enviarJson({'mensaje': 'No se puede realizar el pago', 'campo': 'metodo'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.NOT_FOUND)

				return enviarJson({'mensaje': 'Pagado'}, stat.OK)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def agregarPagoSubsc(usuario, token, tipo, codigo):
		"""
			Metodo para poder agregar la forma de pago en la que se va a hacer los cargos de la subscripcion
		"""
		meth_pago = MetodoPagoMdl (
			idUsuario = usuario,
			tipo = tipo,
			alias = 'Subscripcion',
			token = token,
			metodoDefault = True,
			codigo = codigo
		)
		meth_pago.save()

	@csrf_exempt
	def pmostrarMetodoPagos(request):
		return MetodoPago.mostrarMetodoPagos(request, EMPRESAS['VIEPAY'], APPS_VIE['PAY'])

	@csrf_exempt
	def mostrarMetodoPagos(request, empresa, app):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			if 'token' in datos:
				tok = verfUsuario(datos['token'], app)
				if tok:
					if EMPRESAS['VIETRANSPORTE'] == empresa:
						listMetodoPago = MetodoPagoMdl.objects.filter(idUsuario = tok.idUsuario).exclude(tipo = MetodoPago.MetodoPagos['P_EFECTIVO']).exclude(tipo = MetodoPago.MetodoPagos['P_CARTERA'])
					else:
						if app == APPS_VIE['PAY']:
							debug("con filtro!")
							listMetodoPago = MetodoPagoMdl.objects.filter(idUsuario = tok.idUsuario).exclude(tipo = MetodoPago.MetodoPagos['P_VIETRANS']).exclude(tipo = MetodoPago.MetodoPagos['P_EFECTIVO']).exclude(tipo = MetodoPago.MetodoPagos['P_CARTERA'])
						else:	
							listMetodoPago = MetodoPagoMdl.objects.filter(idUsuario = tok.idUsuario).exclude(tipo = MetodoPago.MetodoPagos['P_VIETRANS'])
					lista = []
					for uno in listMetodoPago:
						campos = {}
						campos['id'] = uno.idMetodoPago
						campos['tipo'] = MetodoPago.PagoInterp[uno.tipo]
						campos['default'] = uno.metodoDefault
						campos['notarj'] = uno.codigo
						campos['alias'] = uno.alias
						campos['saldo']  = uno.saldo
						campos['tipo_tarj'] = uno.tarjeta
						if not uno.eliminado:
							lista.append(campos)
					#debug (lista)
					rest = str(tok.idUsuario.idUsuario)
					return enviarJson({'pagos': lista}, stat.OK)
				else:
					return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)


	@csrf_exempt
	def eliminar(request, empresa):

		datos = recibirJson(request)

		if val.validarExistencia(datos, ['token', 'idPago']):
			user = verfUsuario(datos['token'])

			if user:
				try:
					if empresa == MetodoPago['P_VIETRANS']:
						m_pago = MetodoPagoMdl.objects.exclude(tipo = [MetodoPago.MetodoPagos['P_EFECTIVO'], MetodoPago.MetodoPagos['P_CARTERA']]).get(idMetodoPago = datos['idPago'], idUsuario = user.idUsuario)
					else:
						m_pago = MetodoPagoMdl.objects.exclude(tipo = [MetodoPago.MetodoPagos['P_VIETRANS']]).get(idMetodoPago = datos['idPago'], idUsuario = user.idUsuario)
				except MetodoPagoMdl.DoesNotExist:
					return enviarJson({'mensaje': 'No se puede eliminar', 'campo': 'token, idPago'}, stat.UNPROCESSABLE)

				m_pago.eliminado = True
				m_pago.save()
				return enviarJson({'mensaje': 'Se ha eliminado'}, stat.OK)

			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': ['token', 'idPago']}, stat.BAD_REQUEST)


	@csrf_exempt
	def modificar(request, empresa):

		#Verificar este método para dar más seguridad en cuando al IdPago
		datos = recibirJson(request)

		if val.validarExistencia(datos, ['token', 'idPago']):
			user = verfUsuario(datos['token'])

			if user:
				try:
					if empresa == MetodoPago['P_VIETRANS']:
						m_pago = MetodoPagoMdl.objects.exclude(tipo = [MetodoPago.MetodoPagos['P_EFECTIVO'], MetodoPago.MetodoPagos['P_CARTERA']]).get(idMetodoPago = datos['idPago'], idUsuario = user.idUsuario)
					else:
						m_pago = MetodoPagoMdl.objects.exclude(tipo = [MetodoPago.MetodoPagos['P_VIETRANS']]).get(idMetodoPago = datos['idPago'], idUsuario = user.idUsuario)
				except MetodoPagoMdl.DoesNotExist:
					return enviarJson({'mensaje': 'No se puede modificar'}, stat.UNPROCESSABLE)

				if 'promocion' in datos:
					if m_pago.tipo == 'P_PROMO':
						m_pago.promocion = datos['promocion']
					else:
						return enviarJson({'mensaje': 'No puede modificar este campo', 'campo': 'promocion'}, stat.UNPROCESSABLE)
				if 'alias' in datos:
					m_pago.alias = datos['alias']

				if 'default' in datos:
					listMetodoPago = MetodoPagoMdl.objects.filter(idUsuario = user.idUsuario)

					for item in listMetodoPago:
						item.metodoDefault = False
						item.save(update_fields = ['metodoDefault'])
					m_pago.metodoDefault = datos['default']

				m_pago.save()
				return enviarJson({'mensaje': 'Modificado'}, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': ['token', 'idPago']}, stat.BAD_REQUEST)

	@csrf_exempt
	def recarga(request, empresa = EMPRESAS['VIEPAY']):
		try:
			datos = recibirJson(request)

			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

			#MX PAY RECARGA:IDRecarga CONCEPTO
			if val.validarExistencia(datos, ['token', 'cantidad', 'oxxo']):
				token = verfUsuario(datos['token'], APPS_VIE['PAY'])
				if token:
					if token.idUsuario.permiso == Usuario.CLIENTE:
						correct = [50, 100, 200, 300, 500]
						#En caso de recibir una cadena
						datos['cantidad'] = int(datos['cantidad'])
						if datos['cantidad'] in correct:
							datos['concepto'] = 'MX PAY RECARGA:'
							rhttp = enviarJson({}, stat.OK)
							if datos['oxxo']:
						
								recarga = Pago.regRecarga(token.idUsuario, Pago.ESTATUS['ESPERANDO'],datos['cantidad'], 'referencia', empresa, empresa)
								recarga.save()
								datos['concepto'] = 'MX PAY RECARGA:' + str(recarga.idRecarga) 
								oxxo = MetodoPago.generarOxxo(request, APPS_VIE['PAY'], datos)
								if not oxxo:
									return enviarJson({'mensaje': 'Ocurrio un problema al comunicarse con conecta', 'campo': 'conekta'}, stat.FORBIDDEN)
								pago = Pago.registrarPago(empresa, recarga.idRecarga, oxxo.id, Pago.COMISION,Pago.ESTATUS['ESPERANDO'], MetodoPago.MetodoPagos['P_OXXO'], datos['cantidad'], None, oxxo.charges[0].payment_method.reference, datos['concepto'])
								
									
								if type(pago) == type(rhttp):
									return pago
								if type(recarga) == type(rhttp):
									return recarga
								recarga.referencia = pago.referencia
								recarga.save(update_fields = ['referencia'])

								campos = {}
								campos['mensaje'] = 'Referencia:' + str(pago.referencia)
								campos['referencia']= pago.referencia
								try:
									pers = Persona.objects.get(idPersona = token.idUsuario.idFila)
								except Persona.DoesNotExist:
									campos['mensaje'] = 'El telefono no se encontró...Aunque esto no debería pasar'
								sms = SMS()
								if sms.enviarReferencia(pago.cantidad, pago.referencia, pers.telefono):
									campos['sms'] = True
								else:
									campos['sms'] = False
								resp = Usuario.vieLogout(datos, datos['token'])
								return enviarJson(campos, stat.OK)
							else:
								if not 'tipo' in datos:
									return enviarJson({'mensaje': 'Falta especificar tipo', 'campo': 'tipo'}, stat.BAD_REQUEST)
								# En este caso también es necesario enviar el ID del método de pago
								# el json debería contener entonces... datos['token, cantidad, oxxo, metodo, concepto']

								if MetodoPago.MetodoPagos[datos['tipo']] == MetodoPago.MetodoPagos['P_CREDITO'] or MetodoPago.MetodoPagos[datos['tipo']] == MetodoPago.MetodoPagos['P_DEBITO']:
									debug("recarga con credito")
									recarga = Pago.regRecarga(token.idUsuario, Pago.ESTATUS['ESPERANDO'], datos['cantidad'], 'referencia', empresa, empresa)
									recarga.save()
									if type(recarga) == type(rhttp):
										return recarga

									datos['concepto'] = 'MX PAY RECARGA:' + str(recarga.idRecarga)
									datos['idvie'] = recarga.idRecarga
									
									pago = MetodoPago.pagar(request, datos, empresa)
									
									if type(pago) == type(rhttp):
										return pago

									debug ("Recarga: " + str(recarga))
									debug ("Pago: " + str(pago))

									recarga.referencia = pago.referencia
									recarga.save(update_fields = ['referencia'])
									resp = Usuario.vieLogout(datos, datos['token'])
									return enviarJson({'mensaje': 'Hecho'}, stat.OK)
								else:
									return enviarJson({'mensaje': 'Metodo de pago no valido', 'campo': 'tipo'}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'La cantidad no es valida', 'campo': 'cantidad'}, stat.FORBIDDEN)
					else:
						return enviarJson({'mensaje': 'No ha terminado su registro', 'campo': 'token'}, stat.UNAUTHORIZED)
				else:
					return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, cantidad, oxxo'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def generarOxxo(request, app, datos = {}):

		debug(datos)

		if len(datos) == 0:
			datos = recibirJson(request)

		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if val.validarExistencia(datos, ['cantidad', 'token']):
			if datos['cantidad'] > 0 and datos['cantidad'] <= 500:
				user = verfUsuario(datos['token'], app)

				if not user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'mensaje': 'No se puede realizar esta accion', 'campo': 'token'}, stat.UNAUTHORIZED)
				if user:
					debug("Voy a generar un OXXO!")
					try:
						pers = Persona.objects.get(idPersona = user.idUsuario.idFila)
						nombre = pers.nombre + " " + pers.apellidos
						oxxo = PagoConekta.oxxo_orden(datos['cantidad']*100, nombre, user.idUsuario.email, pers.telefono, datos['concepto'])

						return oxxo

					except Persona.DoesNotExist as e:
						debug(e)
						return enviarJson({'mensaje': 'No se encontró persona', 'campo':'persona'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'La cantidad a recargar es invalido', 'campo': 'cantidad'}, stat.FORBIDDEN)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, cantidad'}, stat.BAD_REQUEST)

	@csrf_exempt
	def consultaSaldo(request):
		try:
			datos = recibirJson(request)

			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			if 'token' in datos:
				user = verfUsuario(datos['token'], APPS_VIE['PAY'])

				if user:
					try:
						cartera = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario.idUsuario, tipo = MetodoPago.MetodoPagos['P_CARTERA'])
						return enviarJson({'saldo': cartera.saldo}, stat.OK)
					except MetodoPagoMdl.DoesNotExist as e:
						debug(e)
						return enviarJson({'mensaje': 'La forma de pago no fue encontrada', 'campo': 'cartera'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def pagoCartera(empresa, datos):

		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

		# Agregar más verificaciones de usuario
		if val.validarExistencia(datos, ['token', 'cantidad', 'concepto', 'viaje']):
			debug(datos)
			user = verfUsuario(datos['token'], app)

			if user:
				if not user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'mensaje': 'El usuario no puede realizar esta accion', 'campo': 'token'}, stat.UNAUTHORIZED)
				try:
					cartera = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario.idUsuario, tipo = MetodoPago.MetodoPagos['P_CARTERA'])
					
					if cartera.saldo <= 0:
						return enviarJson({'mensaje': 'Su saldo es insuficiente', 'campo': 'cantidad'}, stat.UNPROCESSABLE)
					elif cartera.saldo  < datos['cantidad']:
						cartera -= datos['cantidad']
						return enviarJson({'mensaje': 'Su saldo en contra es: $' + str(cartera.saldo)}, stat.UNPROCESSABLE)
					else:
						cartera -= datos['cantidad']
						return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
						#Pago.registrarPago(empresa, datos['viaje'], None, Pago.COMISION, Pago.ESTATUS['PAGADO'],MetodoPago.MetodoPagos['P_CARTERA'], datos['cantidad'], cartera, None, datos['concepto'])
				except MetodoPagoMdl.DoesNotExist as e:
					debug(e)
					return enviarJson({'mensaje': 'No se puede realizar el pago', 'campo': 'metodo, usuario'}, stat.UNPROCESSABLE)
				except CarteraMdl.DoesNotExist as e:
					debug(e)
					return enviarJson({'mensaje': 'No se puede realizar el pago', 'campo': 'metodo, usuario'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.UNAUTHORIZED)					

			return enviarJson({'mensaje': 'Pagado'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'clienteConekta, cantidad, metodo'}, stat.BAD_REQUEST)

	@csrf_exempt
	def recargaLista(request):
		try:
			datos = recibirJson(request)

			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			if 'token' in datos:
				user = verfUsuario(datos['token'], APPS_VIE['PAY'])
				if user:
					try:
						recargas = RecargaMdl.objects.filter(idUsuario = user.idUsuario)
						lista = []
						for recarga in recargas:
							pos = {}
							pos['folio'] = recarga.idRecarga
							pos['cantidad'] = recarga.monto
							pos['estatus'] = recarga.estatus
							pos['fecha'] = recarga.fecha
							lista.append(pos)
						return enviarJson({'mensaje': 'Listado de recargas', 'recargas': lista}, stat.OK)
					except RecargaMdl.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontraron recargas', 'campo': 'recarga'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)	

	def mostrar(token, metodopago, app):
		try:
			user = verfUsuario(datos['token'], app)

			if user:
				if not user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'mensaje': 'El usuario no puede realizar esta accion', 'campo': 'token'}, stat.UNAUTHORIZED)
				try:
					metodo = MetodoPagoMdl.objects.get(idUsuario = user.idUsuario.idUsuario, idMetodoPago = metodopago)
					
					info = {
						'mensaje': 'Informacion del metodo de pago',
						'tipo': MetodoPago.PagoInterp[metodo.tipo],
						'alias': metodo.alias,
						'codigo': metodo.codigo,
						'saldo': metodo.saldo
					}
					return enviarJson(info, stat.OK)
				except MetodoPagoMdl.DoesNotExist as e:
					debug(e)
					return enviarJson({'mensaje': 'No se encontro la forma de pago', 'campo': 'metodo'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)					
		except Exception as e:
			debug(e)		
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)