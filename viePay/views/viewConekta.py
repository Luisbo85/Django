import conekta
from django.http 		import HttpResponse
from django.utils					import timezone
from vie.views.views	import debug

#conekta.api_key = "key_N44xp739E2SqkgZVEht6ag" #Test
conekta.api_key = "key_LryCteQUwCxzJSC9ESzPzA" #Produccion

class PagoConekta:

	def crearCliente(nombre, apellidos, email, tel):
		debug("Crearé un cliente en conekta, con los siguientes datos:")
		debug(nombre + " " + apellidos + " " + email + " " + tel)
		nomCompleto = nombre + " " + apellidos
		fecha = timezone.now()
		fecha = fecha.replace(tzinfo = None)

		try:
			customer = conekta.Customer.create({
			    'name': nomCompleto,
			    'email': email,
			    'phone': tel,
			    'payment_sources': [],
			    'antifraud_info': {
			    	'account_created_at': 1484040996,
			    	'first_paid_at': 1485151007
			    }
			})
			debug("Fecha: " + str(customer.created_at))
			debug("email: " + str(customer.email))
			debug("ID: " + (customer.id))
			#debug("ID metodo pago default: " + str(customer.default_payment_source_id))
			return customer.id

		except conekta.ConektaError as e:
			debug(e)
			return False

	def ordenEjemplo(request):
		try:
			order = conekta.Order.create({
			"line_items": [{
				"name": "Tacos",
				"unit_price": 1000,
				"quantity": 12,
	      		"antifraud_info": {
					"trip_id": "12345",
					"driver_id": "driv_1231",
					"ticket_class": "economic",
					"pickup_latlon": "23.4323456,-123.1234567",
					"dropoff_latlon": "23.4323456,-123.1234567"
				}
			}],
			"shipping_lines": [{
				"amount": 1500,
				"carrier": "mi compañia"
			}],
			"currency": "MXN",
			"customer_info": {
				"customer_id": "cus_2gkYX2g1r3Tb3zZ7J",
				"antifraud_info":{
					"account_created_at": 1484040996,
					"first_paid_at": 1485151007,
					"paid_transactions": 4
				}
			},
			"shipping_contact":{
				"phone": "+523313568912",
				"receiver": "Bruce Wayne",
				"address": {
					"street1": "Calle 123 int 2 Col. Chida",
					"city": "Cuahutemoc",
					"state": "Ciudad de Mexico",
					"country": "MX",
					"postal_code": "06100",
					"residential": True
				}
			},
			"metadata": { "description": "Compra de creditos: 300(MXN)", "reference": "1334523452345" },
			"charges":[{
				"payment_method": {
					"payment_source_id":"src_2gkfsDEZD2hvJahYN",
					"type": "card"
					}
				}]
			})
		
			debug("ID: " + order.id)
			debug("Status: " + order.payment_status)
			debug("$" + str(order.amount/100) + str(order.currency))
			debug("Order")
			debug(str(order.line_items[0].quantity) + " - "
			            + order.line_items[0].name + " - "
			            + str(order.line_items[0].unit_price/100))
			debug("Payment info");
			debug("Code: " + order.charges[0].payment_method.auth_code)
			debug("Card info: "
				+ order.charges[0].payment_method.name + " - "
				+ "<strong><strong>" + order.charges[0].payment_method.last4 + " - "
				+ order.charges[0].payment_method.brand + " - "
				+ order.charges[0].payment_method.type)

			return HttpResponse("Pago efectuado!")

		except conekta.ConektaError as e:
		  debug(e)

		  return HttpResponse("Nel prro!")

	def orden(cliente, monto, metodoConekta, nombreCargo):
		#print (dir(conekta))
		debug(cliente)
		debug(monto)
		debug(metodoConekta)
		debug(nombreCargo)
		try:
			order = conekta.Order.create({
				"line_items": [{
					"name": nombreCargo,
					"unit_price": monto,
					"quantity": 1,
					"antifraud_info": {
						"trip_id": "12345",
						"driver_id": "driv_1231",
						"ticket_class": "economic",
						"pickup_latlon": "23.4323456,-123.1234567",
						"dropoff_latlon": "23.4323456,-123.1234567"
					}
				}],
				"shipping_lines": [{
					"amount": 0,
					"carrier": "VieTaxi"
				}],
				"currency": "MXN",
				"customer_info": {
					"customer_id": str(cliente),
					"antifraud_info":{
						"account_created_at": 1484040996,
						"first_paid_at": 1485151007,
						"paid_transactions": 4
					}
				},
				"shipping_contact":{
					"phone": "+523313568912",
					"address": {
						"street1": "Gonzalo Curiel 347 Col. Chida",
						"city": "Guadalajara",
						"state": "Jalisco",
						"country": "MX",
						"postal_code": "06100",
						"residential": True
					}
				},
				"charges":[{
					"payment_method": {
						"payment_source_id": metodoConekta,
						"type": "card"
					}
				}]
			})
			debug("ID: " + order.id)
			debug("Status: " + order.payment_status)
			debug("$" + str(order.amount/100) + str(order.currency))
			debug("Order")
			debug(str(order.line_items[0].quantity) + " - "
			            + order.line_items[0].name + " - "
			            + str(order.line_items[0].unit_price/100))
			debug("Payment info");
			debug("Code: " + order.charges[0].payment_method.auth_code)
			debug("Card info: "
				+ order.charges[0].payment_method.name + " - "
				+ "<strong><strong>" + order.charges[0].payment_method.last4 + " - "
				+ order.charges[0].payment_method.brand + " - "
				+ order.charges[0].payment_method.type)
			return order
		except conekta.ConektaError as e:
		  debug(e)
		  return False


	def metodo(token_tarj, token_custom):
		try:
			customer = conekta.Customer.find(token_custom)
			source = customer.createPaymentSource({
			  "type": "card",
			  "token_id": token_tarj
			})

			debug (dir(source))
			debug ("ID: " + str(source.id))
			debug ("Tarjeta-habiente: " + source.name)
			debug ("Tipo: " + source.type)
			debug ("Parent: " + str(source.parent))
			debug ("ID del parent: " + str(source.parent_id))
			return source.id
		except Exception as e:
			debug(e)
			return False
		

	def oxxo_orden(monto, nombre, email, telefono, concepto):
		try:
			debug("Oxxo Orden!")
			order = conekta.Order.create({
				"line_items": [{
					"name": concepto,
					"unit_price": monto,
					"quantity": 1,
					"antifraud_info": {
						"trip_id": "12345",
						"driver_id": "driv_1231",
						"ticket_class": "economic",
						"pickup_latlon": "23.4323456,-123.1234567",
						"dropoff_latlon": "23.4323456,-123.1234567"
					}
				}],
					"shipping_lines": [{
					"amount": 0,
					"carrier": "vie"
				}],
				"currency": "MXN",
				"customer_info": {
					"name": nombre,
					"email": email,
					"phone": telefono,
					'antifraud_info': {
				    	'account_created_at': 1484040996,
				    	'first_paid_at': 1485151007,
				    	'paid_transactions': 10
				    }
				},
				"shipping_contact":{
					"phone": "5555555555",
					"address": {
						"street1": "Calle Gonzalo Curiel 347 Col. Chida",
						"city": "Wadalaganatoz",
						"state": "Ajua",
						"country": "MX",
						"postal_code": "06100",
						"residential": True
					}
				},
				"charges":[{
					"payment_method": {
						"type": "oxxo_cash"
						}
					}]
				})
			
			debug("ID: " + order.id)
			debug("Payment Method: " + order.charges[0].payment_method.service_name)
			debug("Reference: " + order.charges[0].payment_method.reference)
			debug("$" + str(order.amount/100) + str(order.currency))
			debug("Order")
			debug(str(order.line_items[0].quantity) + " - "
					+ order.line_items[0].name + " - "
					+ str(order.line_items[0].unit_price/100))

			return order

		except Exception as e:
			debug(e)
			return False