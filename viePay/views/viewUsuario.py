import random as rand
import hashlib
from datetime import datetime, timedelta
import pytz

import rethinkdb as r
import decimal
import conekta

from django.shortcuts				import render
from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from django.utils.crypto 			import get_random_string
from django.http 					import HttpResponse

import vie.tools.validar			as val
from vie.views.views		import recibirJson, enviarJson, generarToken, debug, verfUsuario, verfPersona, verfChofer
from vie.models				import CodValidacion, Usuario as UsuarioMdl, Token, Persona, Chofer, \
Comentarios, Codigos as CodStatics  #FotoUsuario
from viePay.models			import MetodoPago as MetodoPagoMdl, Pago

from vie.tools.SMS				import SMS
#from vie.tools.correo 			import Correo
from vie.tools.errores			import Status as stat
from vieTaxi.views.viewAuto			import Automovil
from viePay.views.viewConekta	import PagoConekta
from vie.views.viewUsuario 		import Usuario as vUsuario
from Django.settings 			import APPS_VIE
#from vie.tools.forms 				import FormFoto

from django.http 					import QueryDict

class Usuario:
	"""
		Vista que controla las opciones con los usuarios del sistema.
	"""
	#Estado de uso de codigos
	CORRECTO 	= 0
	EXPIRADO 	= 1
	#Permisos/estados del usuario
	REGISTRADO 	= 0
	CLIENTE		= 1
	CHOFER		= 2
	CAMBIONUM 	= 3
	REG_RAPIDO	= 4
	#Formas de Login
	L_EMAIL		= 0		#Mediante un correo y contraseña
	L_FACE		= 1		#Mediante una cuenta de Facebook
	L_GOOGLE	= 2		#Mediante una cuenta de Google

	#Numero de sesiones maximas por usuario
	MAX_SESIONES = 10

	# Parte de la URL donde se ubica la imagen
	URL_IMAGEN = "http://test.viemx.com/media/"

	@csrf_exempt
	def accesoRapido(request):

		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			error, campo, tipo = val.validarDatos(datos, {'tel':'string', 'pin': ('string', 'int')})
			if error:
				if val.validarTel(datos['tel']):
					try:
						pinValido = False
						pers = Persona.objects.get(telefono = datos['tel'])
						user = UsuarioMdl.objects.filter(idFila = pers.idPersona).exclude(permiso = vUsuario.CHOFER)
						if len(user) == 0: 
							return enviarJson({'mensaje': 'No se encuentra el usuario', 'campo': 'tel'},stat.NOT_FOUND)
						else: 
							user = user[0]
						pin = int(datos['pin'])
						if user.permiso == Usuario.REGISTRADO:
							#Buscar el pin de validacion en la
							try:
								CodValidacion.objects.get(idUsuario = user, codigo = pin)
								pinValido = True
							except CodValidacion.DoesNotExist:
								pass
						else:
							if user.pin == pin:
								pinValido = True

						if pinValido:
							validoEstatus = False
							lista = Token.objects.filter(idUsuario = user, app = APPS_VIE['PAY'])
							if user.permiso == Usuario.REGISTRADO:
								user.permiso = Usuario.CLIENTE
								user.pin = pin
								user.save(update_fields = ['permiso', 'pin'])
								validoEstatus = True
							elif user.permiso == Usuario.CLIENTE: 
								validoEstatus = True
							else: #Estatus soportado
								pass

							if validoEstatus:
								if len(lista) >= Usuario.MAX_SESIONES:
									Token.objects.all().update(idUsuario=None)
								intentos = 0
								#Intentar conseguir un token para el usuario actual
								if len(lista) == 1:
									debug (lista[0].asignado)
									lista[0].asignado	= False
									lista[0].idUsuario	= None
									lista[0].save(update_fields = ['asignado','idUsuario'])
								while intentos < 3:
									token = vUsuario.asignarToken(user, APPS_VIE['PAY'])
									if token == False:
										intentos += 1
									else:
										break
								if token == False:
									return enviarJson({'mensaje': 'Intentar más tarde', 'campo': 'ninguno'}, stat.TIME_OUT)
								return enviarJson({'mensaje': 'Acceso exitoso!', 'token': token}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Usuario fue dado de baja', 'campo': 'usuario'}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'Pin incorrecto.', 'campo': 'pin'}, stat.NOT_FOUND)
					except Persona.DoesNotExist:
						return enviarJson({'mensaje': 'No existe ese numero telefonico', 'campo': 'tel'}, stat.NOT_FOUND)
				else:
					return enviarJson({'mensaje': 'El telefono no tiene un formato correcto', 'campo':'tel'}, stat.BAD_REQUEST)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)
	
	@csrf_exempt	
	def vieLogout(request):
		"""
			Retira el acceso al usuario debido a su petición. Libera el token que tiene asignado
		"""
		return vUsuario.vieLogout(request)

	@csrf_exempt
	def reCodigo (request):
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			error, campo, tipo = val.validarDatos(datos, {'tel':'string'})
			if  error:
				if val.validarTel(datos['tel']):
					try:
						pers = Persona.objects.get(telefono = datos['tel'])
						user = UsuarioMdl.objects.filter(idFila = pers.idPersona).exclude(permiso = vUsuario.CHOFER)
						if len(user) == 0:
							return enviarJson({'mensaje': 'No se encuentra el usuario', 'campo': 'tel'},stat. NOT_FOUND)
						else:
							user = user[0]	
						debug("ID de persona ->" + str(pers.idPersona))
						debug("El nombre es: " + pers.nombre)

					except Persona.DoesNotExist:
						return enviarJson({'mensaje': 'No se encontro el telefono', 'campo': 'tel'}, stat.NOT_FOUND)

					lista = Token.objects.filter(idUsuario = user, app = APPS_VIE['PAY'])
					if len(lista) < Usuario.MAX_SESIONES:
		 				intentos = 0
		 				#Intentar conseguir un token para el usuario actual
		 				if len(lista) == 1:
		 					debug (lista[0].asignado)
		 					lista[0].asignado	= False
		 					lista[0].idUsuario	= None
		 					lista[0].save(update_fields = ['asignado','idUsuario'])
		 				while intentos < 3:
		 					token = vUsuario.asignarToken(user, APPS_VIE['PAY'])
		 					if token == False:
		 						intentos += 1
		 					else:
		 						break
		 				if token == False:
		 					return enviarJson({'mensaje': 'Intentar más tarde', 'campo': 'ninguno'}, stat.TIME_OUT)

					codigoValidacion = vUsuario.generarCodValidacion()
					debug('Codigo generado: '+str(codigoValidacion))
					#Crear un registro en la bases de datos o asignar uno
					hoy = timezone.now()
					debug("Guardare-> " + str(hoy))
					codigo = CodValidacion(
						codigo			= codigoValidacion,
						idUsuario 		= user,
						fechaAsignacion = hoy
					)
					codigo.save()

					sms = SMS()
					
					if sms.EnviarCodConfirmacion(codigoValidacion, pers.nombre, SMS.RECUPERACION, datos['tel']):
						user.pin = codigo.codigo
						user.save(update_fields = ['pin'])
						return enviarJson({'mensaje': 'Hecho', 'token': token}, stat.OK)

					else:
						return enviarJson({'mensaje': 'Ocurrio un problema...', 'campo':'sms'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'Formato incorrecto del telefono', 'campo': 'tel'}, stat.BAD_REQUEST)
			else:	
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	@csrf_exempt
	def vieValidarCod(request):
		return vUsuario.vieValidarCod(request, APPS_VIE['PAY'])


	@csrf_exempt
	def vieValidarNvoCod(request):
		return vUsuario.vieValidarCod(request, APPS_VIE['PAY'])