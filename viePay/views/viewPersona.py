
from vie.tools.errores				import Status as stat
from vie.views.views				import recibirJson, enviarJson, verfUsuario, verfAcceso, debug, leerDatos
from django.views.decorators.csrf	import csrf_exempt
from Django.settings 				import APPS_VIE

from vie.views.viewPersona			import Persona as vPersona

class Persona:

	@csrf_exempt
	def obtenerPeticion(request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		error, datos = leerDatos(request, True)
		if error:
			return enviarJson(datos, error)
		else:
			if request.method == 'POST':
				return vPersona.vieRestRegistro(datos, APPS_VIE['PAY'])
			elif request.method == 'GET':
				if 'token' in datos:	
				    return vPersona.vieTenerDatos(datos, APPS_VIE['PAY'])
				else:
					return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
			elif request.method == 'PUT':
				return vPersona.modificar(datos, APPS_VIE['PAY'])
			elif request.method == 'DELETE':
				return vPersona.eliminar(datos, APPS_VIE['PAY'])
			else:
				return enviarJson({'mensaje': 'Metodo HTTP no soportado', 'campo': 'http'})
