from django.views.decorators.csrf	import csrf_exempt
from vie.views.views				import recibirJson, enviarJson, verfUsuario, debug
from vie.tools.errores 					import Status as stat
class Webhooks:

	@csrf_exempt
	def conekta(request):
		datos = recibirJson(request)
		return enviarJson({'msj': 'Problemas al enviar la peticion'}, stat.BAD_REQUEST)
