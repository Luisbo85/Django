# -*- coding: utf-8 -*-
from django.conf.urls        import url
from django.conf             import settings
from django.conf.urls.static import static

from viePay.views 				       import views, viewAdmin
from viePay.views.viewMetodoPago       import MetodoPago
from viePay.views.viewPromociones      import Promociones
from viePay.views.viewConekta          import PagoConekta
from viePay.views.viewUsuario          import Usuario
from viePay.views.viewPersona          import Persona
from viePay.views.viewTarjeta          import Tarjeta



urlpatterns = [
    url(r'^$',                      views.index, name='index'),
    url(r'^promociones/agregar/$',  Promociones.generarPromocion),
    url(r'^persona/baja/$',         Tarjeta.desligar),
    url(r'^persona/registro/$', 	Persona.obtenerPeticion),
    url(r'^persona/validarCod/$',   Usuario.vieValidarCod),
    #url(r'^persona/revalidarCod/$', Usuario.vieValidarNvoCod),
    url(r'^persona/saldo/$',        MetodoPago.consultaSaldo),
    url(r'^persona/tarjeta/$',      Tarjeta.ligarTarjeta),
    url(r'^persona/tarjetas/$',     Tarjeta.mostrar),
    url(r'^persona/reNIP/$',        Usuario.reCodigo),
    url(r'^persona/',               Persona.obtenerPeticion),
    url(r'^login/$', 				Usuario.accesoRapido),
    url(r'^logout/$',               Usuario.vieLogout),
    url(r'^recargar/lista/$',       MetodoPago.recargaLista),
    url(r'^recargar/$', 			MetodoPago.recarga),
    url(r'^registroTarjeta/$',      MetodoPago.obtenerPeticion),
    url(r'^metodopago/lista/$',		MetodoPago.pmostrarMetodoPagos),
    url(r'^conekta/$',              PagoConekta.ordenEjemplo),
    url(r'^oxxo/$', 				PagoConekta.oxxo_orden),
]