from django.apps import AppConfig


class ViepayConfig(AppConfig):
    name = 'viePay'

    path = 'viePay/'
