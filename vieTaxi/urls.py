# -*- coding: utf-8 -*-
from django.conf.urls        import url
from django.conf             import settings
from django.conf.urls.static import static

from vie.tools.telefono              import Movil
from vieTaxi.views 				     import views, viewAdmin
from vieTaxi.views.viewUsuario       import Usuario
from vieTaxi.views.viewChofer        import Chofer
from vieTaxi.views.viewAuto          import Automovil
from vieTaxi.views.viewPersona       import Persona
from vieTaxi.views.viewEmpresaChofer import Empresa
from vieTaxi.views.viewMetodoPago    import MetodoPago

urlpatterns = [
    url(r'^$',                      views.index, name='index'),
    url(r'^admin/$',                viewAdmin.dirigir),
    url(r'^comprobar/$',            Usuario.esValido),
    url(r'^logout/$',               Usuario.vieLogout),
    url(r'^killsessions/$',         Usuario.cerrarSesiones),
    url(r'^restablecer/$',          Usuario.restablecer),
    url(r'^validarCod/$',           Usuario.vieValidarCod),
    url(r'^persona/envcod/$',       Usuario.enviarCodigo),
    url(r'^persona/login/$',        Usuario.viePersonaLogin),
    url(r'^persona/chofer/$',       Persona.getChofer),
    url(r'^persona/foto/$',         Persona.peticionFoto),
    #url(r'^persona/foto/$',         Usuario.vieSubirFoto),
    url(r'^persona/historial/$',    Persona.getHistorial),
    url(r'^persona/viaje/$',        Persona.getPetViaje),
    url(r'^persona/calificar/$',    Persona.calificar),
    url(r'^persona/$',              Persona.obtenerPeticion),
    url(r'^cargo/$',                MetodoPago.pagar),
    url(r'^oxxo/$',                 MetodoPago.generarOxxo),
    url(r'^recarga/$',              MetodoPago.recarga),
    url(r'^metodopago/lista/$',     MetodoPago.mostrarMetodoPagos),
    url(r'^metodopago/modificar/$', MetodoPago.modificar),
    url(r'^metodopago/eliminar/$',  MetodoPago.eliminar),
    url(r'^metodopago/$',           MetodoPago.agregar),
    #url(r'^metodopago/$', 	        Usuario.vieMetodoPago),
    url(r'^auto/$',                 Automovil.obtenerPeticion),
    url(r'^auto/asigAuto/$',        Automovil.vieAsignacion),
    url(r'^auto/liberar/$',         Automovil.vieLiberarAsig),
    url(r'^auto/estado/$',          Automovil.vieEstado),
    url(r'^chofer/login/$',         Usuario.vieChoferLogin),
    url(r'^chofer/calificar/$',     Chofer.calificar),
    url(r'^chofer/foto/$',          Chofer.peticionFoto),
    url(r'^chofer/historial/$',     Chofer.getHistorial),
    url(r'^chofer/persona/$',       Chofer.getPersona),
    url(r'^chofer/subscripcion/$',  Chofer.verfsus),
    url(r'^chofer/viaje/$',         Chofer.getPetViaje),
    url(r'^chofer/$',               Chofer.obtenerPeticion),
    #Url no encontrada
  	#url(r'^', views.paginaNoEncontrada),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
