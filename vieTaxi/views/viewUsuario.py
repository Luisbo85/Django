# -*- coding: utf-8 -*-
from django.views.decorators.csrf	import csrf_exempt

from vie.tools.estatus          import APPS_VIE, EMPRESAS

from vie.tools.errores			import Status as stat
from vie.views.viewUsuario      import Usuario as vUsuario
from vie.views.views            import verfAcceso,recibirJson, enviarJson, leerDatos

class Usuario:

	@csrf_exempt
	def cerrarSesiones(request):
		return vUsuario.cerrarSesiones(request, EMPRESAS['VIETAXI'])

	@csrf_exempt
	def esValido(request):
		return vUsuario.esValido(request, EMPRESAS['VIETAXI'])

	@csrf_exempt
	def enviarCodigo(request):
		return vUsuario.enviarCodigo(request, APPS_VIE['TAXI_CLIENTE'])

	@csrf_exempt
	def restablecer(request):
		return vUsuario.restablecer(request, APPS_VIE['TAXI_CLIENTE'])

	@csrf_exempt
	def vieChoferLogin(request):
		error, resp = leerDatos(request)
		if error:
			return enviarJson(resp, error)
		else:
			return vUsuario.vieLogin(resp, APPS_VIE['TAXI_CHOFER'])

	@csrf_exempt
	def viePersonaLogin(request):
		error, resp = leerDatos(request)
		if error:
			return enviarJson(resp, error)
		else: 
			return vUsuario.vieLogin(resp, APPS_VIE['TAXI_CLIENTE'])

	@csrf_exempt
	def vieLogout(request):
		return vUsuario.vieLogout(request)

	@csrf_exempt
	def vieSubirFoto(request):
		error, resp = leerDatos(request)
		if error:
			return enviarJson(resp, error)
		elif verfAcceso(resp['token'], APPS_VIE['TAXI_CLIENTE']):
			return vUsuario.vieSubirFoto(request, APPS_VIE['TAXI_CLIENTE'])
		else:
			return enviarJson({'mensaje': 'Usuario no tiene acceso a esta aplicacion', 'campo': 'acceso'}, stat.UNAUTHORIZED)

	@csrf_exempt
	def vieValidarCod(request):
		return vUsuario.validarCodPeticion(request, APPS_VIE['TAXI_CLIENTE'])