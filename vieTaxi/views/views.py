# Create your views here.
from django.http 					import HttpResponse

def index(request):
	"""
		Vista que muestra la pagina de inicia al sitio.
	"""
	html = """
			<html>
			<head>
			       <title>VieTaxi</title>
			</head>
			<body>
			       <h3>VieTaxi</h3>
			</body>
			</html>"""
	return HttpResponse(html)