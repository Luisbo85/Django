import rethinkdb as r

from django.views.decorators.csrf	import csrf_exempt
from vie.tools.estatus				import RETHINKDB_TEST, APPS_VIE

from vie.models						import Persona as personaMdl, Usuario as usuarioMdl, Token, Chofer, AutoChofer
import vie.tools.validar	        as val
from vie.tools.telefono             import Movil
from vie.tools.errores				import Status as stat
from vie.views.views				import recibirJson, enviarJson, verfUsuario, verfAcceso, debug, leerDatos
from vie.views.viewUsuario 			import Usuario
from vie.views.viewPersona			import Persona as vPersona
from vieTaxi.views.viewAuto			import Automovil
from viePay.views.viewMetodoPago	import MetodoPago as kPago

class Persona:

	@csrf_exempt
	def obtenerPeticion(request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		error, datos = leerDatos(request, True)
		if error:
			return enviarJson(datos, error)
		else:
			if request.method == 'POST':
				return vPersona.vieRestRegistro(datos, APPS_VIE['TAXI_CLIENTE'])
			else:
				if 'token' in datos:
					if type(datos['token']) == str:
						usuario, permitido = verfAcceso(datos['token'], [APPS_VIE['TAXI_CLIENTE'], APPS_VIE['TAXI_ADMIN']])
						if usuario:
							if request.method == 'PROPFIND':
								if datos['token']:	
								    return vPersona.vieTenerDatos(datos['token'], APPS_VIE['TAXI_CLIENTE'])
								else:
									return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
							elif request.method == 'PUT':
								return vPersona.modificar(datos, APPS_VIE['TAXI_CLIENTE'])
							#elif request.method == 'DELETE':
							#	return vPersona.eliminar(datos, APPS_VIE['TAXI_ADMIN'])
							else:
								return enviarJson({'mensaje': 'Metodo HTTP no soportado', 'campo': 'http'}, stat.BAD_REQUEST)
						else:
							if permitido:
								return enviarJson({'mensaje': 'Usuario sin los permisos necesarios', 'campo': 'app'}, stat.UNAUTHORIZED)
							else:
								return enviarJson({'mensaje': 'Usuario no encontrado', 'campo': 'token'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'token', 'tipo': 'string'}, stat.BAD_REQUEST)	
				else:
					return enviarJson({'mensaje': 'Falta campos en la peticion', 'campo': 'token'}, stat.BAD_REQUEST)			

	@csrf_exempt
	def getChofer (request):
		"""
			Permite obtener información de chofer
			mediante token de rethinkdb por método GET
		"""
		if request.method == 'GET':
			token = request.GET.get("token", None)
			if token:
				return Persona.traerDatosChofer(str(token))
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	@csrf_exempt
	def getPetViaje (request):
		"""
			Permite obtener información de chofer
			mediante token de rethinkdb por método GET
		"""
		if request.method == 'POST':
			error, datos = leerDatos(request, True)
			if error:
				return enviarJson(datos, error)
			valido, campo, tipo = val.validarDatos(datos, {'token':'string', 'token_viaje': 'string', 'estado': 'int'})
			if valido:
				if datos['estado'] == 0:
					return Persona.traerDatosPetViaje(datos)
				if datos['estado'] == 1:
					return Persona.traerDatosViaje(datos)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	
	def traerDatosChofer(datos):
		"""
			Método para traer la informacion delChofer mediante el token del viaje
		"""
		if not datos == None:
			debug (datos)
			# conexión - EN PRODUCCION ESTO FALLARÁ AL DEBUGGEAR!!
			# Hacer lo posible para que se conecte a rethinkdb de produccion
			if RETHINKDB_TEST:
				r.connect('localhost', 28015).repl() # local-Lap
			else:
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

			# abreviación a tabla
			vietaxi = r.db('vie').table('viaje')
			rquery = vietaxi.get(datos).run()

			if not rquery == None:
				debug (rquery)
				debug ("El id de chofer es: " + str(rquery['chofer']) + "\nEl id de cliente: " + str(rquery['cliente']))
				campos = {}

				try:
					debug ("Es chofer y su codigo es: " + str(rquery['chofer']))
					chofer = Chofer.objects.get(idChofer = rquery['chofer'])

					try:
						avatar = usuarioMdl.objects.get(idFila = rquery['chofer'], permiso = Usuario.CHOFER)
						campos['avatar'] = str(avatar.foto)
					except:
						campos['avatar'] = None
					campos['placas'] = None
					campos['marca'] = None
					campos['color'] = None
					campos['tel'] = None
					if rquery['estatus'] == 0:
						if not chofer.idAuto == None and chofer.idAuto.status == Automovil.estados['ASIGNADO']:
							campos['placas'] = chofer.idAuto.placas
							campos['marca'] = chofer.idAuto.marca
							campos['color'] = chofer.idAuto.color
						campos['tel'] = chofer.telefono
					campos['nombre'] = chofer.nombre
					campos['apellidos'] = chofer.apellidos 
					campos['calificacion'] = round(chofer.calificacion, 1)

					return enviarJson(campos, stat.OK)

				except Chofer.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a un chofer', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)

	def traerDatosPetViaje(datos):
		"""
			Método para traer PeticiónViaje mediante token de rethinkdb
		"""
		if not datos == None:
			debug (datos)
			# conexión - EN PRODUCCION ESTO FALLARÁ AL DEBUGGEAR!!
			# Hacer lo posible para que se conecte a rethinkdb de produccion
			if RETHINKDB_TEST:
				r.connect('localhost', 28015).repl() # local-Lap
			else:
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor
			rquery = {}
			rquery['idChofer'] = None

			# abreviación a tabla
			vietaxi = r.db('vie').table('peticionViaje')
			rquery = vietaxi.get(datos['token_viaje']).run()

			if not rquery == None:
				debug (rquery)
				debug ("El id de chofer es: " + str(rquery['idChofer']) + "\nEl id de cliente: " + str(rquery['idUsuario']))
				campos = {}

				#consultar con Jarillo
				"""if not rquery['estatus'] == 1:
					return enviarJson({'mensaje': 'El viaje no es valido'}, stat.FORBIDDEN)"""


				user = verfUsuario(datos['token'])
				if not user:
					return enviarJson({'mensaje': 'El usuario no existe'}, stat.UNAUTHORIZED)

				debug(user.idUsuario)

				if not user.idUsuario.idFila == rquery['idUsuario'] and not user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'mensaje': 'El usuario no coincide con el viaje', 'campo': 'token, token_viaje'}, stat.FORBIDDEN)

				try:
					debug ("Es chofer y su codigo es: " + str(rquery['idChofer']))
					try:
						chofer = Chofer.objects.get(idChofer = rquery['idChofer'])
					except Chofer.DoesNotExist:
						return enviarJson({'mensaje': 'No existe el chofer', 'campo':'token_viaje'}, stat.UNPROCESSABLE)

					try:
						avatar = usuarioMdl.objects.get(idFila = rquery['idChofer'], permiso = Usuario.CHOFER)
						campos['foto'] = str(avatar.foto)
					except:
						campos['foto'] = None
					nombre = str(chofer.nombre) + " " + str(chofer.apellidos)
					campos['placas'] = None
					campos['marca'] = None
					campos['color'] = None
					campos['status'] = rquery['estatus']
					campos['fecha'] = None
					campos['pago'] = None
					campos['nombre'] = nombre
					if not chofer.idAuto == None and chofer.idAuto.status == Automovil.estados['ASIGNADO']:
						campos['placas'] = chofer.idAuto.placas
						campos['marca'] = chofer.idAuto.marca
						campos['color'] = chofer.idAuto.color
					if not chofer.idEmpresa == None:	
						campos['empresa'] = chofer.idEmpresa.nombre
					campos['destino'] = None
					campos['origen'] = rquery['origen']
					campos['tiempoPeticion'] = rquery['tiempoPeticion']
					campos['calificacion'] = round(chofer.calificacion, 1)
					campos['fechaFin'] = None
					campos['tel'] = chofer.telefono
					if 'fecha' in rquery:
						campos['fecha'] = rquery['fecha']
					if 'fechaFin' in rquery:
						campos['fechaFin'] = rquery['fechaFin']
					if 'idPago' in rquery:
						campos['pago'] = rquery['idPago']
					if 'destino' in rquery:
						campos['destino'] = rquery['destino']

					return enviarJson(campos, stat.OK)

				except Chofer.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a un chofer', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)

	def traerDatosViaje(datos):
		"""
			Método para traer PeticiónViaje mediante token de rethinkdb
		"""
		if not datos == None:
			debug (datos)
			# conexión - EN PRODUCCION ESTO FALLARÁ AL DEBUGGEAR!!
			# Hacer lo posible para que se conecte a rethinkdb de produccion
			if RETHINKDB_TEST:
				r.connect('localhost', 28015).repl() # local-Lap
			else:
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

			# abreviación a tabla
			vietaxi = r.db('vie').table('viaje')
			rquery = vietaxi.get(datos['token_viaje']).run()

			if not rquery == None:
				debug (rquery)
				debug ("El id de chofer es: " + str(rquery['chofer']) + "\nEl id de cliente: " + str(rquery['cliente']))
				
				#Consultar con Jarillo
				"""if not rquery['estatus'] == 0:
					return enviarJson({'mensaje': 'El viaje no es valido'}, stat.FORBIDDEN)"""

				user = verfUsuario(datos['token'])

				if not user:
					return enviarJson({'mensaje': 'El usuario no existe'}, stat.UNAUTHORIZED)

				if not user.idUsuario.idFila == rquery['cliente'] and not user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'mensaje': 'El usuario no coincide con el viaje', 'campo': 'token, token_viaje'}, stat.FORBIDDEN)

				campos = {}

				try:
					debug ("Es chofer y su codigo es: " + str(rquery['chofer']))
					try:
						chofer = Chofer.objects.get(idChofer = rquery['chofer'])
					except Chofer.DoesNotExist:
						return enviarJson({'mensaje': 'No existe el chofer', 'campo':'token_viaje'}, stat.UNPROCESSABLE)
					
					try:
						avatar = usuarioMdl.objects.get(idFila = rquery['chofer'], permiso = Usuario.CHOFER)
						campos['foto'] = str(avatar.foto)
					except:
						campos['foto'] = None
					nombre = str(chofer.nombre) + " " + str(chofer.apellidos)
					campos['placas'] = None
					campos['marca'] = None
					campos['color'] = None
					if not chofer.idAuto == None and chofer.idAuto.status == Automovil.estados['ASIGNADO']:
						campos['placas'] = chofer.idAuto.placas
						campos['marca'] = chofer.idAuto.marca
						campos['color'] = chofer.idAuto.color
					if not chofer.idEmpresa == None:	
						campos['empresa'] = chofer.idEmpresa.nombre
					campos['status'] = rquery['estatus']
					campos['nombre'] = nombre
					campos['tel'] = chofer.telefono
					campos['origen'] = rquery['origen']
					campos['fechaInicio'] = rquery['tiempoInicio']
					campos['fechaFin'] = None
					campos['calificacion'] = round(chofer.calificacion, 1)
					campos['costo'] = None
					if 'tiempoFin' in rquery:
						campos['fechaFin'] = rquery['tiempoFin']
					if 'costo' in rquery:
						campos['costo'] = rquery['costo']

					return enviarJson(campos, stat.OK)

				except Chofer.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a un chofer', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)	

	@csrf_exempt
	def calificar(request):
		"""
			Metodo que llama el método de usuario para calificar
			a un chofer
		"""
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
		return Usuario.calificar(datos, APPS_VIE['TAXI_CLIENTE'], Usuario.CLIENTE)

	@csrf_exempt
	def getHistorial (request):
		"""
			Metodo para obtener un historial de los viajes
			realizados por la persona, aqui sólo se recibe el token
			mediante el metodo GET y lo envia al metodo correcto
		"""
		if request.method == 'GET':
			q = request.GET.get("token", None)
			t = request.GET.get("bloque", None)
			if q:
				if t:
					return Persona.historialViaje(str(q), int(t), APPS_VIE['TAXI_CLIENTE'])
				else:
					return enviarJson({'mensaje': 'No hay num de bloque'}, stat.BAD_REQUEST)
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def historialViaje (datos, bloq, app):
		"""
			Metodo para obtener el historial
		"""
		salto = 14
		token = verfUsuario(datos, app)

		if token:
			if token.idUsuario.permiso == Usuario.CLIENTE:
				debug("El usuario: " + str(token.idUsuario.idUsuario))
				debug("El cliente: " + str(token.idUsuario.idFila))
				
				if RETHINKDB_TEST:
					r.connect('localhost', 28015).repl() # local-Lap
				else:
					r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

				# abreviación a tabla
				vietaxi = r.db('vie').table('viaje')
				rquery = vietaxi.filter({'cliente': token.idUsuario.idFila}).order_by(r.desc('tiempoInicio')).skip(bloq*salto).limit(salto).run()
				num = vietaxi.filter({'cliente': token.idUsuario.idFila}).order_by(r.desc('tiempoInicio')).skip(bloq*salto).limit(salto).count().run()

				if not num == 0:
				#	debug(rquery)
					debug ("\nEl bloque es el: " + str(bloq))
					debug ("\nEl salto es de: " + str(bloq*salto))
					debug ("\nFueron un total de: " + str(num))
					lista = []
					for i in rquery:
						debug("---------- Viaje -----------")
						del i['id']
						try:
							cli = personaMdl.objects.get(idPersona = i['cliente'])
							i['cliente'] = cli.nombre + cli.apellidos
							chof = Chofer.objects.get(idChofer = i['chofer'])
							i['chofer'] = chof.nombre + chof.apellidos
						except personaMdl.DoesNotExist as e:
							debug(e)
							return enviarJson({'mensaje': 'Cliente no encontrado'}, stat.UNPROCESSABLE)
						except Chofer.DoesNotExist as f:
							debug(f)
							i['chofer'] = None
						for j in i:
							debug(j + ": " + str(i[j]))
						lista.append(i)

					return enviarJson({'resultados': lista}, stat.OK)
				else:
					return enviarJson({'mensaje': 'No se encontro resultados'}, stat.NO_CONTENT)
			else:
				return enviarJson({'mensaje': 'No se permite acceso'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Token no valido'}, stat.BAD_REQUEST)

	@csrf_exempt
	def peticionFoto(request):
		return vPersona.peticionFoto(request, APPS_VIE['TAXI_CLIENTE'])