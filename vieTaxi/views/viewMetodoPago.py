from django.views.decorators.csrf	import csrf_exempt
from vie.tools.errores 		import Status as stat
from viePay.models 		import MetodoPago as MetodoPagoMdl
from viePay.views.viewMetodoPago import MetodoPago as vMetodoPago
from vie.views.views	import recibirJson, enviarJson, verfUsuario, debug
from Django.settings	import EMPRESAS, APPS_VIE
import vie.tools.validar		as val


class MetodoPago:
	"""
		Clase para administrar los metodos de MetodoPago
	"""
	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return Pago.agregar(datos)
		elif request.method == 'GET':
			q = request.GET.get("token", None)
			if q:
			    return enviarJson({'mensaje': 'Holi'}, stat.OK)
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.UNPROCESSABLE)
		elif request.method == 'PUT':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return Pago.eliminar(datos)
		elif request.method == 'PATCH':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			debug(datos)
			return Pago.modificar(datos)
		else:
			return enviarJson({'mensaje': 'El metodo HTTP no se reconoce', 'campo': 'metodo HTTP'}, stat.NOT_ALLOW)

	def pag_predeterminado(usuario, alias = 'Efectivo'):
		"""
			Registrar ciertos metodos de pago que van estar por default en el usuario 
		"""
		if not usuario == None:
			debug ("Persona: " + str(usuario))
			meth_pago = MetodoPagoMdl (
				tipo = vMetodoPago.MetodoPagos['P_EFECTIVO'],
				idUsuario = usuario,
				alias = alias,
				metodoDefault = True
			)
			meth_pago.save()
			return enviarJson({'mensaje': 'Todo chido! :D'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'No se inserto persona', 'campo': 'persona'}, stat.BAD_REQUEST)

	@csrf_exempt
	def agregar(request):

		datos = recibirJson(request)

		if not datos:
			return enviarJson ({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if val.validarExistencia(datos, ['token', 'alias', 'tipo']):
			tok = verfUsuario(datos['token'], APPS_VIE['TAXI_CLIENTE'])
			if tok:
				nomb = datos['alias']
				if datos['tipo'] in vMetodoPago.MetodoPagos and not datos['tipo'] == vMetodoPago.MetodoPagos['P_VIETRANS']:
					return vMetodoPago.agregar(datos, EMPRESAS['VIETAXI'], APPS_VIE['TAXI_CLIENTE'])
				else:
					return enviarJson({'mensaje': 'El tipo de pago no es valido', 'campo': 'tipo'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, alias, tipo'}, stat.BAD_REQUEST)

	def agregarPagoSubsc(usuario, token, tipo, codigo):
		"""
			Metodo para poder agregar la forma de pago en la que se va a hacer los cargos de la subscripcion
		"""
		meth_pago = MetodoPagoMdl (
			idUsuario = usuario,
			tipo = tipo,
			alias = 'Subscripcion',
			token = token,
			metodoDefault = True,
			codigo = codigo
		)
		meth_pago.save()

	@csrf_exempt
	def mostrarMetodoPagos(request):
		return vMetodoPago.mostrarMetodoPagos(request, EMPRESAS['VIETAXI'], APPS_VIE['TAXI_CLIENTE'])


	@csrf_exempt
	def eliminar(request):
		return vMetodoPago.eliminar(request, EMPRESAS['VIETAXI'])


	@csrf_exempt
	def modificar(request):
		#Verificar este método para dar más seguridad en cuando al IdPago
		return vMetodoPago.modificar(request, EMPRESAS['VIETAXI'])

	@csrf_exempt
	def pagar(request):
		return vMetodoPago.agregar(request, EMPRESAS['VIETAXI'])

	@csrf_exempt
	def generarOxxo(request):
		return vMetodoPago.generarOxxo(request)

	@csrf_exempt
	def recarga(request):
		return vMetodoPago.recarga(request, EMPRESAS['VIETAXI'])
