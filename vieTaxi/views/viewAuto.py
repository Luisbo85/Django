from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
import vie.tools.validar		as val
from vie.tools.errores 		import Status as stat
from vie.models			import Chofer as ChoferMdl, Auto as AutoMdl, Usuario as UsuarioMdl, Persona, AutoChofer
from vie.views.views	import recibirJson, enviarJson, debug
import datetime

class Automovil:

	"""Status del automovil"""
	# REGISTRADO 	= 0	 Indica que un auto ha sido registrado
	# DISPONIBLE	= 1  Indica que el auto esta a disposición.
	# ASIGNADO	= 2	 Indica que el auto está asignado a un chofer
	estados = {'REGISTRADO': 0, 'DISPONIBLE': 1, 'ASIGNADO': 2, 'REPARACION': 3}

	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			return Automovil.vieRegAuto(datos)
		elif request.method == 'GET':
			q = request.GET.get("placas", None)
			if q:
			    return Automovil.vieTenerDatos(str(q))
			else:
				return enviarJson({'mensaje': 'No se detecto las placas', 'campo': 'placas'}, stat.UNPROCESSABLE)
		elif request.method == 'PUT':
			datos = recibirJson(request)
			return Automovil.modificar(datos)
		elif request.method == 'DELETE':
			return enviarJson({'mensaje': 'Enviaste un delete'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def vieRegAuto (datos):
		"""
			Permite registrar un automovil.
		"""
		if val.validarExistencia(datos, ['marca', 'modelo', 'placas', 'tipo', 'color']):
			mayplacas = datos['placas'].upper()
			if val.validarPlacas(mayplacas):
				busplacas = AutoMdl.objects.filter(placas = mayplacas)
				if len(busplacas) == 0:
					auto = AutoMdl (
						marca = datos['marca'],
						modelo = datos['modelo'],
						placas = mayplacas,
						tipo = datos['tipo'],
						color = datos['color']
						)
					auto.save()
					if 'chofer' in datos:
						datos ['auto'] = auto.placas
						datos ['cambiarst'] = 1
						return Automovil.vieAsignacion(datos, datos)
					else:
						return enviarJson({'mensaje': 'Auto Registrado exitosamente', }, stat.OK)
				else:
					return enviarJson({'mensaje': 'Placas repetidas', 'campo': 'placas'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Placas incorrectas', 'campo': 'placas'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'marca, modelo, placas, tipo, color'}, stat.BAD_REQUEST)

	@csrf_exempt
	def vieAsignacion(request, datos = {}):
		"""
			Permite asignar un automovil a un chofer.
		"""
		if len(datos) == 0:
			datos = recibirJson(request)	
		if val.validarExistencia(datos, ['chofer', 'placas']):
			chofer = Automovil.verfChofer(datos['chofer'])
			if chofer:
				auto = Automovil.verfAuto(datos['placas'])
				if auto:
					reasignar = Automovil.verfPar (chofer, auto)
					if not reasignar:
						if 'cambiarst' in datos:
							if datos['cambiarst'] == 1:
								auto.status = Automovil.estados['DISPONIBLE']
						if auto.status == Automovil.estados['DISPONIBLE']:
							if chofer.idAuto == None:
								asigauto = AutoChofer(
									idAuto = auto,
									idChofer = chofer,
									placas = auto.placas
									)
								asigauto.save()
								chofer.idAuto = auto
								auto.status = Automovil.estados['ASIGNADO']
								chofer.save(update_fields = ['idAuto'])
								auto.save(update_fields = ['status'])
								return enviarJson({'mensaje': 'Ha sido asignado satisfactoriamente'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'El chofer ya tiene asignado', 'campo': 'chofer'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'El auto no se puede asignar', 'campo': 'placas'}, stat.UNPROCESSABLE)
					else:
						if auto.status == Automovil.estados['DISPONIBLE'] or auto.status == Automovil.estados['REGISTRADO']:
							if chofer.idAuto == None:
								reasignar.fechaAsignacion = datetime.datetime.now()
								auto.status = Automovil.estados['ASIGNADO']
								chofer.idAuto = reasignar.idAuto
								reasignar.save(update_fields = ['fechaAsignacion'])
								auto.save(update_fields = ['status'])
								chofer.save(update_fields = ['idAuto'])
								return enviarJson({'mensaje': 'Ha sido asignado satisfactoriamente'}, stat.OK)
							else:
								return enviarJson({'mensaje': "El chofer ya tiene asignado", 'campo': 'chofer'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'El auto no se puede asignar', 'campo': 'placas'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El auto indicado no existe', 'campo': 'placas'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'El chofer indicado no existe', 'campo': 'chofer'}, stat.NOT_FOUND)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'chofer, placas'}, stat.BAD_REQUEST)

	@csrf_exempt
	def vieLiberarAsig(request):
		"""
			Permite liberar al auto y al chofer para que esten disponibles para
			una nueva asignación.
		"""
		datos = recibirJson(request)
		if val.validarExistencia(datos, ['placas', 'chofer']):
			auto = Automovil.verfAuto(datos['placas'])
			if auto:
				chofer = Automovil.verfChofer(datos['chofer'])
				if chofer:
					par = Automovil.verfPar(chofer, auto)
					if par:
						if chofer.idAuto == auto and auto.status == Automovil.estados['ASIGNADO']:
							auto.status = Automovil.estados['DISPONIBLE']
							chofer.idAuto = None
							auto.save()
							chofer.save()
							return enviarJson({'mensaje': 'Chofer y auto liberados'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'No se puede liberar', 'campo': 'placas, chofer'}, stat.UNPROCESSABLE)
					else:
						return enviarJson({'mensaje': 'Esa asignacion no existe', 'campo': 'placas, chofer'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El auto no existe', 'campo': 'placas'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'El auto ya no existe', 'campo': 'placas'}, stat.NOT_FOUND)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'placas, chofer'}, stat.BAD_REQUEST)

	@csrf_exempt
	def vieEstado (request):
		"""
			Permite cambiar el estado de un auto para conocer
			si es posible asignarlo o no.
		"""
		datos = recibirJson(request)

		if 'estado' in datos and 'placas' in datos:
			auto = Automovil.verfAuto (datos['placas'])
			if auto:
				if auto.status != Automovil.estados['ASIGNADO']:
					if datos['estado'] in Automovil.estados and datos['estado'] != 'REGISTRADO' and \
					 datos['estado'] != 'ASIGNADO':
						auto.status = Automovil.estados[datos['estado']]
						auto.save()
						return enviarJson({'mensaje': 'Estado cambiado'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'El estado indicado es invalido', 'campo': 'estado'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El auto esta en uso', 'campo': 'placas'}, stat.UNPROCESSABLE) 
			else:
				return enviarJson({'mensaje': 'El auto no existe', 'campo': 'placas'}, stat.NOT_FOUND)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'placas'}, stat.BAD_REQUEST)

	def vieTenerDatos (datos):
		"""
			PERMITE OBTENER LOS DATOS DE UN OBJETO Auto
		"""
		auto = Automovil.verfAuto(datos)
		if auto:
			""" Debería implementarse un filtro para que 
			esta información sólo lo obtenga personal autorizado"""
			campos = {}
			campos['placas'] = auto.placas
			campos['marca'] = auto.marca
			campos['modelo'] = auto.modelo
			campos['tipo'] = auto.tipo
			campos['status'] = auto.status
			campos['fecha'] = auto.fechaRegistro
			campos['color']	= auto.color
			return enviarJson(campos, stat.OK)
		else:
			return enviarJson({'mensaje': 'El auto indicado no existe', 'campo': 'placas'}, stat.UNPROCESSABLE)

	def modificar(datos):
		"""
			Permite modificar los datos de un automovil
			mediante las placas que lo identifican.
		"""
		if not datos == None:
			if 'placas' in datos:
				auto = Automovil.verfAuto(datos['placas'])
				campos = []

				if auto:
					# Aplicar filtro de autorización
					debug ("El auto: es " + str(auto))

					if 'marca' in datos and not datos['marca'] == None:
						auto.marca = datos['marca']
						campos.append('marca')
					if 'placasnvo' in datos and not datos['placasnvo'] == None:
						mayplacas = datos['placasnvo'].upper()
						if val.validarPlacas(mayplacas):
							busplacas = AutoMdl.objects.filter(placas = mayplacas)
							if len(busplacas) == 0:
								auto.placas = mayplacas
								campos.append('placas')
							else:
								return enviarJson({'mensaje': 'Placas repetidas', 'campo': 'placas'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'Placas incorrectas', 'campo': 'placas'}, stat.UNPROCESSABLE)

					if 'modelo' in datos and not datos['modelo'] == None:
						auto.modelo = datos['modelo']
						campos.append('modelo')
					if 'tipo' in datos and not datos['tipo'] == None:
						auto.tipo = datos['tipo']
						campos.append('tipo')
					if 'color' in datos and not datos['color'] == None:
						auto.color = datos['color']
						campos.append('color')

					if len(campos) > 0:
						for clave in campos:
							auto.save(update_fields = [clave])
						return enviarJson({'mensaje': 'Cambios guardados'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'Guardado sin cambios'}, stat.OK)
				else:
					return enviarJson({'mensaje': 'El auto indicado no existe', 'campo': 'placas'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'No se detecto las placas', 'campo': 'placas'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'No hay campos', 'campo': 'datos vacio'}, stat.BAD_REQUEST)

	def verfAuto (datos):
		"""
			Verificar si existe el auto, mediante sus placas.
			Retorna el auto.
		"""
		if not datos == None:
			try:
				auto = AutoMdl.objects.get(placas = datos)
				return auto
			except AutoMdl.DoesNotExist as e:
				debug (e)
				return False
		else:
			return False

	def verfPar (chofer, auto):
		"""
			Verificar si existe el par de usuario y chofer
			en la tabla AutoChofer
		"""
		try:
			par = AutoChofer.objects.get(idChofer = chofer, idAuto = auto)
			debug (par)
			return par
		except AutoChofer.DoesNotExist as e:
			debug (e)
			return False

	def verfChofer (datos):
		"""
			Verificar si existe el chofer, mediantte su ID.
		"""
		if not datos == None:
			try:
				chofer = ChoferMdl.objects.get(idChofer = datos)
				if not chofer.idChofer == None:
					return chofer
				else:
					return False
			except ChoferMdl.DoesNotExist as e:
				debug (e)
				return False
		else:
			return False
