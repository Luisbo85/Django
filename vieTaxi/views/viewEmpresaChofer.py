from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
import vie.tools.validar		as val
from vie.tools.errores 		import Status as stat
from vie.views.views	import recibirJson, enviarJson, debug
from vie.models			import EmpresaChofer

class Empresa:

	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			return Empresa.vieRegEmpresa(datos)
		elif request.method == 'GET':
			q = request.GET.get("idEmpresa", None)
			if q:
			    return Empresa.vieTenerDatos(q)
			else:
				return enviarJson({'mensaje': 'No se detecto el ID'}, stat.UNPROCESSABLE)
		elif request.method == 'PUT':
			datos = recibirJson(request)
			return Empresa.vieModificar(datos)
		elif request.method == 'DELETE':
			return enviarJson({'mensaje': 'Enviaste un delete'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce'}, stat.NOT_ALLOW)

	def vieRegEmpresa (datos):
		if val.validarExistencia(datos, ['nombre', 'domicilio', 'representante', 'tel']):
			tels = EmpresaChofer.objects.filter(tel = datos['tel'])
			if len(tels) == 0:
				empresa = EmpresaChofer(
					nombre = datos['nombre'],
					domicilio = datos['domicilio'],
					representante = datos['representante'],
					telefono = datos['tel']
					)
				empresa.save()
			else:
				return enviarJson({'mensaje': 'Telefono repetido'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan campos'}, stat.BAD_REQUEST)

	def vieTenerDatos(datos):
		empresa = Empresa.verfEmpresa(datos)

		if empresa:
			campos = {}
			campos['nombre'] = empresa.nombre
			campos['domicilio'] = empresa.domicilio
			campos['representante'] = empresa.representante
			campos['tel'] = empresa.telefono
			return enviarJson(campos, stat.OK)
		else:
			return enviarJson({'mensaje': 'La empresa indicada no existe'}, stat.NOT_FOUND)

	def vieModificar(datos):
		if 'idEmpresa' in datos:
			empresa = Empresa.verfEmpresa(datos['idEmpresa'])
			campos = []

			if empresa:
				if 'nombre' in datos:
					empresa.nombre = datos['nombre']
					campos.append('nombre')
				if 'domicilio' in datos['domicilio']:
					empresa.domicilio = datos['domicilio']
					campos.append('domicilio')
				if 'representante' in datos:
					empresa.representante = datos['representante']
					campos.append('representante')
				if 'tel' in datos:
					tels = EmpresaChofer.objects.filter(telefono = datos['tel'])
					if len(tels) == 0:
						empresa.telefono = datos['tel']
						campos.append('telefono')
					else:
						return enviarJson({'mensaje': 'Telefono repetido'}, stat.UNPROCESSABLE)

				if len(campos) > 0:
					for clave in campos:
						empresa.save(update_fields = [clave])
					return enviarJson({'mensaje': 'Cambios guardados'}, stat.OK)
			else:
				return enviarJson({'mensaje': 'La empresa no existe'}, stat.NOT_FOUND)
		else:
			return enviarJson({'mensaje': 'No se especifico el ID'}, stat.BAD_REQUEST)

	def verfEmpresa (datos):
		if not datos == None:
			try:
				empresa = EmpresaChofer.objects.get(idEmpresa = datos)
				return empresa
			except EmpresaChofer.DoesNotExist as e:
				debug(e)
				return False
		else:
			return False