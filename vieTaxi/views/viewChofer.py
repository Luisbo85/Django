import random as rand
import datetime
import rethinkdb as r
import conekta

from django.shortcuts				import render
from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from django.utils.crypto 			import get_random_string
from django.http 					import QueryDict, HttpResponse
import vie.tools.validar		as val
from Django.settings 				import DEBUG, EMPRESAS, APPS_VIE

from vie.views.views	import recibirJson, enviarJson, generarToken, verfUsuario, verfEmpresaCh, debug, verfChofer
from vie.models			import Chofer as ChoferMdl, Auto as AutoMdl, Usuario as UsuarioMdl, Persona, AutoChofer
from viePay.views.viewMetodoPago import MetodoPago
from vie.tools.errores		import Status as stat
from vie.views.viewUsuario 			import Usuario
from vie.views.viewChofer			import Chofer as vChofer
from vieTaxi.views.viewAuto				import Automovil


class Chofer:
	"""
		Vista que controla opciones del Chofer.
	"""

	@csrf_exempt
	def obtenerPeticion (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return vChofer.vieRegistroChofer(datos, APPS_VIE['TAXI_CHOFER'])
		elif request.method == 'GET':
			q = request.GET.get("token", None)
			if q:
			    return vChofer.vieTenerDatos(str(q), APPS_VIE['TAXI_CHOFER'])
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.UNPROCESSABLE)
		elif request.method == 'PUT':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return vChofer.modificar(datos, APPS_VIE['TAXI_CHOFER'])
		elif request.method == 'DELETE':
			return enviarJson({'mensaje': 'Enviaste un delete'}, stat.OK)
		else:
			return enviarJson({'mensaje': 'El metodo HTTP no se reconoce', 'campo': 'metodo HTTP'}, stat.NOT_ALLOW)

	@csrf_exempt
	def getPersona (request):
		"""
			Permite obtener información de chofer
			mediante token de rethinkdb por método GET
		"""
		if request.method == 'GET':
			q = request.GET.get("token", None)
			if q:
				return vChofer.traerDatosPersona(str(q))
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	@csrf_exempt
	def peticionFoto (request):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return Usuario.vieFotobitmap(datos, Usuario.CHOFER)
		elif request.method == 'GET':
			q = request.GET.get("token", None)
			if q:	
			    return Usuario.vieFoto(str(q), Usuario.CHOFER)
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def traerDatosPersona(datos):
		"""
			Método para traer Chofer mediante token de rethinkdb
		"""
		if not datos == None:
			debug (datos)
			# conexión
			#r.connect('localhost', 28015).repl() # local-Lap
			r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

			# abreviación a tabla
			vietaxi = r.db('vie').table('viaje')
			rquery = vietaxi.get(datos).run()

			if not rquery == None:
				debug (rquery)
				debug ("El id de cliente es: " + str(rquery['cliente']) + "\nEl id de cliente: " + str(rquery['cliente']))
				campos = {}
				try:
					debug ("Es cliente y su codigo es: " + str(rquery['cliente']))
					cliente = Persona.objects.get(idPersona = rquery['cliente'])
					try:
						avatar = UsuarioMdl.objects.get(idFila = rquery['cliente'], permiso = Usuario.CLIENTE)
						campos['foto'] = str(avatar.foto)
					except:
						avatar['foto'] = None

					campos['tel'] = None
					if rquery['estatus'] == 0:
						campos['tel'] = cliente.telefono
					debug ("La persona es: " + str(cliente))
					campos['nombre'] = cliente.nombre
					campos['apellidos'	]  = cliente.apellidos
					campos['calificacion'] = round(cliente.calificacion, 1)
					return enviarJson(campos, stat.OK)
				except Persona.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a una persona', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)

	def traerDatosUsuario(datos):
		"""
			Método para extraer los datos de un chofer,
			si es persona, sólo algunos datos
		"""
		return vChofer.traerDatosUsuario(datos, APPS_VIE['TAXI_CHOFER'])
		

	@csrf_exempt
	def calificar(request):
		"""
			Metodo que llama el método de usuario para calificar
			a una persona.
		"""
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
		return Usuario.calificar(datos, Usuario.CHOFER)

	@csrf_exempt
	def buscar (request):
		"""
			Método que permitirá buscar a varios choferes
			por determinados filtros que el usuario ingrese.
			La especificación del funcionamiento estará documentado.
		"""
		if request.method == 'GET':
			return vChofer.buscar(request, EMPRESAS['VIETAXI'])
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'metodo HTTP'}, stat.NOT_ALLOW)

	@csrf_exempt
	def getPetViaje (request):
		"""
			Permite obtener información de chofer
			mediante token de rethinkdb por método GET
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			if val.validarExistencia(datos, ['token', 'token_viaje', 'estado']):
				if datos['estado'] == 0:
					return Chofer.traerDatosPetViaje(datos)
				if datos['estado'] == 1:
					return Chofer.traerDatosViaje(datos)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, token_viaje, estado'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def traerDatosPetViaje(datos):
		"""
			Método para traer PeticiónViaje mediante token de rethinkdb
		"""
		if not datos == None:
			#debug (datos)
			# conexión - EN PRODUCCION ESTO FALLARÁ AL DEBUGGEAR!!
			# Hacer lo posible para que se conecte a rethinkdb de produccion
			if DEBUG:
				r.connect('localhost', 28015).repl() # local-Lap
			else:
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor
			rquery = {}
			rquery['idChofer'] = None

			# abreviación a tabla
			vietaxi = r.db('vie').table('peticionViaje')
			rquery = vietaxi.get(datos['token_viaje']).run()

			if not rquery == None:
				#debug (rquery)
				#debug ("El id de chofer es: " + str(rquery['idChofer']) + "\nEl id de cliente: " + str(rquery['idUsuario']))
				campos = {}

				#consultar con Jarillo
				"""if not rquery['estatus'] == 1:
					return enviarJson({'mensaje': 'El viaje no es valido'}, stat.FORBIDDEN)"""


				user = verfUsuario(datos['token'])
				if not user:
					return enviarJson({'mensaje': 'El chofer no existe'}, stat.UNAUTHORIZED)

				#debug(user.idUsuario)

				if not user.idUsuario.idFila == rquery['idChofer'] and not user.idUsuario.permiso == Usuario.CHOFER:
					return enviarJson({'mensaje': 'El chofer no coincide con el viaje', 'campo': 'token, token_viaje'}, stat.FORBIDDEN)

				try:
					debug ("Es usuario y su codigo es: " + str(rquery['idUsuario']))
					try:
						cliente = Persona.objects.get(idPersona = rquery['idUsuario'])
					except Persona.DoesNotExist:
						return enviarJson({'mensaje': 'No existe el cliente', 'campo':'token_viaje'}, stat.UNPROCESSABLE)

					try:
						avatar = UsuarioMdl.objects.get(idFila = rquery['idUsuario'], permiso = Usuario.CLIENTE)
						campos['foto'] = str(avatar.foto)
					except:
						campos['foto'] = None
					debug("El pago..." + str(rquery['idPago']))
					nombre = str(cliente.nombre) + " " + str(cliente.apellidos)
					campos['fecha'] = None
					campos['nombre'] = nombre
					campos['destino'] = None
					campos['origen'] = rquery['origen']
					campos['tiempoPeticion'] = rquery['tiempoPeticion']
					campos['calificacion'] = round(cliente.calificacion, 1)
					campos['fechaFin'] = None
					campos['pago'] = None
					campos['tel'] = cliente.telefono
					if 'fecha' in rquery:
						campos['fecha'] = rquery['fecha']
					if 'fechaFin' in rquery:
						campos['fechaFin'] = rquery['fechaFin']
					if 'idPago' in rquery:
						campos['pago'] = rquery['idPago']
					if 'destino' in rquery:
						campos['destino'] = rquery['destino']

					return enviarJson(campos, stat.OK)

				except Persona.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a un cliente', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)

	def traerDatosViaje(datos):
		"""
			Método para traer PeticiónViaje mediante token de rethinkdb
		"""
		if not datos == None:
			debug (datos)
			# conexión - EN PRODUCCION ESTO FALLARÁ AL DEBUGGEAR!!
			# Hacer lo posible para que se conecte a rethinkdb de produccion
			if DEBUG:
				r.connect('localhost', 28015).repl() # local-Lap
			else:
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

			# abreviación a tabla
			vietaxi = r.db('vie').table('viaje')
			rquery = vietaxi.get(datos['token_viaje']).run()

			if not rquery == None:
				debug (rquery)
				debug ("El id de chofer es: " + str(rquery['chofer']) + "\nEl id de cliente: " + str(rquery['cliente']))
				
				#Consultar con Jarillo
				"""if not rquery['estatus'] == 0:
					return enviarJson({'mensaje': 'El viaje no es valido'}, stat.FORBIDDEN)"""

				user = verfUsuario(datos['token'])

				if not user:
					return enviarJson({'mensaje': 'El usuario no existe'}, stat.UNAUTHORIZED)

				if not user.idUsuario.idFila == rquery['chofer'] and not user.idUsuario.permiso == Usuario.CHOFER:
					return enviarJson({'mensaje': 'El chofer no coincide con el viaje', 'campo': 'token, token_viaje'}, stat.FORBIDDEN)

				campos = {}

				try:
					debug ("Es cliente y su codigo es: " + str(rquery['cliente']))
					try:
						cliente = Persona.objects.get(idPersona = rquery['cliente'])
					except Persona.DoesNotExist:
						return enviarJson({'mensaje': 'No existe el cliente', 'campo':'token_viaje'}, stat.UNPROCESSABLE)
					
					try:
						avatar = UsuarioMdl.objects.get(idFila = rquery['cliente'], permiso = Usuario.CLIENTE)
						campos['foto'] = str(avatar.foto)
					except:
						campos['foto'] = None
					nombre = str(cliente.nombre) + " " + str(cliente.apellidos)
					campos['status'] = rquery['estatus']
					campos['nombre'] = nombre
					campos['tel'] = cliente.telefono
					campos['origen'] = rquery['origen']
					campos['fechaInicio'] = rquery['tiempoInicio']
					campos['fechaFin'] = None
					campos['calificacion'] = round(cliente.calificacion, 1)
					campos['costo'] = None
					if 'tiempoFin' in rquery:
						campos['fechaFin'] = rquery['tiempoFin']
					if 'costo' in rquery:
						campos['costo'] = rquery['costo']

					return enviarJson(campos, stat.OK)

				except Persona.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a un cliente', 'campo': 'token'}, stat.NOT_FOUND)
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El token no es valido'}, stat.UNPROCESSABLE)

		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'},stat.BAD_REQUEST)

	@csrf_exempt
	def getHistorial (request):
		"""
			Metodo para obtener un historial de los viajes
			realizados por la persona, aqui sólo se recibe el token
			mediante el metodo GET y lo envia al metodo correcto
		"""
		if request.method == 'GET':
			q = request.GET.get("token", None)
			t = request.GET.get("bloque", None)
			if q:
				if t:
					return Chofer.historialViaje(str(q), int(t))
				else:
					return enviarJson({'mensaje': 'No hay num de bloque'}, stat.BAD_REQUEST)
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def historialViaje (datos, bloq):
		"""
			Metodo para obtener el historial
		"""
		salto = 14
		token = verfUsuario(datos)

		if token:
			if token.idUsuario.permiso == Usuario.CHOFER:
				debug("El usuario: " + str(token.idUsuario.idUsuario))
				debug("El cliente: " + str(token.idUsuario.idFila))
				# conexión
				# r.connect('localhost', 28015).repl() # local-Lap
				r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

				# abreviación a tabla
				vietaxi = r.db('vie').table('viaje')
				rquery = vietaxi.filter({'chofer': token.idUsuario.idFila}).order_by(r.desc('tiempoInicio')).skip(bloq*salto).limit(salto).run()
				num = vietaxi.filter({'chofer': token.idUsuario.idFila}).order_by(r.desc('tiempoInicio')).skip(bloq*salto).limit(salto).count().run()

				if not num == 0:
				#	debug(rquery)
					debug ("\nEl bloque es el: " + str(bloq))
					debug ("\nEl salto es de: " + str(bloq*salto))
					debug ("\nFueron un total de: " + str(num))
					lista = []
					for i in rquery:
						debug("---------- Viaje -----------")
						del i['id']
						try:
							cli = Persona.objects.get(idPersona = i['cliente'])
							i['cliente'] = cli.nombre + cli.apellidos
							chof = ChoferMdl.objects.get(idChofer = i['chofer'])
							i['chofer'] = chof.nombre + chof.apellidos
						except Persona.DoesNotExist as e:
							debug(e)
							return enviarJson({'mensaje': 'Cliente no encontrado', 'campo': 'cliente, token'}, stat.UNPROCESSABLE)
						except ChoferMdl.DoesNotExist as f:
							debug(f)
							i['chofer'] = None
							return enviarJson({'mensaje': 'Chofer no encontrado', 'campo': 'chofer, token'}, stat.UNPROCESSABLE)
						for j in i:
							debug(j + ": " + str(i[j]))
						lista.append(i)

					return enviarJson({'resultados': lista}, stat.OK)
				else:
					return enviarJson({'mensaje': 'No se encontro resultados'}, stat.NO_CONTENT)
			else:
				return enviarJson({'mensaje': 'No se permite acceso'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Token no valido'}, stat.BAD_REQUEST)

	@csrf_exempt
	def verfsus(request):
		return vChofer.verfsus(request, APPS_VIE['TAXI_CHOFER'])
