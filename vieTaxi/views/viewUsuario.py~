import random as rand

from django.shortcuts				import render
from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from django.utils.crypto 			import get_random_string

import vie.validar		as val
from vie.views.views	import recibirJson, enviarJson, generarToken
from vie.models			import CodValidacion, Usuario as UsuarioMdl, Token, MetodoPago as Pago, Persona
from vie.SMS			import SMS
from vie.errores		import Status as stat


class Usuario:
	"""
		Vista que controla las opciones con los usuarios del sistema.
	"""
	#Permisos del usuario
	REGISTRADO 	= 0
	CLIENTE		= 1
	CHOFER		= 2
	#Formas de Login
	L_EMAIL		= 0		#Mediante un correo y contraseña
	L_FACE		= 1		#Mediante una cuenta de Facebook
	L_GOOGLE	= 2		#Mediante una cuenta de Google
	#Formas de pago
	P_EFECTIVO	= 0
	P_DEBITO	= 1
	P_CREDITO	= 2
	P_PAYPAL	= 3
	P_PROMO		= 4
	#Numero de sesiones maximas por usuario
	MAX_SESIONES = 10

	def asignarToken(usuario):
		"""
			A un usuario permite asignarle un token para poder acceder al sistema.
		"""
		if type(usuario) == UsuarioMdl and not(usuario.idUsuario == None):
			intento = 0
			exito = False
			while not exito and intento < 5: #Permite buscar 5 veces un token disponible
				token = generarToken()
				try:
					objetoToken = Token.objects.get(token = token)
					if objetoToken.asignado == True:
						intento += 1
					else: #Si esta disponible el token para poderlo asignar al usuario
						objetoToken.asignado	= True
						objetoToken.idUsuario 	= usuario
						objetoToken.fecha		= timezone.now()
						objetoToken.save(update_fields = ['asignado','idUsuario','fecha'])
						exito = True
					
				except Token.DoesNotExist: #No existe el token en la base de datos por lo que se debe de crear el campo
					crearToken	= Token(
						token		= token,
						asignado	= True,
						idUsuario	= usuario
					)
					crearToken.save()
					exito = True
				
			if not exito: #Buscar tokens no asignados
				objetoToken = Token.objects.filter(asignado = False)
				if len(objetoToken) > 0: # Designar un token dentro de los que estan disponibles
					#Elige siempre el primero disponible. Se puede mejorar para evitar la concurrencia
					objetoToken = objetoToken[0]
					objetoToken.asignado	= True
					objetoToken.idUsuario 	= usuario
					objetoToken.fecha		= timezone.now()
					objetoToken.save(update_fields = ['asignado','idUsuario','fecha'])
					token = objetoToken.token
				else: # No se encontro un token disponible
					token = False
		else:
			token = False
		return token

	def generarCodValidacion():
		"""
			Metodo que se dedica a generar los numeros de validación que se enviaran por mensaje.
			Estos son generados de forma aletoria del 0 al 100,000-
		"""
		return rand.randint(0, 100000) #Retorna un numero entre 0 y 100,000
		
	
	def registroForm(request):	
		"""
			Se encarga de generar un token para el formulario solicitado para evitar el CSRF
		"""
		return render(request, 'vie/usuarioRegistro.html')
			
	@csrf_exempt
	def vieRestLogin(request):
		"""
			Valida que el usuario este registrado para permitirle el acceso al sistema mediante la
			asignaci´on de un Token
		"""
		datos = recibirJson(request)
		if 'modo' in datos:
			modo = datos['modo']
			del datos['modo']
			exito = False
			if modo == Usuario.L_EMAIL:
				if ('email' in datos) and ('password' in datos):
					try:
						usuario = UsuarioMdl.objects.get(email = datos['email'])
						if usuario.permiso == Usuario.REGISTRADO:
							return enviarJson({'mensaje':'Error: No ha terminado su registro'}, stat.UNPROCESSABLE)
						else:
							if usuario.pw == datos['password']:
								exito = True
							else:
								return enviarJson({'mensaje': 'Error: Contraseña invalida'}, stat.UNPROCESSABLE)
					except UsuarioMdl.DoesNotExist as u:
						print(u)
						return enviarJson({'mensaje': 'Error: No existe el correo indicado'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'Error: Faltan campos'}, stat.UNPROCESSABLE)
			elif modo == Usuario.L_FACE:
				if ('facebook1' in datos) and ('facebook2' in datos):
					try:
						usuario = UsuarioMdl.objects.get(facebook1 = datos['facebook1'], facebook2 = datos['facebook2'])
						if usuario.permiso == Usuario.REGISTRADO:
							return enviarJson({'mensaje':'Error: No ha terminado su registro'}, stat.UNPROCESSABLE)
						else:
							exito = True
					except UsuarioMdl.DoesNotExist as u:
						print(u)
						return enviarJson({'mensaje': 'Error: No existe la cuenta de Facebook indicada'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'Error: Faltan campos'}, stat.UNPROCESSABLE)
			elif modo == Usuario.L_GOOGLE:
				if ('google1' in datos) and ('google2' in datos):
					try:
						usuario = UsuarioMdl.objects.get(facebook1 = datos['google1'], facebook2 = datos['google2'])
						if usuario.permiso == Usuario.REGISTRADO:
							return enviarJson({'mensaje':'Error: No ha terminado su registro'}, stat.UNPROCESSABLE)
						else:
							exito = True
					except UsuarioMdl.DoesNotExist as u:
						print(u)
						return enviarJson({'mensaje': 'Error: No existe la cuenta de Google indicada'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'Error: Faltan campos'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'No se pudo reconocer el modo de acceso'}, stat.UNPROCESSABLE)
			if exito:
				#Verifica cuantos token tiene asignado el usuario
				lista = Token.objects.filter(idUsuario = usuario)
				if len(lista) < Usuario.MAX_SESIONES:
					intentos = 0
					#Intentar conseguir un token para el usuario actual
					while intentos < 3:
						token = Usuario.asignarToken(usuario)
						if token == False:
							intentos += 1
						else:
							break
					if token == False:
						return enviarJson({'mensaje': 'Intentar más tarde'}, stat.FORBIDDEN)
					else:
						return enviarJson({'token': token,'mensaje': 'Acceso exitoso'}, stat.OK)
				else:
					return enviarJson({'mensaje':'Tiene muchas sesiones abiertas, por favor verifique o cierre todas ellas'}, stat.NOT_ALLOW)
				
		else:
			return enviarJson({'mensaje': 'No se especifico el modo de acceso'}, stat.UNPROCESSABLE)
		
	@csrf_exempt
	def vieRestLogout(request):
		"""
			Retira el acceso al usuario debido a su petición. Libera el token que tiene asignado
		"""
		datos = recibirJson(request)
		if 'token' in datos:
			try:
				token = Token.objects.get(token = datos['token'])
				token.asignado	= False
				token.idUsuario	= None
				token.save(update_fields = ['asignado','idUsuario'])
				return enviarJson({'mensaje': 'Usuario deslogueado'})
			except Token.DoesNotExist as t:
				print(t)
				return enviarJson({'mensaje': 'No existe el token enviado'}, stat.NOT_ALLOW)
		else:
			return enviarJson({'mensaje':'No se recibio el token'}, stat.UNPROCESSABLE)
	
	@csrf_exempt
	def vieRestRegistro(request):
		"""
			Método que permite captuar a una persona, y de aquí
			captuar al usuario o chofer, según el caso
		"""
		datos = recibirJson(request)
		if 'permiso' in datos:
			perm = datos['permiso']

			if perm == Usuario.CLIENTE:
				if val.validarExistencia(datos, ['nombre', 'apellidos', 'tel', 'rfc']):
					telefono = Persona.objects.filter(telefono = datos['tel'])
					if len(telefono) == 0:
						if datos['rfc'] != "":
							mayrfc = datos['rfc'].upper()
							rfcs = Persona.objects.filter(rfc = mayrfc)
							datos['rfc'] = mayrfc
							if not val.validarRFC(mayrfc):
								return enviarJson({'mensaje': 'El RFC es invalido'}, stat.UNPROCESSABLE)
							if len(rfcs) != 0:
								return enviarJson({'mensaje': 'RFC repetido'}, stat.UNPROCESSABLE)

						pers = Persona(
							nombre = datos['nombre'],
							apellidos = datos['apellidos'],
							telefono = datos['tel'],
							rfc = datos['rfc']
							)
						pers.save()
						""" El codigo de la persona debe enviarse al registro de usuario
						 para su referencia """
						print ('El código de la persona: ' + str(pers.idPersona))
						datos['idFila'] = pers.idPersona
						Usuario.vieRestUsuario(datos, Usuario.CHOFER)
						return enviarJson({'mensaje': 'Registrado con exito'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'El numero de telefono existe'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'Faltan campos'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'No se ha determinado un permiso'}, stat.UNPROCESSABLE)

	def vieRestUsuario(datos, perm):
		"""
			Este método debería ser accedido mediante una llamada,
			y recibir el codigo de persona correspondiente así como permiso.
			-Recibe también el permiso como parámetro (definir si es mejor recibir como parámetro agregar al diccionario)
			-El modo de registro sigue siendo el mismo, sea correo, face o google,
			 en caso de ser cliente.
			-Para el caso de chofer sólo será validado con el email.
		"""
		if 'modo' in datos:
			modo = datos['modo']
			if modo == Usuario.L_EMAIL:
				print ("datos....")
				print (datos)
				if val.validarExistencia(datos, ['email', 'password', 'tel', 'idFila']):
					if val.validarPassword(datos['password']):
						correos = UsuarioMdl.objects.filter(email = datos['email'])
						if len(correos) == 0:
							usuario = UsuarioMdl(
								permiso = perm,
								idFila = datos['idFila'],
								email = datos['email'],
								pw = datos['password'],
								tel = datos['tel']
								)
							usuario.save()
							print('mensaje: Registrado con exito, stat.OK')
						else:
							print('mensaje: Correo repetido, stat.UNPROCESSABLE')
					else:
						print('mensaje: Contraseña inválida, stat.UNPROCESSABLE')
				else:
					print('mensaje: Faltan campos, stat.UNPROCESSABLE')

			elif modo == Usuario.L_FACE:
				print (datos)
				if val.validarExistencia(datos,['facebook1', 'facebook2', 'idFila']):
					usuario = UsuarioMdl(
						facebook1	= datos['facebook1'],
						facebook2	= datos['facebook2'],
						permiso = perm,
						idFila = datos['idFila']
					)
					usuario.save()
					print ('mensaje: Registrado con exito, stat.OK')
				else:
					print ('Error: Faltan campos, stat.UNPROCESSABLE')

			elif modo == Usuario.L_GOOGLE:
				print (datos)
				if val.validarExistencia(datos, ['google1', 'google2', 'idFila']):
					if len(datos['google1']) > 0 and len(datos['google2']) > 0:
						usuario = UsuarioMdl(
							google1	= datos['google1'],
							google2	= datos['google2'],
							permiso = perm,
							idFila = datos['idFila']
						)
						usuario.save()
						print ('mensaje: Registrado con exito, stat.OK')
					else:
						print ('Error: Los campos enviados no tienen una longitud valida, stat.UNPROCESSABLE')
				else:
					print ('Error: Faltan campos, stat.UNPROCESSABLE')
			else:
				print ('mensaje: El metodo de registro no es reconocido, stat.UNPROCESSABLE')
		else:
			print ('mensaje: No se especifico el metodo de registro, stat.UNPROCESSABLE')

	@csrf_exempt
	def vieRestRegistroAnt(request):
		"""
			Metodo que permite recibir un JSON con la informacion necesaria para registrar un
			usuario del sistema.
		"""
		respuesta = {}
		datos = recibirJson(request)
		if 'modo' in datos:
			modo = datos['modo']
			exito = False
			if modo == Usuario.L_EMAIL:
				if val.validarExistencia(datos, ['nombre', 'apellidos', 'email', 'password', 'tel']):
					if val.validarPassword(datos['password']):
						correos = UsuarioMdl.objects.filter(email = datos['email'])
						if len(correos) == 0:
							usuario = UsuarioMdl(
								nombre		= datos['nombre'],
								apellidos	= datos['apellidos'],
								email		= datos['email'],
								pw			= datos['password'],
								tel			= datos['tel']
							)
							exito = True
						else:
							mensaje = 'Error: Correo repetido'
					else:
						mensaje = 'Error: Password invalida'
				else:
					mensaje = 'Error: Faltan campos'
			elif modo == Usuario.L_FACE:
				print(datos)
				if ('facebook1' in datos) and ('facebook2' in datos):
					usuario = UsuarioMdl(
						facebook1	= datos['facebook1'],
						facebook2	= datos['facebook2']
					)
					exito = True
				else:
					mensaje = 'Error: Faltan campos'
			elif modo == Usuario.L_GOOGLE:
				if ('google1' in datos) and ('google2' in datos):
					if len(datos['google1']) > 0 and len(datos['google2']) > 0:
						usuario = UsuarioMdl(
							google1	= datos['google1'],
							google2	= datos['google2'],
						)
						exito = True
					else:
						mensaje = 'Error: Los campos enviados no tienen una longitud valida'
				else:
					mensaje = 'Error: Faltan campos'
			else:
				mensaje = 'Error: Metodo de registro no identificado'
			if exito:
				usuario.save()
				token = Usuario.asignarToken(usuario)
				while token == False: #Encontrar un token para el usuario
					token = Usuario.asignarToken(usuario)						
				print('ID Usuario: '+str(usuario.idUsuario))
				print('Token: '+str(token))
				codigoValidacion = Usuario.generarCodValidacion()
				print('Codigo generado: '+str(codigoValidacion))
				#Crear un registro en la bases de datos o asignar uno
				codigo = CodValidacion(
					codigo			= codigoValidacion,
					idUsuario 		= usuario,
					fechaAsignacion = timezone.now()
				)
				codigo.save()
				respuesta['token'] = token
				"""sms = SMS()
				if sms.EnviarCodConfirmacion(codValidacion):
					mensaje = 'Usuario registrado'
				else:
					mensaje = 'No se envio mensaje de registro'"""
				mensaje = 'Usuario registrado'
		else:
			mensaje = 'Error: No se especifico el modo de registro'
		respuesta['mensaje'] = mensaje
		return enviarJson(respuesta)

	@csrf_exempt
	def vieValidarCod(request):
		"""
			Metodo que recibe un JSON para validar un codigo enviado al usuario para autentificar
			la posecion del celular
		"""
		datos = recibirJson(request)
		if ('token' in datos) and ('codigo' in datos):
			try:
				token = Token.objects.get(token = datos['token'])
				try:
					codigo = CodValidacion.objects.get(codigo = datos['codigo'])
					if not codigo.idUsuario == None and token.idUsuario.idUsuario == codigo.idUsuario.idUsuario:
						usuario				= codigo.idUsuario
						usuario.permiso		= Usuario.CLIENTE
						usuario.save(update_fields = ['permiso'])
						codigo.idUsuario	= None
						codigo.save(update_fields = ['idUsuario'])
						return enviarJson({'mensaje':'El usuario ya fue validado'}, stat.OK)
					else:
						return enviarJson({'mensaje':'El codigo enviado no esta asignado al usuario'}, stat.NOT_ALLOW)
				except CodValidacion.DoesNotExist as c:
					print(c)
					return enviarJson({'mensaje': 'El codigo recibido no existe'}, stat.NOT_ALLOW)
			except Token.DoesNotExist as e:
				print(e)
				return enviarJson({'mensaje': 'No existe el usuario indicado'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan datos, token!'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def vieMetodoPago(request):
		""""
				METODO PARA EL CONTROL DE LOS POSIBLES METODOS DE PAGO QUE SE 
				EFECTUARAN EN EL SISTEMA, HASTA AHORA			"""
		datos = recibirJson(request)

		if ('token' in datos) and ('md_pago' in datos):
			try:
				usuario = Usuario.verfUsuarioExiste(datos['token']) 
				if usuario:
					md_pago = datos['md_pago']
					if md_pago == Usuario.P_EFECTIVO:
						pago = Pago(idUsuario = usuario, formaPago = datos['md_pago'])
						pago.save()
						return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
					
					elif md_pago == Usuario.P_CREDITO:
						if 'notarjeta' in datos and 'norev' in datos:
							if len(datos['notarjeta']) == 16 and len(datos['norev']) == 4:
								pago = Pago(idUsuario = usuario, formaPago = datos['md_pago'],
								codigo = datos['notarjeta'], codigorev = datos['norev'])
								pago.save()
								return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Revise los codigos de tarjeta'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'Faltan datos de la tarjeta'}, stat.UNPROCESSABLE)

					elif md_pago == Usuario.P_PAYPAL:
						if 'paypal' in datos:
							pago = Pago(idUsuario = usuario, formaPago = datos['md_pago'])
							pago.save()
							return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'Falta el paypal'}, stat.UNPROCESSABLE)
					
					elif md_pago == Usuario.P_PROMO:
						if 'promo' in datos:
							if len(datos['promo']) == 6:
								pago = Pago(idUsuario = usuario, formaPago = datos['md_pago'])
								pago.save()
								return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Verifique el codigo de la promo'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'Falta la promo'}, stat.UNPROCESSABLE)
					else:
						return enviarJson({'mensaje': 'No se reconoce el metodo de pago'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El usuario no es valido'}, stat.UNAUTHORIZED)
			except Token.DoesNotExist as e:
				print (e)
				return enviarJson({'mensaje': 'No existe el usuario indicado'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan datos, el token!'}, stat.UNPROCESSABLE)

	def verfUsuarioExiste(datos):
		"""Metodo para verificar si un usuario existe """

		if not datos == None:
			try:
				token = Token.objects.get(token = datos)

				if not token.idUsuario == None:
					return token.idUsuario
				else:
					return False

			except Token.DoesNotExist as e:
				print (e)
				return False
		else:
			return False