from django.apps import AppConfig


class VieTaxiConfig(AppConfig):
    name = 'vieTaxi'

    path = 'vieTaxi/'