from __future__ import unicode_literals

# Create your models here.
from django.db import models
from vie.models import Usuario

class Zona(models.Model):
	idZona	       = models.AutoField       (primary_key = True)
	zona           = models.CharField       (max_length  = 80)
	#Coordenas de la esquina inferior izquierda
	infe_lat       = models.DecimalField    (max_digits  = 10, decimal_places = 7, default = 0)
	infe_lng       = models.DecimalField    (max_digits  = 10, decimal_places = 7, default = 0)
	#Coordenadas de la esquina superior derecha
	sup_lat        = models.DecimalField    (max_digits  = 10, decimal_places = 7, default = 0)
	sup_lng        = models.DecimalField    (max_digits  = 10, decimal_places = 7, default = 0)
	#Datos de las tarifas
	baseD          = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	baseN          = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	costoMinD      = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	costoMinN      = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	costoKmD       = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	costoKmN       = models.DecimalField    (max_digits  = 10, decimal_places = 3, default = 0)
	horarioInicioD = models.DateTimeField	(auto_now_add = False)
	horarioFinD    = models.DateTimeField	(auto_now_add = False)
	