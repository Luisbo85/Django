# -*- coding: utf-8 -*-
from django.conf             import settings
from django.conf.urls        import url
from django.conf.urls.static import static

#from vie.tools.telefono             import Movil
from vie.tools.webhooks             import Webhooks                    
from vie.views 				        import views, viewAdmin
from vie.views.viewUsuario	        import Usuario
from vie.views.viewChofer           import Chofer
from vie.views.viewPersona          import Persona
from vie.views.viewEmpresaChofer    import Empresa

urlpatterns = [
    url(r'^$',                      views.index, name='index'),
    url(r'^admin/$',                viewAdmin.dirigir),
    url(r'^chofer/lista/$',         Chofer.buscar),
    url(r'^webhooks/twilio/$',      Webhooks.twilio),
    url(r'^webhooks/$',             Webhooks.conekta),
    url(r'^empresa/$',              Empresa.obtenerPeticion),
    #Url no encontrada
  	#url(r'^', views.paginaNoEncontrada),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
