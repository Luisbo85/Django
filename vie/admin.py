from django.contrib import admin
from vie.models		import *

# Register your models here.
admin.site.register(CodValidacion)
admin.site.register(Usuario)
admin.site.register(EmpresaChofer)
admin.site.register(Token)
admin.site.register(Auto)
admin.site.register(Chofer)
admin.site.register(AutoChofer)
admin.site.register(Persona)
admin.site.register(Comentarios)