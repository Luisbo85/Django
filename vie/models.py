from __future__ import unicode_literals

# Create your models here.
from django.db import models

class Usuario(models.Model):
	"""
		Modelo de los Usuarios del sistema.
	"""
	idUsuario		= models.AutoField			(primary_key	= True)
	permiso			= models.SmallIntegerField	(default		= 0)
	idFila			= models.SmallIntegerField	(default		= 0)
	foto 		  	= models.TextField			(max_length		= 51000, null = True)
	email			= models.EmailField			(max_length 	= 50, 	null				= True,	db_index = True, unique = True, blank = True)
	pw				= models.CharField			(max_length 	= 90, 	null				= True, blank = True)
	pin 			= models.IntegerField 		(default 		= 0)
	facebook1		= models.CharField			(max_length		= 50, 	null				= True, db_index = True, blank = True)
	google1			= models.CharField			(max_length		= 50, 	null				= True, db_index = True, blank = True)
	modoRegistro	= models.SmallIntegerField	(default 		= -1)
	clienteID       = models.CharField          (max_length     = 50, null = True,	db_index = True) #Token de cliente en conekta


	def __str__(self):
		if not (self.email == None):
			return self.email
		elif not(self.facebook1 == None):
			return 'Facebook '+str(self.idUsuario)
		elif not(self.google1 == None):
			return 'Google '+str(self.idUsuario)
		else:
			return 'o.0  ??'

	def asignarModelo(self, modelo):
		self.modelo = modelo

class Token(models.Model):
	"""
		Modelo de los tokens del sistema que permite designar si un usuario tiene acceso al sistema
	"""
	token		= models.CharField		(max_length		= 10, primary_key = True)
	app         = models.SmallIntegerField (default = -1)
	idUsuario	= models.ForeignKey		(Usuario, on_delete	= models.CASCADE, null = True, blank = True)
	asignado	= models.BooleanField	(default		= False)
	#Fecha tiene auto_now_add en True para poder modificar el campo
	fecha		= models.DateTimeField	(auto_now_add	= True)
	vence 		= models.DateTimeField	(auto_now_add 	= False, null = True)
	
	def __str__(self):
		return self.token

class CodValidacion(models.Model):
	"""
		Modelo de los Codigos de Validacion enviados para saber si es el usuario es valido
	"""
	codigo			= models.AutoField		(primary_key 	= True)
	fechaAsignacion	= models.DateTimeField	(auto_now_add 	= True)
	idUsuario		= models.ForeignKey		(Usuario, on_delete	= models.CASCADE, null = True, blank = True)
											
	def __str__(self):
		return str(self.codigo)

class Auto (models.Model):
	"""
		Modelo para gestion de los autos relacionados a Chofer.
	"""
	idAuto			= models.AutoField			(primary_key  = True)
	placas			= models.CharField			(max_length   = 7, db_index = True,	unique 	= True, null = False)
	marca			= models.CharField			(max_length	  = 20, null = False)
	color			= models.CharField			(max_length		= 20, default = 'Blanco')
	modelo 			= models.SmallIntegerField	(default 	  = 0, null = False)
	tipo 			= models.SmallIntegerField	(default	  = 0)
	estatus			= models.SmallIntegerField	(default	  = 0)
	fechaRegistro	= models.DateField          (auto_now_add = True)

	def __str__ (self):
		return str(self.idAuto) + '-' + str(self.placas)

class EmpresaChofer (models.Model):
	idEmpresa	    = models.AutoField		(primary_key 	= True)
	nombre		    = models.CharField		(max_length 	= 45)
	domicilio	    = models.CharField		(max_length		= 90)
	representante	= models.CharField	    (max_length		= 45)
	telefono	    = models.CharField		(max_length 	= 20)

	def __str__(self):
		return self.nombre
		
class Chofer (models.Model):
	"""
		Modelo para gestion del Chofer.
	"""
	idChofer		= models.AutoField			(primary_key	= True)
	idEmpresa		= models.ForeignKey			(EmpresaChofer,	on_delete = models.CASCADE, null = True, blank = True)
	empresa 		= models.SmallIntegerField	(default = -1) 		
	idAuto 			= models.ForeignKey			(Auto,			on_delete = models.CASCADE, null = True, blank = True)
	estatus			= models.SmallIntegerField	(default 		= 0, blank = True)
	nombre			= models.CharField			(max_length 	= 30)
	apellidos		= models.CharField			(max_length 	= 40)
	fechaInicio		= models.DateTimeField		(auto_now_add = True)
	calificacion	= models.DecimalField		(max_digits		= 6, decimal_places	= 5, default = 0.00000)
	num_viajes		= models.IntegerField 		(default		= 0)
	telefono 		= models.CharField			(max_length 	= 20)
	domicilio		= models.CharField			(max_length		= 90)
	rfc				= models.CharField			(max_length		= 20)
	licencia		= models.IntegerField 		(default		= 0)
	vigenciaSubs    = models.DateField          (auto_now_add   = True, null = True)

	def __str__ (self):
		return self.nombre + ' ' + self.apellidos

class AutoChofer (models.Model):
	"""
		Modelo para referencias autos con Chofer
	"""
	idAutoChofer 	= models.AutoField			(primary_key	= True)
	idChofer 		= models.ForeignKey			(Chofer, on_delete = models.CASCADE)
	idAuto 			= models.ForeignKey 		(Auto, on_delete = models.CASCADE)
	placas			= models.CharField			(max_length   = 7)
	fechaAsignacion = models.DateTimeField		(auto_now_add 	= True)

	def __str__ (self):
		return 'Chofer: ' + str(self.idChofer) + ' Auto: ' + str(self.idAuto)

class Persona(models.Model):
	idPersona	  = models.AutoField		(primary_key	= True)
	nombre		  = models.CharField		(max_length 	= 30, 	null			= True, blank = True)
	apellidos	  = models.CharField		(max_length 	= 40, 	null			= True, blank = True)
	telefono	  = models.CharField		(max_length		= 20)
	calificacion  = models.DecimalField		(max_digits		= 6,  	decimal_places	= 5,	default = 0)
	rfc			  = models.CharField		(max_length		= 20, 	null			= True, blank = True)
	fechaRegistro = models.DateTimeField	(auto_now_add = True)
	num_viajes		= models.IntegerField 	(default		= 0)

	def __str__(self):
		return self.nombre + ' ' + self.apellidos


class Comentarios (models.Model):
	"""
		Modelo para registrar los comentarios y calificación a un chofer.
	"""
	idComentario	= models.AutoField			(primary_key	= True)
	idChofer 		= models.ForeignKey			(Chofer, on_delete	= models.CASCADE)
	idPersona 		= models.ForeignKey			(Persona, on_delete	= models.CASCADE)
	evaluador		= models.SmallIntegerField	(default = -1)
	comentario 		= models.CharField 			(max_length	= 100,	null = True, blank = True)
	calificacion	= models.DecimalField		(max_digits = 2, decimal_places = 1,	default	= 0)
	fecha 			= models.DateTimeField		(auto_now_add	= True)

class Codigos(models.Model):
	"""
		Modelo para guardar la estadística de códigos de validación.
	"""
	idCodigo		= models.AutoField 			(primary_key = True)
	codigo 			= models.IntegerField 		(default = -1)
	idUsuario		= models.ForeignKey 		(Usuario, on_delete = models.CASCADE, null = True, blank = True)
	fechaAsignacion	= models.DateTimeField 		(auto_now_add = False)
	fechaUso		= models.DateTimeField 		(auto_now_add = True, null = True, blank = True)
	estado 			= models.SmallIntegerField 	(default = 0)
	def __str__(self):
		return str(self.codigo) + str(self.estado)

"""class FotoUsuario(models.Model):
	idFoto 		= models.AutoField		(primary_key = True)
	idUsuario 	= models.ForeignKey		(Usuario, on_delete = models.CASCADE)
	Foto 		= models.ImageField		(upload_to = 'personas', default = 'personas/vietaxi_user.jpeg')

	def __str__(self):
		return str(self.idFoto) + ' - ' + str(self.idUsuario)"""

class EventoWebhook(models.Model):
	"""
		Modelo que servira para almacenar la informacione enviada al Webhook
	"""
	idEvento      = models.AutoField     (primary_key = True)
	tipoEvento    = models.CharField     (max_length = 40)
	error         = models.BooleanField  (default = False)
	procesado     = models.BooleanField  (default = False)
	idConekta     = models.CharField     (max_length = 30, db_index = True)
	fechaCreacion = models.DateTimeField (auto_now_add = True)
	info          = models.TextField     (max_length = 6000)

class Subscripcion(models.Model):
	"""
		Modelo que maneja un registro de las subscripciones que tiene los usuarios
	"""
	idSubscripcion = models.AutoField(primary_key = True)
	idConekta = models.CharField(max_length = 30, db_index = True)
	idUsuario = models.ForeignKey(Usuario, on_delete = models.CASCADE)
	planID = models.CharField (max_length = 20)
	cobro = models.DecimalField (max_digits = 7, decimal_places = 2)
	comision = models.DecimalField (max_digits = 6, decimal_places = 2, default = 0)


class Lookup(models.Model):
	"""
		Modelo para almacenar conjunto de valores que varian segun el ambiente. Evitar el hardcordear valores en el codigo
	"""
	idLookup = models.AutoField(primary_key = True)
	#Uso que se le da al lookup
	tipo = models.SmallIntegerField(default = 0)
	#Nombre del tipo de lookup
	tipoNombre = models.CharField(max_length = 100)
	#Modulo donde se usa el lookup
	modulo = models.CharField(max_length = 50)
	#Nombre del Lookup
	nombre = models.CharField(max_length = 100, unique = True, db_index = True)
	valor = models.CharField(max_length = 400)
	descripcion = models.CharField(max_length = 300)

