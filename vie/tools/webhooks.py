from django.views.decorators.csrf import csrf_exempt
from django.http                  import HttpResponse, HttpResponseBadRequest, HttpResponseServerError
from Django.settings              import EMPRESAS

from vie.views.views              import recibirJson, enviarJson, verfUsuario, debug, determinarCargo
from vie.tools.errores            import Status as stat
import vie.tools.validar          as val
from viePay.views.viewPagos       import Pago
from vie.tools.estatus            import METODOPAGOS
from vie.views.viewWebhook	      import Webhook


class Webhooks:

	@csrf_exempt
	def conekta(request):
		error = True
		datos = recibirJson(request)
		if datos and 'type' in datos and 'data' in datos:
			try:
				tipoEvento = datos['type']
				if tipoEvento and datos['data'] and datos['data']['object']:
					objecto = datos['data']['object']
					if objecto and objecto['object']:
						if tipoEvento == 'charge.created':
							debug('Pago Creado')
						elif tipoEvento == 'charge.paid':
							#CARGO PAGADO
							debug('Cargo Pagado')
							Webhook.registrarEvento(datos['id'], datos)
						elif tipoEvento == 'order.paid':
							debug('Orden Pagada')
							if objecto['object'] == 'order':
								cargo = Webhooks.obtenerCargoOrden(objecto)
								if cargo and cargo['name']:
									campos = determinarCargo(cargo['name'])
									debug(cargo)
									if campos:
										tipoPago = Webhooks.determinarTipoPago(cargo['payment_method'])
										if tipoPago:
											if campos['empresa'] == EMPRESAS['VIETAXI']:
												if campos['concepto'] == 'TRIP':
													#Registra pago vie
													Pago.registrarPago(EMPRESAS['VIETAXI'], campos['id'], cargo['parent_id'], cargo['fee'], Pago.ESTATUS['PAGADO'], tipoPago, cargo['amount'])
													error = False
												elif campos['concepto'] == 'RECARGA':
													Pago.webRecarga(EMPRESAS['VIEPAY'], campos['id'], cargo['parent_id'], cargo['fee'], Pago.ESTATUS['PAGADO'], tipoPago, cargo['amount'])
													error = False
												else:
													pass
											else:
												if campos['concepto'] == 'RECARGA':
													Pago.webRecarga(EMPRESAS['VIEPAY'], campos['id'], cargo['parent_id'], cargo['fee'], Pago.ESTATUS['PAGADO'], tipoPago, cargo['amount'])
													error = False
												else:
													pass
							else:
								pass #Error	
							if error:
								Webhook.registrarEvento(datos['id'], datos, tipoEvento, True)
							else:
								Webhook.registrarEvento(datos['id'], datos, tipoEvento)
						elif tipoEvento == 'charge.refunded':
							debug('Rembolsar Cobro')
							debug(datos)
							Webhook.registrarEvento(datos['id'], datos, tipoEvento)
						elif tipoEvento == 'charge.chargeback.created':
							debug('Cargo no  autorizado')
						elif tipoEvento == 'subscripcion.created':
							debug('Subscripcio creada')
							debug(datos)
							Webhook.registrarEvento(datos['id'], datos, tipoEvento)
						elif tipoEvento == 'subscripcion.paid':
							debug('Subscripcion Pagada')
							debug(datos)
							if datos['plan_id'] and datos['descripcion']:
								pass
							Webhook.registrarEvento(datos['id'], datos, tipoEvento)
						elif tipoEvento == 'subscripcion.payment_failed':
							debug('Pago Subscripcion Fallo')
							debug(datos)
						else:
							debug('Evento no manejado: '+tipoEvento)
							debug(datos)
							Webhook.registrarEvento(datos['id'], datos, tipoEvento)

						return HttpResponse('Vie')
					else:
						return HttpResponseBadRequest('No se envio informacion sobre el cargo')
				else:
					return HttpResponseBadRequest('No se recibio informacion del tipo de evento')
			except Exception as e:
				debug('Exception')
				debug(e)
				Webhook.registrarEvento(datos['id'], datos, 'error', True)
				return HttpResponseServerError("Error no contemplado")
		else:
			return HttpResponseBadRequest('No se envio la informacion necesaria')

	@csrf_exempt
	def twilio(request):
		error = True
		datos = recibirJson(request)
		debug('Twilio')
		debug(datos)
		return HttpResponse('Vie')

	def obtenerCargoOrden(datos):
		"""
			Campo para obtener el primer cargo de la orden de pago
		"""
		resp = False
		try:
			if datos['line_items'] and datos['charges']:
				if datos['line_items']['data'] and len(datos['line_items']['data']) == 1:
					resp = datos['line_items']['data'][0]

					if datos['charges']['data'] and len(datos['charges']['data']) > 0:
						cargo = datos['charges']['data'][0]
						resp['paid_at'] = cargo['paid_at']
						resp['amount'] = cargo['amount']
						resp['fee'] = cargo['fee']
						resp['status'] = cargo['status']
						resp['created_at'] = cargo['created_at']
						resp['payment_method'] = cargo['payment_method']

		except Exception as e:
			return False
		return resp

	def determinarTipoPago(metodoPago):
		if metodoPago['object']:
			if metodoPago['object'] == 'card_payment':
				return METODOPAGOS['P_DEBITO']
			elif metodoPago['object'] == 'cash_payment':
				return METODOPAGOS['P_OXXO']
			elif metodoPago['object'] == 'credit':
				return METODOPAGOS['P_CREDITO']
			elif metodoPago['object'] == 'bank_transfer_payment':
				return METODOPAGOS['P_TRANS']
			else:
				return False
		elif metodoPago['subscription_start']:
			return METODOPAGOS['P_SUBSCR']
		else:
			return False
		

