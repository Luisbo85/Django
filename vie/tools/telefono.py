from django.utils					import timezone
from datetime 						import datetime

from vie.tools.errores 					import Status as stat
from vie.views.views				import recibirJson, enviarJson, verfUsuario, debug
from django.views.decorators.csrf	import csrf_exempt
import vie.tools.validar		as val
from vie.models						import Persona as PersonaMdl, Usuario as usuarioMdl, Token, CodValidacion, Codigos as CodStatics
from vie.tools.SMS						import SMS
#from vie.tools.correo 					import Correo Sendgrid temporalmente detenido
from vie.views.viewUsuario			import Usuario
from Django.settings 				import APPS_VIE
from viePay.views.viewConekta	import PagoConekta


class Movil:

	@csrf_exempt
	def reenvioCod(datos, app):
		"""
			Metodo en el cual, el usuario reescribira
			su número de telefono para rectificar, en caso
			de que se detecte que el numero ya existe. El asignado
			anterior será cambiado.
		"""

		if 'token' in datos and 'tel' in datos:
			usuario = verfUsuario(datos['token'], app)

			if usuario:
				if val.validarTel(datos['tel']):
					pers = PersonaMdl.objects.get(idPersona = usuario.idUsuario.idFila)
					debug (usuario.idUsuario)
					if not Usuario.CodigoaGenerar(usuario.idUsuario, datos['tel'], pers.nombre, SMS.RECUPERACION):
						return enviarJson({'mensaje': 'No pudo enviarse el SMS'}, stat.UNPROCESSABLE)

					return enviarJson({'mensaje': 'Esperando su confirmacion'}, stat.OK)
				else:
					return enviarJson({'mensaje': 'El telefono no es valido', 'campo': 'tel'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, tel'}, stat.BAD_REQUEST)

	def revalidarCod(datos, apps):
		"""
			Metodo que recibe un JSON para validar un codigo enviado al usuario para autentificar
			la posesion del celular
		"""
		"""datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)"""
		if val.validarExistencia(datos, ['token', 'codigo', 'tel']):
			try:
				token = Token.objects.get(token = datos['token'], app = apps)
				try:
					codigo = CodValidacion.objects.get(codigo = datos['codigo'])
					if not codigo.idUsuario == None and token.idUsuario.idUsuario == codigo.idUsuario.idUsuario:
						if not Usuario.codigoExpirado(codigo.fechaAsignacion):

							usuarion			= codigo.idUsuario
							usuarion.permiso		= Usuario.CLIENTE
							try:
								personao = PersonaMdl.objects.get(telefono = datos['tel'])
								usuarioo = usuarioMdl.objects.get(idFila = personao.idPersona)
								personan = PersonaMdl.objects.get(idPersona = usuarion.idFila)
								personao.telefono = '-1'
								personan.telefono = datos['tel']
								usuarioo.permiso = Usuario.CAMBIONUM
								usuarioo.pin = -1
								usuarion.pin = datos['codigo']
								usuarion.clienteID = PagoConekta.crearCliente(personan.nombre, personan.apellidos, usuarion.email, personan.telefono)
								usuarion.save(update_fields = ['permiso', 'pin', 'clienteID'])
								usuarioo.save(update_fields = ['permiso', 'pin'])
								personao.save(update_fields = ['telefono'])
								personan.save(update_fields = ['telefono'])
								#	return enviarJson({'mensaje': 'No pudo cambiarse el num', 'campo': 'tel, usuario'}, stat.UNPROCESSABLE)
								#email = Correo()
								#email.bienvenida(usuarion.email, personan.nombre)
								Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.CORRECTO)
								codigo.idUsuario = None
								codigo.save(update_fields = ['idUsuario'])
								debug("Repetido")
								return enviarJson({'mensaje':'El usuario ya fue validado'}, stat.OK)
							except PersonaMdl.DoesNotExist as x:
								debug(x)
								personan = PersonaMdl.objects.get(idPersona = usuarion.idFila)
								personan.telefono = datos['tel']
								usuarion.pin = datos['codigo']
								usuarion.clienteID = PagoConekta.crearCliente(personan.nombre, personan.apellidos, usuarion.email, personan.telefono)								
								personan.save(update_fields = ['telefono'])
								usuarion.save(update_fields = ['permiso', 'pin', 'clienteID'])
								#email = Correo()
								#email.bienvenida(usuarion.email, personan.nombre)
								Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.CORRECTO)
								codigo.idUsuario = None
								codigo.save(update_fields = ['idUsuario'])
								debug ("Nuevo")
								return enviarJson({'mensaje': 'El usuario ya fue validado'}, stat.OK)
						else:
							Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.EXPIRADO)
							codigo.idUsuario = None
							codigo.save(update_fields = ['idUsuario'])
							return enviarJson({'mensaje':'El codigo ha expirado', 'campo': 'codigo'}, stat.UNPROCESSABLE)
					else:
						return enviarJson({'mensaje':'El codigo enviado no esta asignado al usuario', 'campo': 'codigo'}, stat.UNAUTHORIZED)
				except CodValidacion.DoesNotExist as c:
					debug(c)
					return enviarJson({'mensaje': 'El codigo recibido no existe', 'campo': 'codigo'}, stat.UNPROCESSABLE)
			except Token.DoesNotExist as e:
				debug(e)
				return enviarJson({'mensaje': 'No existe el usuario indicado', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan datos', 'campo': 'token, codigo, tel'}, stat.BAD_REQUEST)

	def usuariocamb (tel):
		debug(tel)
		try:
			pers1 = PersonaMdl.objects.get(telefono = tel)
			debug(pers)
			debug("ya esta chido")
		except PersonaMdl.DoesNotExist as p:
			debug (p)
		return True