""""Modulo en que se definen los codigos de errores que existen el sistema"""

class Status:
	# Estatus' de Exito (2xx). Petición del cliente recibida, entendida y aceptada
	OK				= 200	#Salio todo bien
	NO_CONTENT		= 204	#Hecho, pero no hay respuesta
	RESET_CONT		= 205	#Hecho, el cont. origen debe resetearse.
	PARCIAL_CONT	= 206	#Responde a peticiones parciales.
	# Status de código de Redirecciones (3xx)
	REDIRECTION		= 307	#Necesita ser redirigido sin cambiar de método.
	# Estatus' de Errores del CLiente (4xx).
	BAD_REQUEST		= 400	#Información no entendida por el servidor (error de sintaxis).
	UNAUTHORIZED	= 401	#El cliente requiere autentificación
	FORBIDDEN		= 403	#Se entiende, pero no se puede responder, no hay privilegios o así (NO general).
	NOT_FOUND		= 404	#Recurso o página no encontrada.
	NOT_ALLOW		= 405	#El método de la petición no es permitida.
	TIME_OUT		= 408	# El cliente tarda demasiado en realizar la petición.
	UNPROCESSABLE	= 422	#Petición correcta, error en contenido (todo bien, pero no se pueden procesar las instrucciones).
	# Status de Errores del Servidor (5xx)
	INTERNAL_SERV	= 500	#Error inesperado en el servidor.
	UNAVALIBLE		= 503 	#No está disponible en ese momento.
	INSUF_STORAGE	= 507	#No hay espacio.


	