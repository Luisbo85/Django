#-*-coding: utf-8 -*-
import re

def validarExistencia(diccionario, campos):
	"""Validar si un diccionario tiene los campos indicados.
	   En caso de tenernos todos los campos regresa un True y en caso contrario un False"""

	valido = True
	tipo = type(campos)
	if type(diccionario) == dict and ( tipo == list or tipo == tuple):
		for campo in campos:
			if not(type(campo) == str):
				valido = False
				break
			else:
				if not campo in diccionario:
					valido = False
					break					
	else:
		valido = False
	return valido

def validarEmail(email):
	"""Validar si la cadena ingresada cumple con el formato de un correo eletronico
		En caso de ser un formato valido regresa un True y en caso contrario un False"""
		
	if type(email)==str and re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$', email):
		return True
	else:
		return False

def validarRFC(rfc):
	"""
		Validar si la cadena ingresada corresponde con el formato de un RFC,
		es decir, 4 letras, 9 numeros.
	"""
	if type(rfc)==str and re.match('^[(A-Z)]{4}[0-9]{6}[A-Z0-9]{3}$', rfc):
		return True
	else:
		return False

def validarPlacas(placas):
	"""
		Validar si la cadena introducida para placas cumple con el formato
	"""
	if type(placas)==str and re.match('^[A-Z]{3}[0-9]{4}$', placas):
		return True
	else:
		return False

def validarPassword(password):
	"""Validar si la cadena ingresada cumple con el formato de una contraseña. Minimo 5 caracteres
		En caso de ser un formato valido regresa un True y en caso contrario un False

		expresion para contrasena segura
		(?=^.{4,}$)(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*\W)(?=.*[0-9]).*$
		"""
	if type(password)==str and len(password) >= 4:
		if re.match('(?=^.{4,}$)(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$', password):
			return True, 'OK'
		else:
			return False, 'Contrasena muy sencilla (al menos 1 digito, 1 mayus/minus)'
	else:
		return False, 'Contrasena muy corta (al menos 4 caracteres)'

		
def validarNombre(nombre):
	"""Validar si la cadena ingresada cumple con el formato de un nombre
		En caso de ser un formato valido regresa un True y en caso contrario un False"""
	if type(nombre)==str and re.match('^([a-zA-Z]+[ ]?)+$', nombre):
		return True
	else:
		return False
		
		
def validarTel(tel):
	"""Validar si la cadena ingresada cumple con el formato de un telefono incluyendo el código del
		pais.
		En caso de ser un formato valido regresa un True y en caso contrario un False"""
	if type(tel)==str and re.match('^(\+\d{2,4})+\d{8,10}$', tel):
		return True
	else:
		return False

def validarChip(chip):
	"""
		Valida el formato del chip
	"""
	if re.match('^([a-zA-Z0-9]){8,12}$', chip):
		return True
	else:
		return False


def validarCodigo(tarjeta):
	"""
		Validar el formato del codigo externo
	"""
	if re.match('^([A-Z0-9]){6,12}$', tarjeta):
		return True
	else:
		return False


def validarDatos(datos, diccionario):
	"""
		Valida la existencia de los datos en el diccionario recibido como primer parametro. El segundo parametro
		tiene los campos a buscar y su tipo de dato
	"""
	tipoValidos = {
		'boolean': bool,
		'bool': bool,
		'float': float,
		'int': int,
		'string': str,
		'none': type(None)
	}
	valido = True
	campoError = None
	tipoError = None 
	if type(datos) == dict and type(diccionario) == dict:
		if len(diccionario) == 0:
			raise Exception('Diccionario de validacion esta vacio')
		for campo in diccionario.keys():
			try:
				tipos = diccionario[campo]
			except KeyError:
				raise KeyError('No se envio tipo de dato con la clave '+str(campo))
			if campo in datos:
				try:
					if type(tipos) == str:
						if type(datos[campo]) == tipoValidos[tipos]:	
							pass
						else:
							valido = False
							campoError = campo
							tipoError = tipos
							break
					elif type(tipos) == list or type(tipos) == tuple:
						invalido = True
						for pos in tipos:
							try:
								if type(datos[campo]) == tipoValidos[pos]:
									invalido = False
									break
								else:
									tipoError = tipoValidos[pos]
							except TypeError:
								raise TypeError('Tipo de dato a validar '+str(pos)+' no es un string')
					else:
						raise TypeError('Tipo de dato a validar '+str(tipos)+' no es soportado')
				except KeyError:
					raise KeyError('Tipo de dato '+str(tipos)+' no soportado')
			else:
				valido = False
				campoError = campo
				break
	
		return  valido, campoError, tipoError
	else:
		raise TypeError('Parametros deben ser diccionarios')
		
		
