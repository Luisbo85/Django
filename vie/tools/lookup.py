from vie.models import Lookup as LookupMdl

from singleton3 import Singleton

class Lookup(object, metaclass=Singleton):

	def __init__(self):
		"""
			Cargar los diferentes valores al sistema
		"""
		print('Iniciando')
		self.tipo = {
			'EMPRESAS': 0,
			'SETTING': 1,
			'ACCESO': 2
		}
		self.EMPRESAS = self.get_empresas()
		self.APPS_VIE = self.get_appsVie()

	def get_empresas(self):
		return {}


	def get_appsVie(self):
		"""
			Obtener los valores para los accesos de las aplicaciones
		"""
		try:
		  apps = LookupMdl.objects.get(tipo = self.tipo['ACCESO'], nombre = 'APPS_VIE')
		  return {}
		except LookupMdl.DoesNotExist:
			return {}
