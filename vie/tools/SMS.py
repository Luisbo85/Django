import twilio
import requests
import json

from twilio.rest import Client
from vie.views.views	import debug
from Django.settings	import NO_SENT

SERVICE_ID = 'MGa8a1ec5d364e621c7c26e9b68e09c6c4'

# put your own credentials here 12018854500
ACCOUNT_SID	= "ACc27c8126200a19ccdc9876ad6fee3d27"
AUTH_TOKEN	= "403d467fa08416e71f3bc23cdde7836e"


# Cuenta de prueba 16184946326
#ACCOUNT_SID	= "AC2d607f942a69c309e1e9badf828fe0a7"
#AUTH_TOKEN	= "db02993dad9db391011bf6234f7c112c"

class SMS:

	REGISTRO = 0
	RECUPERACION = 1
	ENV_REFERENCIA = 2

	HOST_SMS_MASIVOS = 'http://smsmasivos.com.mx/sms/api.envio.new.php'
	APIKEY_SMSM = '8cff23a8385e1d06331e8651c2ffc745768fdb8b'

	""""""
	def __init__(self):
		self.client = Client(ACCOUNT_SID, AUTH_TOKEN)

	"""
		Verificar el numero que twilio proporciona para ver si es posible el envio de msj
	"""
	def EnviarCodConfirmacion(self, codigo, nombre, bandera, tel_destino = "+523315646602"):
		enviado = False
		#debug(dir(twilio.rest))
		if type(codigo) == int:
			if not NO_SENT:
				try:
					if bandera == SMS.REGISTRO:
						mensaje = '¡Hola ' + nombre + '! Bienvenido a ViePay. \nCódigo: '+str(codigo)
						self.client.messages.create(
						    to		= tel_destino,
						    messaging_service_sid	= SERVICE_ID,
						    body	= mensaje
						)
					elif bandera == SMS.RECUPERACION:
						mensaje = '¡Hola ' + nombre + '. Tu Nuevo NIP: ' + str(codigo)
						self.client.messages.create(
						    to		= tel_destino,
						    messaging_service_sid	= SERVICE_ID,
						    body	= mensaje
						)
					elif bandera == SMS.ENV_REFERENCIA:
						debug("Enviando mensaje...")
						mensaje = 'Recarga ViePay de ' + str(cantidad) + ". \nReferencia: " + str(referencia)
						self.client.messages.create(
							to 		= tel_destino,
							messaging_service_sid 	= SERVICE_ID,
							body 	= mensaje
							)
						debug("Enviado!")
					enviado = True
				except twilio.rest.TwilioException as e:
					debug(e)
			else:
				debug("desde sms: " + str(bandera))
				if bandera == SMS.REGISTRO:
					debug ('¡Hola ' + nombre + '! Bienvenido a VieTaxi. \nCódigo de confirmación: '+str(codigo))
				elif bandera == SMS.RECUPERACION:
					debug(nombre + ' Tu recuperación de contraseña.  Código: ' + str(codigo))
		else:
			enviado = False
		return enviado

	def enviarReferencia(self, cantidad, referencia, tel_destino):
		enviado = False

		if not NO_SENT:
			try:
				debug("Enviando mensaje...")
				debug(tel_destino)
				debug(referencia)
				datos = {
						'apikey': SMS.APIKEY_SMSM,
						'numcelular': tel_destino[3:],
						'numregion': '52'
					}
				mensaje = 'Recarga ViePay de $' + str(cantidad) + ".00 \nReferencia: " + str(referencia)
				self.client.messages.create(
					to 		= tel_destino,
					messaging_service_sid 	= SERVICE_ID,
					body 	= mensaje
					)
				enviado = True
				debug("Enviado!")
			except twilio.rest.TwilioException as e:
				debug(e)
		else:
			debug("Desde SMS :D")
			debug("Tel: " + tel_destino)
			debug('Recarga ViePay de ' + str(cantidad) + ". \nReferencia: " + str(referencia))
		return enviado

	def confirmacionRecarga(self, cantidad, tel_destino):
		enviado = False

		if not NO_SENT:
			try:
				debug("Enviando mensaje...")
				debug(tel_destino)
				datos = {
						'apikey': SMS.APIKEY_SMSM,
						'numcelular': tel_destino[3:],
						'numregion': '52'
					}
				mensaje = 'Recarga ViePay de $' + str(cantidad) + ".00 fue abonada."
				self.client.messages.create(
					to 		= tel_destino,
					messaging_service_sid 	= SERVICE_ID,
					body 	= mensaje
					)
				datos['mensaje'] = mensaje+' M'
				resp = requests.post(url=SMS.HOST_SMS_MASIVOS, params=datos, timeout=20)
				debug(resp)
				enviado = True
				debug("Enviado!")
			except twilio.rest.TwilioException as e:
				debug(e)
		else:
			debug("Desde SMS :D")
			debug("Tel: " + tel_destino)
			debug('Recarga ViePay de ' + str(cantidad) + ". \nReferencia: " + str(referencia)) + " fue abonada."
		return enviado

