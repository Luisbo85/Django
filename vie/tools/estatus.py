#Estatus de objectos del sistema

#Formas por las que se ingresa al sistema
APPS_VIE = {
    'TAXI_CLIENTE': 0,
    'TAXI_CHOFER': 1,
    'TRANS_CLIENTE': 2,
    'TRANS_CHOFER': 3,
    'PAY': 4,
    'ADMIN': 5,
    'TEMP': 6,
    'TRANS_ADMIN': 7,
    'TAXI_ADMIN': 8
}

#Estatus del chofer
CHOFER_ESTATUS = {
	'ACTIVO'     : 0,
	'SUSPENDIDO' : 1
}

# Identificar empresas
EMPRESAS = {
    'VIETAXI': 0,
    'VIETRANSPORTE': 1,
    'VIEPAY': 2,
    'TODO': 3
}

#Estatus de los lectores
LECTOR_ESTATUS = {
	'REGISTRADO': 0,
	'LIGADO': 1,
	'REPARACION': 2,
	'ELIMINADO': 3
}

#Estatus de un usuario
USUARIO_ESTATUS = {
	'REGISTRADO' : 0,
	'ACTIVO'     : 1,
	'SUSPENDIDO' : 2,
	'CAMBIONUM'  : 3,
	'REG_RAPIDO' : 4
}

# Estado de tarjetas
TARJETA_ESTATUS = {
	'ACTIVO': 0,
	'LIGADO': 1,
	'CANCELADO': 2
}

#Estatus de tarjeta ligada
T_LIGADA_ESTATUS = {
	'ACTIVO': 0,
	'CANCELADO': 1
}

#Estatus de un turno
TURNO_ESTATUS = {
	'SERVICIO': 0,
	'TERMINADO': 1
}

#Tipos de usuarios
USUARIO_ROLES = {
	'CLIENTE'	: 1,
	'CHOFER'	: 2,
	'ADMIN'     : 5
}

#Metodos de pago
METODOPAGOS = {
	'P_EFECTIVO': 0,
	'P_DEBITO': 1,
	'P_CREDITO': 2,
	'P_CARTERA': 3,
	'P_PAYPAL': 4,
	'P_PROMO': 5,
	'P_VIETRANS': 6,
	'P_OXXO' : 7,
	'P_TRANS': 8,
	'P_SUBSCR': 9
}

#Banderas del sistema

# Bandera para evitar envío de SMS/correo 
NO_SENT = False

# Activar rethinkdb de forma local
RETHINKDB_TEST = False