from django.apps import AppConfig

class VieConfig(AppConfig):
    name = 'vie'
    path = 'vie/'