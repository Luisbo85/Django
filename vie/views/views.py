import json
import logging
from django.views.decorators.csrf	import csrf_exempt

# Create your views here.
from django.http 					import HttpResponse, HttpResponseNotFound, JsonResponse
from django.utils					import timezone

from vie.tools.errores				import Status
from django.utils.crypto			import get_random_string
from vie.models						import Token, Chofer, Persona, EmpresaChofer
from Django.settings 				import DEBUG, EMPRESAS, APPS_VIE

logger = logging.getLogger(__name__)

@csrf_exempt
def index(request):
	"""
		Vista que muestra la pagina de inicia al sitio.
	"""
	html = """
			<html>
			<head>
			       <title>Vie</title>
			</head>
			<body>
			       <h3>Vie</h3>
			</body>
			</html>"""
	return HttpResponse(html)

def recibirJson(request, esString = False):
	"""
		Recibe un Objeto de tipo HttpRequest para captar el JSON que se envio.
		Regresa el dicionario que representa al JSON recibido. En caso de error regresa un diccionacio vacio
	"""
	#Obtener el JSON que se mando en la peticion
	jsonBytes = request.read()
	#Convierte los bytes recibidos en el JSON tipo texto correspondiente y lo vuelve un diccionacio
	try:
		if esString:
			return json.loads(jsonBytes)
		else:
			return json.loads(jsonBytes.decode())
	except:
		logger.error('Error: Formato erroneo del JSON')
		return False


def enviarJson(diccionario, estatus = Status.OK):
	"""
		Permite enviar un JSON en base al diccionacio que se pasa como parametro y el coigo de
		estatus que debe colocar como respuesta segun el protocolo HTTP
	"""
	if type(diccionario) == dict:
		resp = JsonResponse(diccionario, safe = False)
		resp.status_code = estatus
	else:
		resp = JsonResponse({'mensaje': 'Problemas!! Fallo de formato al enviar'}, safe = False)
		resp.status_code = Status.INTERNAL_SERV
	return resp

def generarToken(longitud = 10):
	"""
		Metodo dedicado a generar los tokens que los usuarios utilizaran para autentificarze
		en el sistema. Genera cadena de 10 caracteres de longitud
	"""
	return get_random_string(longitud)
	
def paginaNoEncontrada(request):
	"""
		En caso de no poder encontrar estar definida un Url se manda la siguiente respuesta y
		con el su contenido
	"""
	return HttpResponseNotFound('Pagina no encontrada')	

def verfUsuario(datos, app):
	"""
		Metodo para verificar si un usuario existe 
		Regresa el usuario (objeto).
	"""

	if not datos == None:
		try:
			token = Token.objects.get(token = datos, app = app)

			if not token.idUsuario == None:
				if app == APPS_VIE['PAY'] and not token.vence == None:
					from vie.views.viewUsuario 	import Usuario
					if timezone.now() > token.vence:
						logger.debug("El token vencio!")
						dicc = {}
						dicc['token'] = datos
						resp = Usuario.vieLogout(datos, dicc)
						return False
				return token
			else:
				return False

		except Token.DoesNotExist as e:
			logger.error(e)
			return False
	else:
		return False

def verfChofer (datos):
	"""
		Verificar si existe el chofer, mediantte su ID.
	"""
	if not datos == None:
		try:
			chofer = Chofer.objects.get(idChofer = datos)
			if not chofer.idChofer == None:
				return chofer
			else:
				return False
		except Chofer.DoesNotExist as e:
			logger.error(e)
			return False
	else:
		return False

def verfPersona (datos):
	"""
		Verificar si existe una persona, mediantte su ID.
	"""
	if not datos == None:
		try:
			persona = Persona.objects.get(idPersona = datos)
			if not persona.idPersona == None:
				return persona
			else:
				return False
		except Persona.DoesNotExist as e:
			logger.error(e)
			return False
	else:
		return False

def verfAcceso(token, permitido):
	"""
		Verificar si usuario existe y si tiene los permisos necesarios
	"""
	if not (token == None) and type(token) == str:
		try:
			datos = Token.objects.get(token = token, asignado = True)
			if not datos.idUsuario == None:
				if type(permitido) == int:
					if permitido == datos.app:
						return datos.idUsuario, False
					else:
						return False, True
				elif type(permitido) == list:
					if datos.app in permitido:
						return datos.idUsuario, False
					else:
						return False, True
		except Token.DoesNotExist as e:
			logger.error(e)
			return False, False
	return False, False

def verfEmpresaCh(datos):
	"""
		Método para verificar la existencia de una Empresa-Chofer
	"""
	if not datos == None:
		try:
			empresa = EmpresaChofer.objects.get(idEmpresa = datos)

			if not empresa.idEmpresa == None:
				debug("LA empresa es: " + str(empresa))
				return empresa
			else:
				return False
		except EmpresaChofer.DoesNotExist as e:
			logger.error(e)
			return False
	else:
		return False

def debug (mensaje):
	"""
		Función que nos permite debugear, se usa igual que print.
	"""
	if DEBUG == True:
		if not mensaje == None:
			logger.debug(mensaje)
		else:
			logger.debug("No hay un mensaje para mostrar")

def cadenaAEmpresa(cadena):
	"""
		Determinar si la cadena enviada representa una de las empresas o no. SI la represanta, regresa su valor en el sistema
	"""
	if cadena == 'VIETAXI':
		return EMPRESAS['VIETAXI']
	elif cadena == 'VIETRANS':
		return EMPRESAS['VIETRANSPORTE']
	else:
		return -1

def determinarCargo(mensaje):
	"""
		Recibe una cadena proveniente de un cargo. El estadar de la informacion enviada es el pais, empresa del que procede el cargo, el concepto
		y el id del cargo realizado
		En caso de exito regresa un diccionario con los respectivos campos o un False en caso de error.
	"""
	if type(mensaje) == str:
		lista = mensaje.split(':')
		if len(lista) == 2:
			campos = {
				'id':lista[1]
			}
			lista = lista[0].split()
			if len(lista) == 3: 
				campos['pais'] = lista[0]
				campos['empresa'] = cadenaAEmpresa(lista[1])
				campos['concepto'] = lista[2]
				return campos
			else:
				return False 
		else:
			return False
	else:
		return False

def leerDatos(request, metodoGet = False):
	"""
		Funcion para leer los datos enviados mediante la peticion HTTP
	"""
	error = None
	respuesta = ''
	try:
		if request.method == 'GET':
			respuesta = request.GET.dict()
		else:
			datos = recibirJson(request)
			if datos:
				respuesta = datos
			else:
				error = Status.BAD_REQUEST
				respuesta = {
					'mensaje': 'Formato erroneo del JSON',
					'campo': 'json'
				}
	except Exception as e:
		logger.error(e)
		error = Status.BAD_REQUEST
		respuesta = {
			'mensaje': 'Formato erroneo del JSON',
			'campo': 'json',
			'error': str(e)
		}
	return error, respuesta
