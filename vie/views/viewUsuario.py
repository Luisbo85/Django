import random as rand
import hashlib
import logging
from datetime import datetime, timedelta
import rethinkdb as r
import decimal

from django.shortcuts				import render
from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from django.utils.crypto 			import get_random_string
from django.http 					import HttpResponse, QueryDict

import vie.tools.validar			as val
from vie.tools.estatus              import CHOFER_ESTATUS, EMPRESAS, APPS_VIE
from vie.tools.SMS				    import SMS
from vie.tools.errores			    import Status as stat
from vie.views.views		        import recibirJson, enviarJson, generarToken, verfUsuario, verfPersona, verfChofer
from vie.models				        import CodValidacion, Usuario as UsuarioMdl, Token, Persona, Chofer, \
Comentarios, Codigos as CodStatics  #FotoUsuario
from viePay.models			        import MetodoPago as MetodoPagoMdl, Pago
from vieTaxi.views.viewAuto			import Automovil
from viePay.views.viewConekta	    import PagoConekta
#from vie.tools.forms 				import FormFoto

logger = logging.getLogger(__name__)

class Usuario:
	"""
		Vista que controla las opciones con los usuarios del sistema.
	"""
	#Estado de uso de codigos
	CORRECTO 	= 0
	EXPIRADO 	= 1
	#Permisos/estados del usuario
	REGISTRADO 	= 0
	CLIENTE		= 1
	CHOFER		= 2
	CAMBIONUM 	= 3
	REG_RAPIDO	= 4
	ADMIN       = 5
	#Formas de Login
	L_EMAIL		= 0		#Mediante un correo y contraseña
	L_FACE		= 1		#Mediante una cuenta de Facebook
	L_GOOGLE	= 2		#Mediante una cuenta de Google

	#Numero de sesiones maximas por usuario
	MAX_SESIONES = 10

	# Parte de la URL donde se ubica la imagen
	URL_IMAGEN = "http://test.viemx.com/media/"

	def asignarToken(usuario, app):
		"""
			A un usuario permite asignarle un token para poder acceder al sistema.
		"""
		if type(usuario) == UsuarioMdl and not(usuario.idUsuario == None):
			intento = 0
			exito = False
			while not exito and intento < 5: #Permite buscar 5 veces un token disponible
				token = generarToken()
				try:
					objetoToken = Token.objects.get(token = token)
					if objetoToken.asignado == True:
						intento += 1
					else: #Si esta disponible el token para poderlo asignar al usuario
						objetoToken.asignado	= True
						objetoToken.idUsuario 	= usuario
						objetoToken.fecha		= timezone.now()
						objetoToken.app         = app
						objetoToken.save(update_fields = ['asignado','idUsuario','fecha', 'app'])
						exito = True
					
				except Token.DoesNotExist: #No existe el token en la base de datos por lo que se debe de crear el campo
					crearToken	= Token(
						token		= token,
						asignado	= True,
						idUsuario	= usuario,
						app         = app
					)
					crearToken.save()
					exito = True
				
			if not exito: #Buscar tokens no asignados
				objetoToken = Token.objects.filter(asignado = False)
				if len(objetoToken) > 0: # Designar un token dentro de los que estan disponibles
					#Elige siempre el primero disponible. Se puede mejorar para evitar la concurrencia
					objetoToken = objetoToken[0]
					objetoToken.asignado	= True
					objetoToken.idUsuario 	= usuario
					objetoToken.fecha		= timezone.now()
					objetoToken.app = app 
					objetoToken.save(update_fields = ['asignado','idUsuario','fecha', 'app'])
					token = objetoToken.token
				else: # No se encontro un token disponible
					token = False
		else:
			token = False
		return token

	def verfToken(token):

		tok = verfUsuario(token)

		if tok.vence < timezone.now():
			return False

		else:
			return True

	def generarCodValidacion():
		"""
			Metodo que se dedica a generar los numeros de validación que se enviaran por mensaje.
			Estos son generados de forma aletoria del 0 al 100,000-
		"""
		return rand.randint(0, 100000) #Retorna un numero entre 0 y 100,000
		
	
	def registroForm(request):	
		"""
			Se encarga de generar un token para el formulario solicitado para evitar el CSRF
		"""
		return render(request, 'vie/usuarioRegistro.html')

	def vieLogin(datos, app):
		"""
			Valida que el usuario este registrado para permitirle el acceso al sistema mediante la
			asignaci´on de un Token
		"""
		try:
			if 'modo' in datos:
				try:
					modo = int(datos['modo'])
				except ValueError as e:
					return enviarJson({'mensaje': 'Formato no reconocido', 'campo': 'modo'}, stat.UNPROCESSABLE)
				# bandera para dar hasta 10 tokens a cliente
				exito = False
				# bandera para dar 1 token a chofer
				exito2 = False
				if modo == Usuario.L_EMAIL:
					if val.validarExistencia(datos, ['email', 'password']):
						try:
							usuario = UsuarioMdl.objects.get(email = datos['email'], modoRegistro = modo)
							if usuario.permiso == Usuario.REGISTRADO:
								return enviarJson({'mensaje':'No ha terminado su registro', 'campo': 'email'}, stat.UNAUTHORIZED)
							else:
								# Encriptacion
								pw = hashlib.sha256()
								pw.update(datos['password'].encode('utf-8'))
								pws = str(pw.hexdigest())
								logger.debug(pws)
								if usuario.pw == pws:
									if usuario.permiso == Usuario.CLIENTE:
										if app in (APPS_VIE['TAXI_CLIENTE'], APPS_VIE['TRANS_CLIENTE']):
											logger.debug ("Es un cliente")
										else:
											return enviarJson({'mensaje': 'No puede iniciar sesion con los permisos actuales', 'campo': 'app'}, stat.UNAUTHORIZED)
									elif usuario.permiso == Usuario.CHOFER:

										chofer  = verfChofer(usuario.idFila)
										#Chofer esta suspendido o eliminado
										if not chofer or chofer.estatus == CHOFER_ESTATUS['SUSPENDIDO']:
											return enviarJson({'mensaje': 'Chofer suspendido', 'campo': 'suspendido'}, stat.UNAUTHORIZED)
										else:
											if app in (APPS_VIE['TAXI_CHOFER'], APPS_VIE['TRANS_CHOFER']):
												logger.debug ("Es un chofer")
												exito2 = True
											elif app in (APPS_VIE['TAXI_CLIENTE'], APPS_VIE['TRANS_CLIENTE']):
												logger.debug('Chofer como cliente')
											else:
												return enviarJson({'mensaje': 'No puede iniciar sesion con los permisos actuales', 'campo': 'app'}, stat.UNAUTHORIZED)
									else:
										if app in (APPS_VIE['TRANS_ADMIN'], APPS_VIE['TAXI_ADMIN']):
											logger.debug('Es un administrativo')
										elif app in (APPS_VIE['TAXI_CLIENTE'], APPS_VIE['TRANS_CLIENTE']):
											logger.debug('Administrativo como cliente')
										else:
											return enviarJson({'mensaje': 'No puede iniciar sesion con los permisos actuales', 'campo': 'app'}, stat.UNAUTHORIZED)
									exito = True
								else:
									return enviarJson({'mensaje': 'Contraseña invalida', 'campo': 'password'}, stat.FORBIDDEN)
						except UsuarioMdl.DoesNotExist as u:
							logger.error(u)
							return enviarJson({'mensaje': 'No existe el correo indicado', 'campo': 'email'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Faltan campos', 'campo': 'email, password'}, stat.BAD_REQUEST)
						
				elif modo == Usuario.L_FACE:
					if 'facebook' in datos:
						try:
							usuario = UsuarioMdl.objects.get(facebook1 = datos['facebook'], modoRegistro = modo)
							if usuario.permiso == Usuario.REGISTRADO:
								return enviarJson({'mensaje':'No ha terminado su registro', 'campo': 'facebook'}, stat.UNAUTHORIZED)
							else:
								exito = True
						except UsuarioMdl.DoesNotExist as u:
							logger.error(u)
							return enviarJson({'mensaje': 'No existe la cuenta de Facebook indicada', 'campo': 'facebook'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Faltan campos', 'campo': 'facebook'}, stat.BAD_REQUEST)
				elif modo == Usuario.L_GOOGLE:
					if 'google' in datos:
						try:
							usuario = UsuarioMdl.objects.get(google1 = datos['google'], modoRegistro = modo)
							if usuario.permiso == Usuario.REGISTRADO:
								return enviarJson({'mensaje':'No ha terminado su registro', 'campo': 'google'}, stat.UNAUTHORIZED)
							else:
								exito = True
						except UsuarioMdl.DoesNotExist as u:
							logger.error(u)
							return enviarJson({'mensaje': 'No existe la cuenta de Google indicada', 'campo': 'google'}, stat.NOT_FOUND)
					else:
						return enviarJson({'mensaje': 'Faltan campos', 'campo': 'google'}, stat.BAD_REQUEST)
				else:
					return enviarJson({'mensaje': 'No se pudo reconocer el modo de acceso', 'campo': 'modo'}, stat.BAD_REQUEST)
				if exito:
					#Verifica cuantos token tiene asignado el usuario
					lista = Token.objects.filter(idUsuario = usuario, app = app)
					if len(lista) < Usuario.MAX_SESIONES:
						intentos = 0
						#Intentar conseguir un token para el usuario actual
						if exito2 and len(lista) == 1:
							logger.debug (lista[0].asignado)
							lista[0].asignado	= False
							lista[0].idUsuario	= None
							lista[0].save(update_fields = ['asignado','idUsuario'])
						while intentos < 3:
							token = Usuario.asignarToken(usuario, app)
							if token == False:
								intentos += 1
							else:
								break
						if token == False:
							return enviarJson({'mensaje': 'Intentar más tarde', 'campo': 'ninguno'}, stat.TIME_OUT)
						else:
							return enviarJson({'token': token,'mensaje': 'Acceso exitoso'}, stat.OK)
					else:
						return enviarJson({'mensaje':'Tiene muchas sesiones abiertas, por favor verifique o cierre todas ellas', 'campo': 'ninguno'}, stat.FORBIDDEN)
			else:
				return enviarJson({'mensaje': 'No se especifico el modo de acceso', 'campo': 'modo'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)
		
	def vieLogout(request, datos = {}):
		"""
			Retira el acceso al usuario debido a su petición. Libera el token que tiene asignado
		"""
		try:
			logger.debug(datos)
			if len(datos) == 0:
				datos = recibirJson(request)
			
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)
			if 'token' in datos:
				try:
					token = Token.objects.get(token = datos['token'])
					token.asignado	= False
					token.idUsuario	= None
					token.app       = -1
					token.save(update_fields = ['asignado','idUsuario', 'app'])
					return enviarJson({'mensaje': 'Sesion cerrada'}, stat.OK)
				except Token.DoesNotExist as t:
					logger.error(t)
					return enviarJson({'mensaje': 'Token no valido', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje':'No se recibio el token', 'campo': 'token'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	@csrf_exempt
	def cerrarSesiones(request):
		"""
			Método para permitir al usuario cerrar todas sus sesiones
		"""
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
		if 'token' in datos:
			try:
				token = Token.objects.get(token = datos['token'])
				if token.asignado:
					lista = Token.objects.filter(idUsuario = token.idUsuario.idUsuario)
					logger.debug(lista)
					for item in lista:
						logger.debug (item)
						item.asignado	= False
						item.idUsuario	= None
						item.app        = -1
						item.save(update_fields = ['asignado','idUsuario', 'app'])

					return enviarJson({'mensaje': 'Sesiones cerradas... Incluyendo esta'}, stat.OK)
				else:
					return enviarJson({'mensaje':'El token enviado no tiene sesion activa', 'campo': 'token'}, stat.UNAUTHORIZED)
			except Token.DoesNotExist as e:
				logger.error(e)
				return enviarJson({'mensaje':'No existe el token enviado', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'No se recibio el token', 'campo': 'token'}, stat.UNPROCESSABLE)

	def vieRestUsuario(datos, persona, perm, app, clienteID = None, auto = False):
		"""
			Este método debería ser accedido mediante una llamada,
			y recibir el codigo de persona correspondiente así como permiso.
			-Recibe también el permiso como parámetro
			-El modo de registro sigue siendo el mismo, sea correo, face o google,
			 en caso de ser cliente.
			-Para el caso de chofer sólo será validado con el email.
			-El parámetro opcional (auto) es cambiado cuando se registra un chofer y 
			se le asigna un auto.
		"""
		guardado = False
		try:
			if 'modo' in datos:
				try:
					modo = int(datos['modo'])
				except ValueError as e:
					return enviarJson({'mensaje': 'Formato no reconocido para el modo de acceso', 'campo': 'modo'}, stat.BAD_REQUEST)
				if modo == Usuario.L_EMAIL:
					error, campo, tipo = val.validarDatos(datos, {'email': 'string', 'password': 'string'})
					if error:
						if val.validarEmail(datos['email']):
							contra = val.validarPassword(datos['password'])
							logger.debug(contra[1])
							if contra[0]:
								correos = UsuarioMdl.objects.filter(email = datos['email'])
								if len(correos) == 0:
									from viePay.views.viewMetodoPago	import MetodoPago as mPago
									
									mensaje = {}
									msj = False
									#logger.debug ("datos...."+str(perm))
									#logger.debug (datos)
									persona.save()
									guardado = True
									if perm == Usuario.CLIENTE:
										#logger.debug ("es cliente")
										msj = True
										datos['idFila'] = persona.idPersona
										perm = Usuario.REGISTRADO
									elif perm == Usuario.CHOFER:
										datos['idFila'] = persona.idChofer
										
									#logger.debug(str(datos['idFila']))
									# Encriptacion
									pw = hashlib.sha256()
									pw.update(datos['password'].encode('utf-8'))
									pws = str(pw.hexdigest())
									usuario = UsuarioMdl(
										permiso = perm,
										idFila = datos['idFila'],
										email = datos['email'],
										pw = pws,
										modoRegistro = datos['modo']
									)
									logger.debug("Entraré a conekta")
									exito = PagoConekta.crearCliente(persona.nombre, persona.apellidos, usuario.email, persona.telefono)
									if exito:
										usuario.clienteID 
										usuario.save()
									else: 
										persona.delete()

									if auto:
										datos['chofer'] = persona.idChofer
										return Automovil.vieAsignacion(datos, datos)
									if msj:
										#logger.debug("entre a cliente")
										token = Usuario.asignarToken(usuario, app)
										while token == False: #Encontrar un token para el usuario
											token = Usuario.asignarToken(usuario, app)						
										#logger.debug('ID Usuario: '+str(usuario.idUsuario))
										#logger.debug('Token: '+str(token))
										mensaje['token'] = token
										logger.debug ("Token: " + token)
										if persona.telefono == '0':
											usuario.permiso = Usuario.CAMBIONUM
											usuario.save(update_fields = ['permiso'])
											return enviarJson({'mensaje': 'El telefono ya existe, rectifique...', 'campo': 'tel', 'token': token}, stat.REDIRECTION)
										if Usuario.CodigoaGenerar(usuario, persona.telefono, persona.nombre, SMS.REGISTRO):
											mensaje['sms'] = True
										else:
											mensaje['sms'] = False

									if perm == Usuario.CHOFER:
										if 'token_tarj' in datos:
											mPago.agregarPagoSubsc(usuario, datos['token_tarj'], datos['tipo'], datos['cod'])
									else:
										mPago.pag_predeterminado(usuario)
									mensaje['mensaje'] = 'Registrado con exito'
									return enviarJson(mensaje, stat.OK)
								else:
									return enviarJson({'mensaje': 'Correo repetido', 'campo': 'email'}, stat.FORBIDDEN)
							else:
								return enviarJson({'mensaje': contra[1], 'campo': 'password'}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'Correo con formato invalido', 'campo': 'email'}, stat.FORBIDDEN)
					else:
						respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
						if tipo:
							respuesta['tipo'] = tipo
						return enviarJson(respuesta, stat.BAD_REQUEST)
				elif modo == Usuario.L_FACE:
					logger.debug (datos)
					if perm == Usuario.CLIENTE:
						if val.validarExistencia(datos,['facebook']):
							mensaje = {}
							tokfa = UsuarioMdl.objects.filter(facebook1 = datos['facebook'])
							if not len(tokfa) == 0:
								return enviarJson({'mensaje': 'La cuenta ya se existe', 'campo': 'facebook'}, stat.UNPROCESSABLE)
							else:
								logger.debug(persona.telefono)
								persona.save()
								guardado = True
								datos['idFila'] = persona.idPersona
								perm = Usuario.REGISTRADO
								usuario = UsuarioMdl(
									permiso = perm,
									facebook1	= datos['facebook'],
									idFila = datos['idFila'],
									modoRegistro = datos['modo']
								)
								usuario.save()
								token = Usuario.asignarToken(usuario, app)
								while token == False: #Encontrar un token para el usuario
									token = Usuario.asignarToken(usuario, app)						
								#logger.debug('ID Usuario: '+str(usuario.idUsuario))
								#logger.debug('Token: '+str(token))
								mensaje['token'] = token
								logger.debug ("Token: " + token)
								nombre = "\b"
								if persona.telefono == '0':
									usuario.permiso = Usuario.CAMBIONUM
									usuario.save(update_fields = ['permiso'])
									return enviarJson({'mensaje': 'El telefono ya existe, rectifique...', 'campo': 'tel', 'token': token}, stat.REDIRECTION)
								if Usuario.CodigoaGenerar(usuario, persona.telefono, nombre, SMS.REGISTRO):
									mensaje['sms'] = True
								else:
									mensaje['sms'] = False
								mensaje['mensaje'] = 'Registrado con exito'
								return enviarJson(mensaje, stat.OK)
						else:
							return enviarJson({'mensaje': 'Faltan campos', 'campo': 'facebook, persona'}, stat.BAD_REQUEST)
					else:
						return enviarJson({'mensaje': 'No es cliente, no se puede acceder', 'campo': 'permiso'}, stat.UNAUTHORIZED)

				elif modo == Usuario.L_GOOGLE:
					logger.debug (datos)
					if perm == Usuario.CLIENTE:
						if val.validarExistencia(datos,['google']):
							mensaje = {}
							tokfa = UsuarioMdl.objects.filter(google1 = datos['google'])
							if not len(tokfa) == 0:
								return enviarJson({'mensaje': 'La cuenta ya se existe', 'campo': 'google'}, stat.UNPROCESSABLE)
							else:
								logger.debug(persona.telefono)
								persona.save()
								guardado = True
								datos['idFila'] = persona.idPersona
								perm = Usuario.REGISTRADO
								usuario = UsuarioMdl(
									permiso = perm,
									google1	= datos['google'],
									idFila = datos['idFila'],
									modoRegistro = datos['modo']
								)
								usuario.save()
								token = Usuario.asignarToken(usuario, app)
								while token == False: #Encontrar un token para el usuario
									token = Usuario.asignarToken(usuario, app)						
								#logger.debug('ID Usuario: '+str(usuario.idUsuario))
								#logger.debug('Token: '+str(token))
								mensaje['token'] = token
								logger.debug ("Token: " + token)
								nombre = "\b"
								if persona.telefono == '0':
									usuario.permiso = Usuario.CAMBIONUM
									usuario.save(update_fields = ['permiso'])
									return enviarJson({'mensaje': 'El telefono ya existe, rectifique...', 'campo': 'tel', 'token': token}, stat.REDIRECTION)
								if Usuario.CodigoaGenerar(usuario, persona.telefono, nombre, SMS.REGISTRO):
									mensaje['sms'] = True
								else:
									mensaje['sms'] = False
								mensaje['mensaje'] = 'Registrado con exito'
								return enviarJson(mensaje, stat.OK)
						else:
							return enviarJson({'mensaje': 'Faltan campos', 'campo': 'google, persona'}, stat.BAD_REQUEST)
					else:
						return enviarJson({'mensaje': 'No es cliente, no se puede acceder', 'campo': 'permiso'}, stat.UNAUTHORIZED)
				else:
					return enviarJson({'mensaje': 'El metodo de registro no es reconocido', 'campo': 'modo'}, stat.FORBIDDEN)
			else:
				return enviarJson({'mensaje': 'No se especifico el metodo de registro', 'campo': 'modo'}, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			if guardado:
				persona.delete()
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	def vieSubirFoto (request):
		if request.method == 'POST':
			token = request.POST.get('token', None)
			logger.debug(str(request.POST.get))
			logger.debug(token)
			if not token == None:
				usuario = verfUsuario(token)
				if usuario:
					perm = usuario.idUsuario.permiso
					if perm == Usuario.CLIENTE or perm == Usuario.CHOFER:
						personita = usuario.idUsuario.idUsuario
						logger.debug(personita)
						dic = {'idUsuario': personita}
						qdic = QueryDict('', mutable=True)
						qdic.update(dic)
						logger.debug('-->' + str(qdic))
						form = FormFoto(qdic, request.FILES)
						if form.is_valid():
							try:
								ava_del = FotoUsuario.objects.get(idUsuario = personita)
								ava_del.delete()
								logger.debug("Foto actualizada")
								
							except FotoUsuario.DoesNotExist:
								logger.error("todo verde carnal! (Y)")
							except:
								return enviarJson({'mensaje': 'Error al guardar'}, stat.UNPROCESSABLE)
							form.save()
							img_prof = FotoUsuario.objects.get(idUsuario = personita)
							pers = UsuarioMdl.objects.get(idUsuario = personita)
							pers.foto = img_prof.Foto
							pers.save(update_fields = ['foto'])
							return enviarJson({'mensaje': 'Imagen actualizada'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'Error en el formato', 'campo': 'Form'}, stat.BAD_REQUEST)
					else:
						return enviarJson({'mensaje': "No es usuario activo.", 'campo': 'token'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.UNPROCESSABLE)
		else:
			form = FormFoto()
		return render(request, 'Foto.html')

	def vieFotobitmap (datos, permiso, app):

		if val.validarExistencia(datos, ['token', 'foto']):
			token = verfUsuario(datos['token'], app)
			
			if token:
				if not token.idUsuario.permiso == permiso:
					return enviarJson({'mensaje': 'El permiso no es el correcto', 'campo': 'token'}, stat.FORBIDDEN)
				try:
					user = UsuarioMdl.objects.get(idUsuario = token.idUsuario.idUsuario)
				except:
					return enviarJson({'mensaje': 'No hace referencia a un usuario', 'campo': 'token'}, stat.UNAUTHORIZED)
				user.foto = datos['foto']
				user.save(update_fields = ['foto'])
				return enviarJson({'mensaje': 'Foto actualizada'},stat.OK)
			else:
				return enviarJson({'mensaje': 'No existe el usuario', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token, foto'}, stat.BAD_REQUEST)

	@csrf_exempt
	def validarCodPeticion(request, app):
		"""
			Manejar los cuatro tipos de validación de código.
			1. Registro
			2. Número repetido en registro
			3. Recuperación de contraseña
			4. Recuperación de NIP (código)
		"""

		datos = recibirJson(request)

		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		else:
			from vie.tools.telefono		import Movil

			if 'token' in datos:
				tok = verfUsuario(datos['token'], APPS_VIE['TAXI_CLIENTE'])

				if tok:
					perm = tok.idUsuario.permiso
					logger.debug('permiso -> ' + str(perm))

					if perm == Usuario.REGISTRADO:
						return Usuario.vieValidarCod(datos, app)

					elif perm == Usuario.CAMBIONUM:
						return Movil.revalidarCod(datos, app)

					else:
						return enviarJson({'mensaje': 'Validación no reconocida', 'campo': 'permiso'}, stat.FORBIDDEN)
				else:
					return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'No se detectó token', 'campo': 'token'}, stat.BAD_REQUEST)

	@csrf_exempt
	def vieValidarCod(request, apps):
		"""
			Metodo que recibe un JSON para validar un codigo enviado al usuario para autentificar
			la posecion del celular
		"""
		try:
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON', 'campo': 'json'}, stat.BAD_REQUEST)

			error, campo, tipo = val.validarDatos(datos, {'token':'string', 'codigo': ('string', 'int')})
			if error:
				try:
					token = Token.objects.get(token = datos['token'], app = apps)
					try:
						codigo = CodValidacion.objects.get(codigo = datos['codigo'])
						if not codigo.idUsuario == None and token.idUsuario.idUsuario == codigo.idUsuario.idUsuario:
							expiro = Usuario.codigoExpirado(codigo.fechaAsignacion)
							logger.debug(expiro)
							if not expiro:
								Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.CORRECTO)
								usuario				= codigo.idUsuario
								usuario.permiso		= Usuario.CLIENTE
								usuario.pin 		= codigo.codigo
								usuario.save(update_fields = ['permiso', 'pin'])
								codigo.idUsuario	= None
								codigo.save(update_fields = ['idUsuario'])
								persona = Persona.objects.get(idPersona = usuario.idFila)
								#email = Correo()
								#email.bienvenida(datos['email'], persona.nombre)
								return enviarJson({'mensaje':'El usuario ya fue validado'}, stat.OK)
							else:
								Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.EXPIRADO)
								codigo.idUsuario = None
								codigo.save(update_fields = ['idUsuario'])
								return enviarJson({'mensaje': 'El codigo ha expirado'}, stat.REDIRECTION)
						else:
							return enviarJson({'mensaje':'El codigo enviado no esta asignado al usuario', 'campo': 'codigo'}, stat.NOT_FOUND)
					except CodValidacion.DoesNotExist as c:
						logger.error(c)
						return enviarJson({'mensaje': 'El codigo recibido no existe', 'campo': 'codigo'}, stat.NOT_FOUND)
				except Token.DoesNotExist as e:
					logger.error(e)
					return enviarJson({'mensaje': 'No existe el usuario indicado', 'campo': 'token'}, stat.NOT_FOUND)
			else:
				respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
				if tipo:
					respuesta['tipo'] = tipo
				return enviarJson(respuesta, stat.BAD_REQUEST)
		except Exception as e:
			logger.error(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def codigoExpirado (fecha):
		""" 
			Método para verificar si un codigo ha excedio su tiempo de vida.
		"""
		logger.debug("Antes de: " + str(fecha))
		fecha = fecha.replace(tzinfo = None)
		logger.debug("fecha normal-> " + str(fecha))
		fechamas = fecha+timedelta(minutes = 7)
		ahora = timezone.now()
		ahora = ahora.replace(tzinfo = None)
		diff = ahora - fecha
		logger.debug ("diferencia-> " + str(diff))
		logger.debug("Fecha que expira-> " + str(fechamas))
		logger.debug("Fecha ahora-> " + str(ahora))
		if diff >= timedelta(minutes = 20):
			return True
		else:
			return False

	"""
		REVISION: Analizar eliminacion
	"""
	@csrf_exempt
	def vieMetodoPago(request):
		""""
				METODO PARA EL CONTROL DE LOS POSIBLES METODOS DE PAGO QUE SE 
				EFECTUARAN EN EL SISTEMA, HASTA AHORA			"""
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if ('token' in datos) and ('md_pago' in datos):
			try:
				usuario = verfUsuario(datos['token']) 
				if usuario:
					md_pago = datos['md_pago']
					if md_pago == Usuario.P_EFECTIVO:
						pago = MetooPago(idUsuario = usuario, formaPago = datos['md_pago'])
						pago.save()
						return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
					
					elif md_pago == Usuario.P_CREDITO:
						if 'notarjeta' in datos and 'norev' in datos:
							if len(datos['notarjeta']) == 16 and len(datos['norev']) == 4:
								pago = MetodoPagoMdl(idUsuario = usuario, formaPago = datos['md_pago'],
								codigo = datos['notarjeta'], codigorev = datos['norev'])
								pago.save()
								return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Revise los codigos de tarjeta', 'campo': 'notarjeta, norev'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'Faltan datos de la tarjeta', 'campo': 'notarjeta, norev'}, stat.BAD_REQUEST)

					elif md_pago == Usuario.P_PAYPAL:
						if 'paypal' in datos:
							pago = Pago(idUsuario = usuario, formaPago = datos['md_pago'])
							pago.save()
							return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'Falta el paypal', 'campo': 'paypal'}, stat.BAD_REQUEST)
					
					elif md_pago == Usuario.P_PROMO:
						if 'promo' in datos:
							if len(datos['promo']) == 6:
								pago = MetodoPagoMdl(idUsuario = usuario, formaPago = datos['md_pago'])
								pago.save()
								return enviarJson({'mensaje': 'Pago efectuado'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'Verifique el codigo de la promo', 'campo': 'promo'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'Falta la promo', 'campo': 'promo'}, stat.BAD_REQUEST)
					else:
						return enviarJson({'mensaje': 'No se reconoce el metodo de pago', 'campo': 'md_pago'}, stat.UNPROCESSABLE)
				else:
					return enviarJson({'mensaje': 'El usuario no es valido', 'campo': 'token'}, stat.UNAUTHORIZED)
			except Token.DoesNotExist as e:
				logger.error(e)
				return enviarJson({'mensaje': 'No existe el usuario indicado', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan datos', 'campo': 'token, md_pago'}, stat.BAD_REQUEST)

	def esValido(request, empresa):
		#Método para saber si un usuario
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if 'token' in datos:
			user = verfUsuario(datos['token'], empresa)
			if user:
				if user.idUsuario.permiso == Usuario.REGISTRADO:
					return enviarJson({'estado': 'Registrado', 'mensaje': 'Falta validar.'}, stat.OK)
				elif user.idUsuario.permiso == Usuario.CLIENTE:
					return enviarJson({'estado': 'Cliente', 'mensaje': 'Valido'}, stat.OK)
				elif user.idUsuario.permiso == Usuario.CHOFER:
					return enviarJson({'estado': 'Chofer', 'mensaje': 'Valido'}, stat.OK)
				elif user.idUsuario.permiso == Usuario.CAMBIONUM:
					return enviarJson({'estado': 'Cambio de numero', 'mensaje': 'No valido'}, stat.OK)
			else:
				return enviarJson({'mensaje': 'Token no valido/sesión no valida'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Falta el token'}, stat.BAD_REQUEST)

	def CodigoaGenerar (usuario, telf, nombre, tipo):
		"""
			Permite generar un codigo que sere usado validar el celular del usuario. Es enviado mediante un mensaje de texto.
		"""
		codigoValidacion = Usuario.generarCodValidacion()
		logger.debug('Codigo generado: '+str(codigoValidacion))
		#Crear un registro en la bases de datos o asignar uno
		hoy = timezone.now()
		logger.debug("Guardare-> " + str(hoy))
		codigo = CodValidacion(
			codigo			= codigoValidacion,
			idUsuario 		= usuario,
			fechaAsignacion = hoy
		)
		codigo.save()

		sms = SMS()
		logger.debug("aqui el tipo-> " +  str(tipo))
		if sms.EnviarCodConfirmacion(codigoValidacion, nombre, tipo, telf):
			return True
		
		return False

	def staticCod(codigo, usuario, fecha, status):
		codigostat = CodStatics(
			codigo = codigo,
			idUsuario = usuario,
			fechaAsignacion = fecha,
			fechaUso = timezone.now(),
			estado = status
		)
		codigostat.save()

	@csrf_exempt
	def enviarCodigo(request, app):

		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if 'token' in datos:
			tok = verfUsuario(datos['token'], APPS_VIE['TAXI_CLIENTE'])

			if not tok:
				return enviarJson({'mensaje': 'Token valido ', 'campo': 'token'}, stat.UNPROCESSABLE)

			logger.debug("Permiso envcod ->" + str(tok.idUsuario.permiso))
			if tok.idUsuario.permiso == Usuario.CAMBIONUM:
				from vie.tools.telefono              import Movil

				return Movil.reenvioCod(datos, app)

		if 'tel' in datos:
			try:
				exist = Persona.objects.get(telefono = datos['tel'])

				return Usuario.recuperar(datos, app)
			except Persona.DoesNotExist:
				return enviarJson({'mensaje': 'No se encuentra telefono', 'campo': 'tel'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'No se detectó teléfono', 'campo': 'tel'}, stat.BAD_REQUEST)
	
	def recuperar(datos, apps):
		"""datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)"""

		if 'tel' in datos:
			try:
				pers = Persona.objects.get(telefono = datos['tel'])
				user = UsuarioMdl.objects.get(idFila = pers.idPersona, permiso = Usuario.CLIENTE)
				logger.debug("ID de persona ->" + str(pers.idPersona))
				logger.debug("El nombre es: " + pers.nombre)
			except Persona.DoesNotExist:
				return enviarJson({'mensaje': 'No se encontro el telefono', 'campo': 'tel'}, stat.UNPROCESSABLE)
			except UsuarioMdl.DoesNotExist:
		 		return enviarJson({'mensaje': 'No se encuentra el usuario', 'campo': 'tel'},stat.UNPROCESSABLE)


			lista = Token.objects.filter(idUsuario = user, app = apps)
			if len(lista) < Usuario.MAX_SESIONES:
 				intentos = 0
 				#Intentar conseguir un token para el usuario actual
 				if len(lista) == 1:
 					logger.debug (lista[0].asignado)
 					lista[0].asignado	= False
 					lista[0].idUsuario	= None
 					lista[0].save(update_fields = ['asignado','idUsuario'])
 				while intentos < 3:
 					token = Usuario.asignarToken(user, APPS_VIE['TEMP'])
 					if token == False:
 						intentos += 1
 					else:
 						break
 				if token == False:
 					return enviarJson({'mensaje': 'Intentar más tarde', 'campo': 'ninguno'}, stat.FORBIDDEN)

			codigoValidacion = Usuario.generarCodValidacion()
			logger.debug('Codigo generado: '+str(codigoValidacion))
			logger.debug('Token genearado: ' + str(token))
			#Crear un registro en la bases de datos o asignar uno
			hoy = timezone.now()
			logger.debug("Guardare-> " + str(hoy))
			codigo = CodValidacion(
				codigo			= codigoValidacion,
				idUsuario 		= user,
				fechaAsignacion = hoy
			)
			codigo.save()

			sms = SMS()
			
			if sms.EnviarCodConfirmacion(codigoValidacion, pers.nombre, SMS.RECUPERACION, datos['tel']):
				return enviarJson({'mensaje': 'Hecho', 'token': token}, stat.OK)

			else:
				return enviarJson({'mensaje': 'Ocurrio un problema...(SMS)'}, stat.UNPROCESSABLE)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'tel'}, stat.BAD_REQUEST)
			
		return False


	def restablecer(request, empresa):
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		error, campo, tipo = val.validarDatos(datos, {'token':'string', 'codigo': ('string', 'int'), 'password': 'string', 'rpassword': 'string'})
		if error:
			tok = verfUsuario(datos['token'], APPS_VIE['TEMP'])

			if tok:
				try:

					codigo = CodValidacion.objects.get(codigo = int(datos['codigo']))
					expiro = Usuario.codigoExpirado(codigo.fechaAsignacion)
					logger.debug(expiro)
					if not expiro:
						Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.CORRECTO)
						contra = val.validarPassword(datos['password'])

						if contra[0]:
							try:
								if codigo.idUsuario == None:
									return enviarJson({'mensaje': 'El codigo no es valido', 'campo': 'codigo'}, stat.UNPROCESSABLE)
								else:
									user = UsuarioMdl.objects.get(idUsuario = codigo.idUsuario.idUsuario)
									logger.debug("El usuario ->" + str(user))
							except UsuarioMdl.DoesNotExist:
								return enviarJson({'mensaje': 'El usuario no es valido', 'campo': 'codigo'}, stat.UNPROCESSABLE)

							if datos['password'] == datos['rpassword']:
								# Encriptacion
								pw = hashlib.sha256()
								pw.update(datos['password'].encode('utf-8'))
								pws = str(pw.hexdigest())

								user.pw = pws;
								codigo.idUsuario = None
								user.save(update_fields = ['pw'])
								codigo.save(update_fields = ['idUsuario'])
								return enviarJson({'mensaje': 'Password reestablecida', 'campo': 'password'}, stat.OK)
							else:
								return enviarJson({'mensaje': 'No coinciden la contraseñas', 'campo': 'password, rpassword'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': contra[1], 'campo': 'password'}, stat.UNPROCESSABLE)

						return enviarJson({'mensaje':'El usuario ya fue validado'}, stat.OK)
					else:
						Usuario.staticCod(codigo.codigo, codigo.idUsuario, codigo.fechaAsignacion, Usuario.EXPIRADO)
						codigo.idUsuario = None
						codigo.save(update_fields = ['idUsuario'])
						return enviarJson({'mensaje': 'El codigo ha expirado', 'campo': 'codigo'}, stat.UNPROCESSABLE)
				except CodValidacion.DoesNotExist as c:
					logger.error(c)
					return enviarJson({'mensaje': 'El codigo recibido no existe', 'campo': 'codigo'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Token inválido', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
			if tipo:
				respuesta['tipo'] = tipo
			return enviarJson(respuesta, stat.BAD_REQUEST)
 
	def vieFoto(datos, app, permiso = -1):

		user = verfUsuario(datos, app)
		if user:
			if not user.idUsuario.permiso == permiso:
				return enviarJson({'mensaje': 'El permiso no es el correcto', 'campo': 'token'}, stat.FORBIDDEN)
			#logger.debug("Mostrando ->" + str(len(user.idUsuario.foto)))
			return HttpResponse(user.idUsuario.foto)
		else:
			return enviarJson({'mensaje': 'Token no valido', 'campo': 'token'}, stat.UNAUTHORIZED)

	def calificar(datos, app, permiso = -1):
		"""
			Método calificar que permite agregar calificación y comentario
			a un chofer o persona.
		"""

		if permiso == Usuario.CLIENTE or permiso == Usuario.CHOFER:
			if val.validarExistencia(datos, ['calificacion', 'token', 'idViaje']):
				userz = verfUsuario(datos['token'], app)
				logger.debug("El id ->" + str(userz.idUsuario.idUsuario))
				logger.debug("El idFila ->" + str(userz.idUsuario.idFila))
				logger.debug("El permiso ->" + str(userz.idUsuario.permiso))

				if not permiso == userz.idUsuario.permiso:
					return enviarJson({'mensaje': 'El usuario no es correcto', 'campo': 'token'}, stat.FORBIDDEN)

				if userz:
					# conexión
					#r.connect('localhost', 28015).repl() # local-Lap
					r.connect('localhost', 28015, db='vie', user='conexion', password='vie_2016').repl() # Local-Servidor

					# abreviación a tabla
					vietaxi = r.db('vie').table('viaje')
					rquery = vietaxi.get(datos['idViaje']).run()

					if not rquery == None:
						if permiso == Usuario.CLIENTE and not userz.idUsuario.idFila == rquery['cliente']:
							return enviarJson({'mensaje': 'No coincide usuario con viaje', 'campo': 'token, viaje'}, stat.FORBIDDEN)

						if permiso == Usuario.CHOFER and not userz.idUsuario.idFila == rquery['chofer']:
							return enviarJson({'mensaje': 'No coincide chofer con viaje', 'campo': 'viaje'}, stat.FORBIDDEN)

						if datos['calificacion'] > 0 and datos['calificacion'] <= 5:
							try:
								cliente = Persona.objects.get(idPersona = rquery['cliente'])
								chof = Chofer.objects.get(idChofer = rquery['chofer'])
								logger.debug ("El cliente: " + str(cliente))
								logger.debug ("El chofer: " + str(chof))
							except Persona.DoesNotExist:
								return enviarJson({'mensaje': 'No existe la persona', 'campo': 'token'}, stat.UNPROCESSABLE)
							except Chofer.DoesNotExist:
								return enviarJson({'mensaje': 'No existe el chofer', 'campo': 'token'}, stat.UNPROCESSABLE)
								
							comment = Comentarios(
								idChofer = chof,
								idPersona = cliente,
								calificacion = float(datos['calificacion']),
								evaluador = permiso
								)
							if 'comentario' in datos:
								comment.comentario = datos['comentario']
							comment.save()

							if permiso == Usuario.CLIENTE:
								chof.num_viajes += 1
								chof.calificacion = (((chof.num_viajes-1)*chof.calificacion) + \
									decimal.Decimal(comment.calificacion))/chof.num_viajes
								chof.save()
							elif permiso == Usuario.CHOFER:
								cliente.num_viajes += 1
								cliente.calificacion = (((cliente.num_viajes-1)*cliente.calificacion) + \
									decimal.Decimal(comment.calificacion))/cliente.num_viajes
								cliente.save()
							else:
								return ({'mensaje': 'No tiene permiso (ya no debería pasar esto)', 'campo': 'permiso'}, stat.FORBIDDEN)

							return enviarJson({'mensaje': 'Comentario enviado'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'La calificacion no es valida', 'campo': 'calificacion'}, stat.UNPROCESSABLE)
					else:
						return enviarJson({'mensaje': 'Viaje no valido', 'campo': 'idViaje'}, stat.UNAUTHORIZED)
				else:
					return enviarJson({'mensaje': 'El usuario no es valido'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Faltan campos', 'campo': 'calificacion, token, idViaje'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'No tiene permisos para esto', 'campo': 'permiso'}, stat.FORBIDDEN)

