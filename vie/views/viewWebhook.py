from vie.models						import EventoWebhook
from vie.views.views				import debug

class Webhook:

	def registrarEvento(EventoID, contenido, tipoEvento = '', error = False):
		"""
			Registra la informacion del evento recibido
		"""
		modelo = EventoWebhook(
			idConekta = EventoID,
			tipoEvento = tipoEvento,
			info = contenido,
			error = error
		)
		modelo.save()