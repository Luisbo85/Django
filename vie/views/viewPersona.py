from django.views.decorators.csrf	import csrf_exempt
from django.http 					import HttpResponse

import vie.tools.validar	        as val
from vie.tools.errores				import Status as stat
from vie.views.viewUsuario 			import Usuario
from vie.views.views				import recibirJson, enviarJson, verfUsuario, debug
from vie.models						import Persona as personaMdl, Usuario as usuarioMdl, Token, Chofer, AutoChofer
from vie.views.viewUsuario 			import Usuario
from vieTaxi.views.viewAuto			import Automovil
from viePay.views.viewMetodoPago	import MetodoPago as kPago


class Persona:

	def vieRestRegistro(datos, app):
		"""
			Método que permite captuar a una persona, y de aquí
			captuar al usuario o chofer, según el caso
		"""
		try:
			if 'modo' in datos:
				try:
					modo = int(datos['modo'])
				except ValueError as e:
					return enviarJson({'mensaje': 'Formato no reconocido', 'campo': 'modo'}, stat.BAD_REQUEST)
				if modo == Usuario.L_EMAIL:
					error, campo, tipo = val.validarDatos(datos, {'tel':'string', 'nombre': 'string', 'apellidos': 'string'})
					if error:
						if val.validarTel(datos['tel']):
							telefono = personaMdl.objects.filter(telefono = datos['tel'])
							if len(telefono) == 0:
								pers = personaMdl( 
									nombre = datos['nombre'],
									apellidos = datos['apellidos'],
									telefono = datos['tel']
									)
								if 'rfc' in datos:
									mayrfc = datos['rfc'].upper()
									rfcs = personaMdl.objects.filter(rfc = mayrfc)
									pers.rfc = mayrfc
									if not val.validarRFC(mayrfc):
										return enviarJson({'mensaje': 'El RFC es invalido', 'campo': 'rfc'}, stat.FORBIDDEN)
									if len(rfcs) != 0:
										return enviarJson({'mensaje': 'RFC repetido', 'campo': 'rfc'}, stat.FORBIDDEN)

								""" El codigo de la persona debe enviarse al registro de usuario
								 para su referencia """
								return Usuario.vieRestUsuario(datos, pers, Usuario.CLIENTE, app)
							else:
								""" El codigo de la persona debe enviarse al registro de usuario
								 para su referencia """
								return enviarJson({'mensaje': 'Telefono repetido', 'campo': 'tel'}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'El telefono no tiene un formato valido', 'campo': 'tel'}, stat.FORBIDDEN)
					else:
						respuesta = {'mensaje': 'Faltan campos', 'campo': campo}
						if tipo:
							respuesta['tipo'] = tipo
						return enviarJson(respuesta, stat.BAD_REQUEST)
				elif modo == Usuario.L_FACE or modo == Usuario.L_GOOGLE:
					if 'tel' in datos:
						if val.validarTel(datos['tel']):
							telefono = personaMdl.objects.filter(telefono = datos['tel'])
							if len(telefono) == 0:
								pers = personaMdl(
									telefono = datos['tel']
									)
								return Usuario.vieRestUsuario(datos, pers, Usuario.CLIENTE, app)
							else:
								return enviarJson({'mensaje': 'Telefono repetido', 'campo': 'tel'}, stat.FORBIDDEN)
						else:
							return enviarJson({'mensaje': 'El telefono no es valido', 'campo': 'tel'}, stat.FORBIDDEN)
					else:
						return enviarJson({'mensaje': 'No se ha detectado el telefono', 'campo': 'tel'}, stat.BAD_REQUEST)
				else: 
					return enviarJson({'mensaje': 'Modo de registro no reconocido', 'campo': 'modo'}, stat.FORBIDDEN)
			else:
				return enviarJson({'mensaje': 'No se ha detectado el modo', 'campo': 'modo'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor', 'error': str(e)}, stat.UNPROCESSABLE)

	def vieTenerDatos (datos, app):
		"""
			PERMITE OBTENER LOS DATOS DE UN OBJETO Persona
		"""

		usuario = verfUsuario(datos['token'], app)
		if usuario:
			return Persona.traerDatosUsuario(usuario)
		else:
			return enviarJson({'mensaje': 'Token no valido', 'campo': 'token'}, stat.UNAUTHORIZED)

	@csrf_exempt
	def peticionFoto (request, app):
		"""
			Permite mediante una misma URL realizar distintas 
			acciones, las cuales se determinan según el tipo de petición.
		"""
		if request.method == 'POST':
			datos = recibirJson(request)
			if not datos:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			return Usuario.vieFotobitmap(datos, Usuario.CLIENTE, app)
		"""elif request.method == 'GET':
			q = request.GET.get("token", None)
			if q:"""
		if request.method == 'PROPFIND':
			q = recibirJson(request)
			debug(q)
			if not q:
				return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)
			else:
				return Usuario.vieFoto(q['token'], app, Usuario.CLIENTE)
			
		else:
			return enviarJson({'mensaje': 'El metodo no se reconoce', 'campo': 'Metodo HTTP'}, stat.NOT_ALLOW)

	def traerDatosUsuario(datos):
		"""
			Método para extraer los datos de un cliente y el chofer,
			para éste, algunos datos.
		"""

		if not datos == None:
			debug ("El permiso es: " + str(datos.idUsuario.permiso))
			campos = {}
			if datos.idUsuario.permiso == Usuario.CLIENTE:
				try:
					debug ("Es cliente y su codigo es: " + str(datos.idUsuario.idFila))
					cliente = personaMdl.objects.get(idPersona = datos.idUsuario.idFila)

					debug ("La persona es: " + str(cliente))
					avatar = usuarioMdl.objects.get(idUsuario = datos.idUsuario.idUsuario)
					campos['foto'] = str(avatar.foto)
					campos['nombre'] = cliente.nombre
					campos['apellidos'] = cliente.apellidos
					campos['email'] = datos.idUsuario.email
					campos['telefono'] = cliente.telefono
					campos['calificacion'] = round(cliente.calificacion, 1)
					campos['fecha'] = cliente.fechaRegistro
					campos['viajes'] = cliente.num_viajes
					campos['rfc'] = None
					if cliente.rfc:
						campos['rfc'] = cliente.rfc					
					return enviarJson(campos, stat.OK)
				except personaMdl.DoesNotExist as e:
					debug (e)
					return enviarJson({'mensaje': 'El token no hace referencia a una persona', 'campo': 'token'}, stat.UNPROCESSABLE)
				except personaMdl.DoesNotExist as f:
					debug (f)
					return enviarJson({'mensaje': 'No hay referencia a alguna imagen'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'El permiso es diferente', 'campo': 'permiso'}, stat.FORBIDDEN)
		else:
			return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'}, stat.BAD_REQUEST)

	def modificar (datos, app):
		"""
			PERMITE MODIFICAR (ACTUALIZAR) LOS DATOS RECIBIDOS
		"""
		try:
			if not datos == None:
				if 'token' in datos:
					usuario = verfUsuario(datos['token'], app)
					campous = []
					campope = []

					if usuario:
						if usuario.idUsuario.permiso == Usuario.CLIENTE:
							debug ("El codigo del usuario es: " + str(usuario.idUsuario.idUsuario))
							debug ("El codigo de la persona es: " + str(usuario.idUsuario.idFila))
							user = usuarioMdl.objects.get(idUsuario = usuario.idUsuario.idUsuario)
							pers = personaMdl.objects.get(idPersona = usuario.idUsuario.idFila)

							if 'email' in datos and not datos['email'] == None:
								if val.validarEmail(datos['email']):
									correos = usuarioMdl.objects.filter(email = datos['email'])
									if len(correos) == 0:
										user.email = datos['email']
										campous.append('email')
									else:
										return enviarJson({'mensaje': 'Correo repetido', 'campo': 'email'}, stat.UNPROCESSABLE)
								else:
									return enviarJson({'mensaje': 'Correo no valido', 'campo': 'email'}, stat.UNPROCESSABLE)
							if 'password' in datos and not datos['password'] == None:
								if val.validarPassword(password):
									user.password = datos['password']
									campous.append('password')
								else:
									return enviarJson({'mensaje': 'Password no valido', 'campo': 'password'}, stat.UNPROCESSABLE)
							if 'nombre' in datos and not datos['nombre'] == None:
								pers.nombre = datos['nombre']
								campope.append('nombre')
							if 'apellidos' in datos and not datos['apellidos'] == None:
								pers.apellidos = datos['apellidos']
								campope.append('apellidos')
							if 'tel' in datos and not datos['tel'] == None:
								pers.telefono = datos['tel']
								campope.append('telefono')
							if 'rfc' in datos and not datos['rfc'] == None:
								mayrfc = datos['rfc'].upper()
								if val.validarRFC(mayrfc):
									pers.rfc = mayrfc
									campope.append('rfc')
								else:
									return enviarJson({'mensaje': 'RFC no valido', 'campo': 'rfc'}, stat.UNPROCESSABLE)
						else:
							return enviarJson({'mensaje': 'No se puede acceder', 'campo': 'permiso'}, stat.UNAUTHORIZED)
						if len(campous) > 0:
							user.save(update_fields = campous)
						if len(campope) > 0:
							pers.save(update_fields = campope)
						return enviarJson({'mensaje': 'Cambios guardados'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNAUTHORIZED)
				else:
					return enviarJson({'mensaje': 'No se detecto el token', 'campo': 'token'}, stat.BAD_REQUEST)
			else:
				return enviarJson({'mensaje': 'No hay campos', 'campo': 'datos vacio'}, stat.BAD_REQUEST)
		except Exception as e:
			debug(e)
			return enviarJson({'mensaje': 'Problema con el servidor', 'campo': 'servidor'}, stat.UNPROCESSABLE)

	def eliminar(datos):
		if len(datos) > 0 and 'token' in datos:
			token = verfUsuario(datos['token'])
			if token:
				usuario = token.idUsuario
				lista = Token.objects.filter(idUsuario = usuario)
				debug(lista)
				for item in lista:
					debug (item)
					item.asignado	= False
					item.idUsuario	= None
					item.save(update_fields = ['asignado','idUsuario'])
				usuario.delete()
				return enviarJson({'mensaje': 'Eliminados los registros'}, stat.OK)
			else:
				return enviarJson({'mensaje': 'No se encontro el token enviado', 'campo': 'token'}, stat.UNAUTHORIZED)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'token'}, stat.BAD_REQUEST)