import random as rand
import datetime
import rethinkdb as r
import conekta

from django.shortcuts				import render
from django.views.decorators.csrf	import csrf_exempt
from django.utils					import timezone
from django.utils.crypto 			import get_random_string
from django.http 					import QueryDict, HttpResponse
import vie.tools.validar		as val
from Django.settings 				import DEBUG, EMPRESAS

from vie.views.views	import recibirJson, enviarJson, generarToken, verfUsuario, verfEmpresaCh, debug, verfChofer
from vie.models			import Chofer as ChoferMdl, Auto as AutoMdl, Usuario as UsuarioMdl, Persona, AutoChofer
from viePay.views.viewMetodoPago import MetodoPago
from vie.tools.errores		import Status as stat
from vie.views.viewUsuario 			import Usuario
from vieTaxi.views.viewAuto				import Automovil


class Chofer:
	"""
		Vista que controla opciones del Chofer.
	"""
	ESTATUS = {
		'ACTIVO': 0,
		'SUSPENDIDO': 1
	}

	def vieRegistroChofer (datos, app):
		"""
			Permite registrar a un chofer.
		"""
		if val.validarExistencia(datos, ['nombre', 'apellidos', 'domicilio', 'rfc', 'tel']):
			mayrfc = datos['rfc'].upper()
			if val.validarRFC(mayrfc):
				rfcs = ChoferMdl.objects.filter(rfc = mayrfc)
				if len(rfcs) == 0:
					chofer = ChoferMdl(
							nombre		= datos['nombre'],
							apellidos	= datos['apellidos'],
							rfc 		= mayrfc,
							domicilio	= datos['domicilio'],
							telefono	= datos['tel']
						)
					datos['modo'] = Usuario.L_EMAIL
					#Registro de un chofer en conekta para poder hacer el cobro a las subscripciones
					if 'email' in datos and val.validarEmail(datos['email']):
						correos = UsuarioMdl.objects.filter(email = datos['email'])
						if len(correos) == 0:
							choferConekta = { 
								'name': datos['nombre'], 
								'email': datos['email'], 
								'plan_id': 'cuota_7', 
								'corporate': False
							}
							clienteID = ''
							#En caso de enviar una forma de pago en la que se cobre la subscripcion
							if val.validarExistencia(datos, ['token_tarj', 'cod', 'tipo']) and datos['tipo'] in MetodoPagos:
								choferConekta['payment_sources'] = {
									'token_id': datos['token_tarj'],
									'type': 'card'
								}
								try:
									#Mandar a registrar el chofer a conekta
									cliente = conekta.Customer.create(choferConekta) 
									clienteID = cliente._id
								except:
									return enviarJson({'mensaje': 'Hubo un problema para registrar al chofer en Conekta', 'campo': 'conekta'}, stat.UNPROCESSABLE)
							if 'empresa' in datos:
								empresa = verfEmpresaCh(datos['empresa'])
								if empresa:	
									chofer.idEmpresa = empresa
									if 'placas' in datos:
										datos['placas'] = datos['placas'].upper()
										return Usuario.vieRestUsuario(datos, chofer, Usuario.CHOFER, app, clienteID, True)
									return Usuario.vieRestUsuario(datos, chofer, Usuario.CHOFER, app, clienteID)
								else:
									return enviarJson({'mensaje': 'La empresa ingresada no existe', 'campo': 'empresa'}, stat.NOT_FOUND)
							if 'placas' in datos:
								datos['placas'] = datos['placas'].upper()
								return Usuario.vieRestUsuario(datos, chofer, Usuario.CHOFER, app, clienteID, True)
							return Usuario.vieRestUsuario(datos, chofer, Usuario.CHOFER, app, clienteID)
						else:
							return enviarJson({'mensaje': 'Correo repetido', 'campo': 'email'}, stat.FORBIDDEN)
					else:
						return enviarJson({'mensaje': 'Falta el correo', 'campo': 'email'}, stat.BAD_REQUEST)
				else:
					return enviarJson({'mensaje': 'RFC repetido', 'campo': 'rfc'}, stat.FORBIDDEN)
			else:
				return enviarJson({'mensaje': 'RFC invalido', 'campo': 'rfc'}, stat.FORBIDDEN)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'nombre, apellidos, domicilio, rfc, tel'}, stat.BAD_REQUEST)

	def vieTenerDatos (datos, app):
		"""
			PERMITE OBTENER LOS DATOS DE UN OBJETO Chofer
		"""
		token = verfUsuario(datos, app)
		if token:
			return Chofer.traerDatosUsuario(token, app)
		else:
			return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNAUTHORIZED)

	def traerDatosUsuario(datos, app):
		"""
			Método para extraer los datos de un chofer,
			si es persona, sólo algunos datos
		"""

		if not datos == None:
			debug ("El permiso es: " + str(datos.idUsuario.permiso))
			campos = {}
			if datos.idUsuario.permiso == Usuario.CHOFER or datos.idUsuario.permiso == Usuario.ADMIN:
				debug ("Es chofer y su codigo es: " + str(datos.idUsuario.idFila))
				try:
					avatar = UsuarioMdl.objects.get(idUsuario = datos.idUsuario.idUsuario)
					campos['foto'] = str(avatar.foto)
				except:
					campos['foto'] = None
				chofer = ChoferMdl.objects.get(idChofer = datos.idUsuario.idFila)
				debug ("El chofer es: " + str(chofer))
				campos['placas'] = None
				campos['marca'] = None
				campos['color'] = None
				campos['empresa'] = None
				if not chofer.idAuto == None and chofer.idAuto.estatus == Automovil.estados['ASIGNADO']:
					campos['placas'] = chofer.idAuto.placas
					campos['marca'] = chofer.idAuto.marca
					campos['color'] = chofer.idAuto.color
				if not chofer.idEmpresa == None:	
					campos['empresa'] = chofer.idEmpresa.nombre
				campos['nombre'] = chofer.nombre
				campos['apellidos'] = chofer.apellidos
				campos['domicilio'] = chofer.domicilio
				campos['telefono'] = chofer.telefono
				campos['rfc'] = chofer.rfc
				campos['calificacion'] = round(chofer.calificacion, 1)
				campos['fecha'] = chofer.fechaInicio
				campos['licencia'] = chofer.licencia
				campos['viajes'] = chofer.num_viajes
				return enviarJson(campos, stat.OK)
			else:
				return enviarJson({'mensaje': 'El permiso es diferente', 'campo': 'permiso'}, stat.UNAUTHORIZED)

	def mostrar(datos):
		if 'id' in datos:
			try:
				id = int(datos['id'])
				chofer = ChoferMdl.objects.get(pk=id)
				campos = {'mensaje': 'Informacion del chofer'}
				campos['nombre'] = chofer.nombre
				campos['apellidos'] = chofer.apellidos
				campos['domicilio'] = chofer.domicilio
				campos['telefono'] = chofer.telefono
				campos['rfc'] = chofer.rfc
				campos['fecha'] = chofer.fechaInicio
				return enviarJson(campos, stat.OK)
			except ValueError:
				return enviarJson({'mensaje': 'El tipo de datos es incorrecto', 'campo':'id', 'tipo': 'int'}, stat.BAD_REQUEST)
			except ChoferMdl.DoesNotExist:
				return enviarJson({'mensaje': 'No se encontro al chofer'}, stat.NOT_FOUND)
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo':'id'}, stat.BAD_REQUEST)


	def modificar(datos):
		"""
			Métodos para modificar (actualizar los datos de Chofer
		"""
		if not datos == None:
			if 'token' in datos:
				usuario = verfUsuario(datos['token'])
				campous = []
				campoch = []

				if usuario:
					if usuario.idUsuario.permiso == Usuario.CHOFER:
						debug ("El codigo del usuario es: " + str(usuario.idUsuario.idUsuario))
						debug ("El codigo del chofer es: " + str(usuario.idUsuario.idFila))
						user = UsuarioMdl.objects.get(idUsuario = usuario.idUsuario.idUsuario)
						chof = ChoferMdl.objects.get(idChofer = usuario.idUsuario.idFila)

						if 'email' in datos and not datos['email'] == None:
							if val.validarEmail(datos['email']):
								correos = UsuarioMdl.objects.filter(email = datos['email'])
								if len(correos) == 0:
									user.email = datos['email']
									campous.append('email')
								else:
									return enviarJson({'mensaje': 'Correo repetido', 'campo': 'email'}, stat.UNPROCESSABLE)
							else:
								return enviarJson({'mensaje': 'Correo no valido', 'campo': 'email'}, stat.UNPROCESSABLE)
						if 'password' in datos and not datos['password'] == None:
							if val.validarPassword(password):
								user.password = datos['password']
								campous.append('password')
							else:
								return enviarJson({'mensaje': 'Password no valido', 'campo': 'password'}, stat.UNPROCESSABLE)
						if 'domicilio' in datos and not datos['domicilio'] == None:
							chof.domicilio = datos['domicilio']
							campoch.append('domicilio')
						if 'tel' in datos and not datos['tel'] == None:
							chof.telefono = datos['tel']
							campoch.append('telefono')

						if len(campous) > 0 or len(campoch) > 0:
							for clave in campous:
								user.save(update_fields = [clave])

							for clave in campoch:
								chof.save(update_fields = [clave])
							return enviarJson({'mensaje': 'Cambios guardados'}, stat.OK)
						else:
							return enviarJson({'mensaje': 'Guardado sin cambios'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'No se puede acceder', 'campo': 'permiso'}, stat.FORBIDDEN)
				else:
					return enviarJson({'mensaje': 'El token no es valido', 'campo': 'token'}, stat.UNAUTHORIZED)
			else:
				return enviarJson({'mensaje': 'No se detecto un usuario', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'No hay campos', 'campo': 'datos vacio'}, stat.BAD_REQUEST)

	@csrf_exempt
	def buscar (request, empresa):
		"""
			Método que permitirá buscar a varios choferes
			por determinados filtros que el uuario ingrese.
			La especificación del funcionamiento estará documentado.
		"""

		if request.method == 'GET':
			consulta = {}
			nombre = request.GET.get("nombre", None)
			apellidos = request.GET.get("apellidos", None)
			domicilio = request.GET.get("domicilio", None)
			telefono = request.GET.get("tel", None)
			rfc = request.GET.get("rfc", None)
			calificacion = request.GET.get("cal", None)
			fecha1 = request.GET.get("fecha", None)
			fecha2 = request.GET.get("fechasm", None)
			fecha3 = request.GET.get("fechasma", None)
			viajes = request.GET.get("viajes", None)
			licencia = request.GET.get("lic", None)
			placas = request.GET.get("placas", None)

			if nombre:
				consulta['nombre__icontains'] = nombre
			if apellidos:
				consulta['apellidos__icontains'] = apellidos
			if domicilio:
				consulta['domicilio__icontains'] = domicilio
			if telefono:
				consulta['telefono__contains'] = telefono
			if rfc:
				consulta['rfc__icontains'] = rfc
			if calificacion:
				try:
					consulta['calificacion__contains'] = int(calificacion)
				except:
					return enviarJson({'mensaje': 'El tipo de dato es incorrecto', 'campo': 'calificacion'}, stat.BAD_REQUEST)
			if fecha1:
				consulta['fechaInicio__contains'] = fecha1
			if fecha2:
				consulta['fechaInicio__lte'] = fecha2
			if fecha3:
				consulta['fechaInicio__gte'] = fecha3
			if viajes:
				try:
					consulta['num_viajes'] = int(viajes)
				except:
					return enviarJson({'mensaje': 'El tipo de dato es incorrecto', 'campo': 'num_viajes'}, stat.BAD_REQUEST)
			if licencia:
				try:
					consulta['licencia__contains'] = int(licencia)
				except:
					return enviarJson({'mensaje': 'El tipo de dato es incorrecto', 'campo': 'licencia'}, stat.BAD_REQUEST)
			if placas:
				consulta['autochofer__placas__icontains'] = placas

			if len(consulta) == 0:
				return enviarJson({'mensaje': 'No se detectaron campos', 'campo': 'datos vacio'}, stat.BAD_REQUEST)
			if not empresa == EMPRESAS['TODO']:
				consulta['empresa'] = empresa  
			try:
				if empresa == EMPRESAS['TODO']:
					chofs = ChoferMdl.objects.filter(**consulta).exclude(empresa = -1)
				else:
					chofs = ChoferMdl.objects.filter(**consulta)
				lista =  []

				for chofer in chofs:
					campos = {}
					campos['placas'] = null
					campos['marca'] = null
					campos['color'] = null
					campos['empresa'] = null
					if not chofer.idAuto == None and chofer.idAuto.estatus == Automovil.estados['ASIGNADO']:
						campos['placas'] = chofer.idAuto.placas
						campos['marca'] = chofer.idAuto.marca
						campos['color'] = chofer.idAuto.color
					if not chofer.idEmpresa == None:	
						campos['empresa'] = chofer.idEmpresa.nombre
					campos['nombre'] = chofer.nombre
					campos['apellidos'] = chofer.apellidos
					campos['domicilio'] = chofer.domicilio
					campos['telefono'] = chofer.telefono
					campos['rfc'] = chofer.rfc
					campos['calificacion'] = round(chofer.calificacion, 1)
					campos['fecha'] = chofer.fechaInicio
					campos['licencia'] = chofer.licencia
					campos['viajes'] = chofer.num_viajes
					lista.append(campos)

				debug (len(lista))
				result = {'resultados': lista}
				debug('\n')
				debug(result)

				if len(lista) == 0:
					return enviarJson({'mensaje': 'No hay coincidencias'}, stat.OK)

				return enviarJson(result, stat.OK)
			except:
				return enviarJson({'mensaje': 'Consulta erronea, verifique', 'campo': 'consulta'}, stat.BAD_REQUEST)

			return enviarJson(consulta, 418)

		else:
			return enviarJson({'mensaje': 'El metodo HTTP no se reconoce', 'campo': 'http'}, stat.NOT_ALLOW)
	
	@csrf_exempt
	def verfsus(request, app):
		datos = recibirJson(request)
		if not datos:
			return enviarJson({'mensaje': 'Formato erroneo del JSON'}, stat.BAD_REQUEST)

		if 'token' in datos:
			usuario = verfUsuario(datos['token'])
			if usuario:
				debug ("Su ID de fila: " + str(usuario.idUsuario.idFila))
				chofer = verfChofer(usuario.idUsuario.idFila)
				if chofer and usuario.idUsuario.permiso == Usuario.CHOFER:
					hoy = datetime.date.today()
					debug("vigencia : " + str(chofer.vigenciaSubs) + "\nHoy: " + str(hoy))

					if chofer.vigenciaSubs >= hoy:
						return enviarJson({'suscripcion': True}, stat.OK)
					else:
						return enviarJson({'suscripcion': False}, stat.OK)
				else:
					return enviarJson({'mensaje': 'No es chofer', 'campo': 'token'}, stat.UNPROCESSABLE)
			else:
				return enviarJson({'mensaje': 'Usuario no valido', 'campo': 'token'}, stat.BAD_REQUEST)
		else:
			return enviarJson({'mensaje': 'No se detecto token', 'campo': 'token'}, stat.BAD_REQUEST)

	def buscarChofer(id):
		"""
			Buscar un chofer por su id
		"""
		try:
			return ChoferMdl.objects.get(pk=id)
		except ChoferMdl.DoesNotExist:
			return False

	def listar():
		lista = ChoferMdl.objects.filter()
		choferes = []
		for pos in lista:
			chofer = {}
			chofer['id']        = pos.idChofer
			chofer['estatus']   = pos.estatus
			chofer['nombre']    = pos.nombre
			chofer['apellidos'] = pos.apellidos
			chofer['telefono']  = pos.telefono
			choferes.append(chofer)

		return enviarJson({'mensaje': 'Lista de choferes', 'choferes': choferes}, stat.OK)

	def vieSuspender(datos):
		if 'id' in datos:
			if type(datos['id']) == int:
				chofer = Chofer.buscarChofer(datos['id'])
				if chofer:
					if chofer.estatus == Chofer.ESTATUS['ACTIVO']:
						chofer.estatus = Chofer.ESTATUS['SUSPENDIDO']
						chofer.save(update_fields = ['estatus'])
						return enviarJson({'mensaje': 'El chofer ha sido suspendido'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'Chofer ya esta suspendido', 'campo': 'estatus'}, stat.FORBIDDEN)	
				else:
					return enviarJson({'mensaje': 'Chofer no fue encontrado', 'campo': 'id'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'id', 'tipo': 'int'}, stat.BAD_REQUEST)	
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'id'}, stat.BAD_REQUEST)

	def vieReactivar(datos):
		if 'id' in datos:
			if type(datos['id']) == int:
				chofer = Chofer.buscarChofer(datos['id'])
				if chofer:
					if chofer.estatus == Chofer.ESTATUS['SUSPENDIDO']:
						chofer.estatus = Chofer.ESTATUS['ACTIVO']
						chofer.save(update_fields = ['estatus'])
						return enviarJson({'mensaje': 'El chofer ha sido reactivado'}, stat.OK)
					else:
						return enviarJson({'mensaje': 'Chofer ya esta activo', 'campo': 'estatus'}, stat.FORBIDDEN)	
				else:
					return enviarJson({'mensaje': 'Chofer no fue encontrado', 'campo': 'id'}, stat.NOT_FOUND)
			else:
				return enviarJson({'mensaje': 'Tipo de dato incorrecto', 'campo': 'id', 'tipo': 'int'}, stat.BAD_REQUEST)	
		else:
			return enviarJson({'mensaje': 'Faltan campos', 'campo': 'id'}, stat.BAD_REQUEST)